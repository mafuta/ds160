<?php
error_reporting(0);

if (extension_loaded("zlib") && (ini_get("output_handler") != "ob_gzhandler")) {
    ini_set('zlib.output_compression', 1);
    ini_set('zlib.output_compression_level', -1);
}

if ( !isset($_GET['Type']) || !isset($_GET['File']) )
{
    exit;
}

//Expires header
header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + 2592000));

if ( $_GET['Type']=='js' )
{
    header('Content-type:text/javascript; charset=utf-8');
    $file = $_SERVER['DOCUMENT_ROOT'].'/js/'.$_GET['File'].'.js';

}
elseif($_GET['Type']=='css')
{
    header('Content-type:text/css; charset=utf-8');
    $file = $_SERVER['DOCUMENT_ROOT'].'/css/'.$_GET['File'].'.css';
}
else
{
    exit;
}

$last_modified_time = filemtime($file);
$etag 				= md5_file($file);

header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
header("Etag: $etag");

if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time ||
		trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag) {
	header("HTTP/1.1 304 Not Modified");
	exit;
}

readfile($file);