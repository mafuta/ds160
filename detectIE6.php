<?php
 
//**************************************//
//										//
//	Graceful IE6 Detect Advise w/jQuery	//
//		by: Joe Watkins					//
//		http://www.thatgrafix.com		//
//										//
//**************************************//

// IE6 string from user_agent
$ie6 = "MSIE 6.0";

// detect browser
$browser = $_SERVER['HTTP_USER_AGENT'];

// yank the version from the string
$browser = substr("$browser", 25, 8);

/* 
It appears that you are using an out dated web browser Internet Explorer 6.  
While you may still visit this website we encourage you to upgrade your web browser so you can enjoy all the rich features this website offers as well as other websites. 
<br /><div class=\"kickRight\"><a href=\"javascript: killIt('error');\"> Close This Box</a></div></div>";
 * 
 * 
 * */

// html for error
$error = "<div class=\"error\" id=\"error\"><strong>הודעת מערכת:</strong> 
המערכת זיהתה שהנך משתמש בדפדפן אינטרנט אקספלורר מגירסה 6. על מנת שתוכל להנות מגלישה באתר זה ובאתרים אחרים יש לבצע שדרוג של הדפדפן.
מצורף בזאת קישור לעדכון הדפדפן לגירסת אינטרנט אקספלורר האחרונה. העדכון אורך מספר דקות בלבד.
<br /><br />
<a href=\"http://windows.microsoft.com/he-IL/internet-explorer/products/ie/home\"><strong>עדכון אינטרנט אקספלורר</strong></a>";

// if IE6 set the $alert 
if($browser == $ie6){ $alert = TRUE; }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<title>Graceful IE6 Detect Advise w/jQuery</title>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
<style type="text/css">

/* styles for error box */
.error {
    direction:rtl;
	background:#FBE3E4;
	color:#8a1f11;
	border-color:#FBC2C4;
	padding:20px;
	border:dashed 1px red;
	width:800px;
	margin:0px auto;
	font-family:Arial, Arial, Helvetica, sans-serif;
	font-size:1em;
	line-height:1.3em;
}

.error a, .error a:hover{
	color:#8a1f11;
}

/* simply moves close box to right */
.kickRight {
	text-align:right;
}
</style>
<script type="text/javascript">
   	function killIt(item){
		$('#'+item).fadeOut(1000);
	}			
</script>	
</head>

<body>
<!-- IE6 Detect Advise Notice -->
	<?php if(isset($alert) && $alert){ echo $error; } ?>
<!-- // end IE6 Detect Advise Notice -->
</body>
</html>

