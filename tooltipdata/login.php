<div class="mask roundedCorners loginBlock" style="background: #343434; width:590px; height:auto; ">
	<div class="roundedCorners" style="width:515px; background:#ededed; border:8px solid #5e5e5e; padding:30px;">
		<div id="login_main">
			<h3 style="display:block;">התחברות לקוח</h3>
			
			<div id="loginans"></div>
			
			<div style="color:#3f3d42; font-size:12px; margin-bottom:5px;">
			    שלום, במידה ובצעת הרשמה בעבר נא הזן את שם המשתמש והסיסמה איתם נרשמת.
			</div>
		  
			<form method="post" id="loginform" name="loginform">
			<table>
				<tr>
					<td style="padding-left:10px;">
						<label for="login_user" style="color:#000; font-size:13px; font-weight:bold;">שם משתמש</label>
						<input type="text" id="login_user" name="login_user" placeholder="שם משתמש" style="line-height:19px; height:33px; width:156px; margin-bottom:0; background:url('/img/icons.png') #fff no-repeat; background-position: 164px -212px; padding-right:30px;" maxlength="12" />
					</td>
					
					<td style="padding-left:10px;">
						<label for="login_pass" style="color:#000; font-size:13px; font-weight:bold;">סיסמה</label>
						<input type="password" id="login_pass" name="login_pass" placeholder="סיסמה" style="line-height:33px; height:33px; width:156px; margin-bottom:0; background:url('/img/icons.png') #fff no-repeat; background-position: 166px -183px; padding-right:30px;" maxlength="12" />
					</td>
					
					<td style="vertical-align:bottom;">
						<button type="submit" id="connect" name="connect" onclick="return makeLogin();" ></button>
					</td>
				</tr>
			</table>
			</form>
			
	
			<div class="password-remainder">
	
				<div style="clear:both; display:block; border-bottom:1px dotted #afafaf; margin:1em auto;"></div>
			
			
				<h3 style="display:block;">שכחת סיסמה?</h3>
				<div id="response-container">
		
					<div id="reset-loginans"></div>
				
					<table id="reminder_form">
						<tr>
							<td colspan="2" style="color:#3f3d42; font-size:12px; margin-bottom:15px; text-align:justify; padding-bottom:4px;">
							שכחת את סיסמת ההתחברות? נא הזן את כתובת הדוא"ל עמה נרשמת לאתר, בתוך מספר דקות ישלח אליך קישור לאיפוס הסיסמה.
							</td>
						</tr>
		
						<tr>
							<td style="padding-left:10px;">
								<label for="reminder" style="font-size:13px; color:#030102;">אימייל</label>
								<input type="text" name="remindermail" id="remindermail" style="padding-left:10px; direction:ltr; text-align:left; width:356px; height:33px; background:url('/img/icons.png') #fff no-repeat; line-height:20px; padding-left:30px; background-position: 6px -44px; margin-bottom:0;" maxlength="50" />
							</td>
							
							<td style="vertical-align:bottom;">
								<input style="cursor:pointer;" type="submit" id="remind" name="remind" value="" onclick="return resetPassword(document.getElementById('remindermail').value);" />
							</td>
						</tr>
					</table>
				
					<div id="ajax-loader" style="display:none; background:url('/img/ajax-loader.gif') no-repeat; width:220px; height:19px; margin:1em auto 0 auto;">
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>