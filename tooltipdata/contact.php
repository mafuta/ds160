<div class="contact-form roundedCorners">
	<script>
	var loader = $('<img/>',{'src':'/img/smallLoading.gif'});

	$('.contact-form').find('textarea,:text').each(function(){
		$(this).attr('autocomplete','off');
		
	});
	</script>

	<form id="contactf" name="contactf" action="">
		<div class="contact-form-right">

			<label for="contact_name">שם מלא</label>
	      	<input class="bigField" type="text" id="contact_name" name="contact_name" placeholder="הקלד שם מלא" />
	      
			<label for="contact_email">כתובת מייל</label>
	     	<input class="bigField" type="text" id="contact_email" name="contact_email" placeholder="הקלד כתובת דואר אלקטרוני" class="ltr-field" />
	     
			<label for="contact_phone">טלפון ליצירת קשר</label>
	      	<input class="bigField" type="text" id="contact_phone" name="contact_phone" placeholder="הקלד טלפון ללא סימני הפרדה" class="ltr-field" />
	     
			<label for="subject">נושא</label>
	      	<input class="bigField" type="text" id="contact_subject" name="contact_subject" placeholder="הקלד נושא" />
		
			<input type="hidden" name="contact_ip" id="contact_ip" value="<?=getenv("REMOTE_ADDR");?>" />
		</div>
	
		<div class="contact-form-left">
			<textarea class="contact_message" name="contact_message" id="contact_message" placeholder="הקלד כאן הודעתך"></textarea>
		</div>
	</form>
	
	<div class="contact-answer"></div>
	
	<div class="contact-grayLine">
		<button class="contact-send" type="submit" name="contact_send" id="contact_send" onclick="return false;"></button>
		
		<div class="contact-info">
			    <b>ניתן ליצור עמנו קשר גם בדרכים הבאות:</b><br />
   				<b>אימייל:</b> <a class="email" href="mailto:service@ds160.co.il">service@ds160.co.il</a>&emsp;
    			<b>טלפון: </b>052-8518665 <br /> (א-ה: 08:00-20:00, ו' 08:00-14:00)
		</div>
	</div>
	
</div>

<script>
trackConversion("contact");
</script>