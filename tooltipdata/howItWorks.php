<div class="how-it-works roundedCorners" style="width:960px;">
	<script type="text/javascript">
	$(document).on("click", ".how-it-works .start", function(){
	
		window.location.href='ds160.php';
		
	});
	</script>
	
	<?php
	if ( isset($_GET['newWindow']) )
	{
	?>
	<style type="text/css">
	.wrapper{overflow-y:visible !important;}
	</style>
	<?php
	}
	?>
	
	<div class="wrapper">
		<div class="header">
			<div class="title"></div>
			
			<div class="cubes">
				<div class="cube cube1">
					<div class="desc">
                                       לחצו בעמוד הבית על "לחץ כאן כדי להתחיל" והשלימו את שאלון הוויזה לארה"ב המתורגם בכמה דקות. אל חשש אם אינכם יודעים את כל התשובות.
					</div>
				</div>
				
				<div class="cube cube2">
					<div class="desc">
					לאחר השלמת שאלון הוויזה, נציג שירות יצור עמכם קשר על מנת להשלים פרטים חסרים ולתאם עבורכם ראיון בשגרירות ארה"ב.
					</div>
				</div>
				
				<div class="cube cube3">
					<div class="desc">
                                        לאחר שתאמנו לכם ראיון בשגרירות ארה"ב, אנו נדאג לשלוח לכם למייל את כל המסמכים הנדרשים לראיון בשגרירות יחד עם דף הנחיות מפורט, בנוסף נתדרך אתכם באופן אישי ונכווין אתכם ליום הראיון בשגרירות ארה"ב.
					</div>
				</div>
			</div>
		</div>
		
		<div class="middle">
			<div class="middle-left">
				<div class="title"></div>
				<div class="note">
					<div>טופס הבקשה הרשמי לוויזה לארה"ב (ds160)</div>
				
					<div>זימון לראיון בשגרירות ארה״ב</div>
				
					<div>קבלה על תשלום אגרת וויזת תייר לארה"ב</div> 
				
					<div>יעוץ ותדרוך מלא ליום הריאיון בשגרירות ארה"ב</div>
				</div>
				<div class="start"></div>
			</div>
		</div>
	
		
		<div class="bottom">
			<div class="text">
                כל המעוניין לבקש ויזה לארה"ב ללא קשר לגילו, מחוייב לשלם את אגרת השגרירות. עלות האגרה היא 160$ לאדם! (כ-576 ₪). על מנת שנוכל לתאם לכם ריאיון בשגרירות ארה"ב יש לשלם את האגרה, אפשרויות תשלום האגרה הינן:
				<br /><br />
				<ol style="list-style-type:decimal; margin-right:2em;">
					<li>בכרטיס אשראי, ניתן לשלם באמצעות כרטיס אשראי מסוג ויזה ומאסטרקארד בלבד באמצעות אחד מנציגי השירות שלנו.</li>
					<li>במזומן: ניתן לשלם את האגרה במזומן בשקלים בלבד בכל סניף של בנק-דואר ישראל, על מנת לשלם את האגרה במזומן יש לגשת לדואר עם שובר תשלום אישי מיוחד אותו נדאג להנפיק ולשלוח לכם למייל ללא עלות כספית נוספת!</li>
					
				</ol>
			</div>
			<div class="start"></div>
		</div>
	</div>
</div>
