<?php
$secured = true;

require_once ($_SERVER ['DOCUMENT_ROOT'] . '/inc/base.php');

if ( $system->checkConnected() ) {
	header('location:Returning.php');
	exit;
}

require_once ($_SERVER ['DOCUMENT_ROOT'] . '/inc/header.php');
?>
<button onclick="Tooltip('login');" class="main-login-button pie">התחבר</button>

<div id="intro-image" class="intro_image_961"></div>

<div class="intro_text">

	<button class="intro_button" onclick="window.location.href='registration.php';"></button> 
		<div class="ds-index-title">
		שירות להוצאת<br />
		ויזת תיירות ועסקים<br />
		לארצות הברית
		</div>
		
		<div class="ds-index-subtitle">הוצאת ויזה לארה"ב ב-3 שלבים פשוטים!</div>
		
        <span class="how-it-works-link" onclick="Tooltip('HowItWorks');">
	לחץ כאן לגלות איך זה עובד
	</span>
        
        <div class="intro_note">
                <div>מילוי טופס הבקשה לויזה לארה"ב</div>
                <div>תאום מועד ראיון בשגרירות ארה"ב</div>
                <div>יעוץ והכוונה ליום הראיון בשגרירות ארה"ב </div>
                <div>טיפול תוך 24 שעות בהתחייבות!</div>
        </div>
        
	<div class="whiteline" style="top:46em;"></div>

    <?php
    $time = mktime(23, 00, 00, 02, 20, 2014);
    if ( date("Y-m-d H:i:s", time()) < date("Y-m-d H:i:s", $time) ) {
    ?>
    <div class="coupon84">
        <div class="redpromotion" onclick="Tooltip('coupon84terms')"></div>
        <div class="sticker" onclick="Tooltip('coupon84terms')"></div>
    </div>
    <?php
    }
    ?>
</div>

<div style="margin-top: 15em;"></div>

<script type="text/javascript">
$(function(){
	$(document).on('mouseover', '.fb-share', function(){
		$(this).css('opacity','1');
	}).on('mouseout', '.fb-share', function(){
		$(this).css('opacity','0.8');
	});	
});

function setIntroImage()
{
	if ($(window).width()>=1800)
	{
		$("#intro-image").attr('class','intro_image_961').css('display','');
	}
	else if($(window).width()<1800 && $(window).width()>=1450)
	{
		$("#intro-image").attr('class','intro_image_750').css('display','');
	}
        else if ( $(window).width()>970 )
        {
                $('#intro-image').attr('class','intro_image_500').css('display','');
        }
        else
        {
                $('#intro-image').css('display','none');
        }
}
$(window).resize(function(){
	setIntroImage();
});

$(function(){
	setIntroImage();
});
</script>
<?php
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/inc/footer.php');
?>
