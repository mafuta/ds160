<?php
define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);

class imageProcess
{
 
	protected $image		= null;
	protected $originInfo	= null;
	protected $imageType 	= null;
	protected $imageInfo 	= null;

	protected $allowedExt 	= array(
									"jpg", "jpeg", "JPG", "JPEG", 
									"png", "PNG", 
									"gif", "GIF", 
									"bmp", "BMP"
								);
	
	/**
	 * @method load
	 * @param filename - origin image file path including file name, for example: /home/www/old_image.jpg
	 * @return class object
	 *
	 * @example: load('/home/www/old_image.jpg')
	 */
	public function load($filename)
	{
 		if ( !file_exists($filename) || is_dir($filename) )
 		{
 			if ( !file_exists($filename) )
 			{
 				echo "<b>Load error:</b> file does not exist<br />";
 			}else{
 				echo "<b>Load error:</b> given path is a directory<br />";
 			}
 			
 			return $this;
 		}
 
 		//Reseting class variables before getting new file information
 		$this->originInfo  	= pathinfo($filename);	
 		$this->imageInfo 	= null;
		$this->imageType 	= null;
 		$this->image	 	= null;
				
 		/**
 		 * getimagesize returns array:
 		 * [0] - width
 		 * [1] - height
 		 * [2] - image type
 		 * [3] - height="yyy" width="xxx" string that can be used directly in an IMG tag.
 		 * ['mime'] - mime type
 		 */
		$this->imageInfo = getimagesize($filename);

		$this->imageType = $this->imageInfo[2];
		
		switch($this->imageType)
		{
			case IMAGETYPE_JPEG:
				$this->image = imagecreatefromjpeg($filename);
				break;
				
			case IMAGETYPE_GIF:
				$this->image = imagecreatefromgif($filename);
				break;
				
			case IMAGETYPE_PNG:
				$this->image = imagecreatefrompng($filename);
				break;
		}

      	return $this;
   }
   
   /**
    * @method save
    * @param filename		-	destination file path including file name, for example: /home/www/new_image.jpg
    * @param image_type		-	image_type - saving format	
    * @param compression	-	compression level - from 1 to 100 (0 - low quality, 100 - high quality)
    * @param permissions 	-	can be used to chmod file directly after saving it (LINUX/UNIX only)
    * 
    * @return class object
    * 
    * @example: save('/home/www/new_image.jpg', IMAGETYPE_JPEG, 80, 755)
    */
   public function save($filename, $image_type = IMAGETYPE_JPEG, $compression = 75, $permissions = null)
   {
	   	switch($this->imageType)
	   	{
	   		case IMAGETYPE_JPEG:
	   			imagejpeg($this->image, $filename, $compression);
	   			break;
	   	
	   		case IMAGETYPE_GIF:
	   			imagegif($this->image, $filename);
	   			break;
	   	
	   		case IMAGETYPE_PNG:
	   			imagepng($this->image, $filename);
	   			break;
	   	}

		if( !empty($permissions) && is_numeric($permissions) )
		{
			@chmod($filename,$permissions);
		}
      
		return $this;
	}
   
	/**
	 * @method output
	 * @param image_type - output format
	 * @return void
	 * description: output file that was loaded using the load function to the output stream	
	 */
	function output($image_type = IMAGETYPE_JPEG) 
	{
		switch($this->imageType)
		{
			case IMAGETYPE_JPEG:
				imagejpeg($this->image);
				break;
			  
			case IMAGETYPE_GIF:
				imagegif($this->image);
				break;
			  
			case IMAGETYPE_PNG:
				imagepng($this->image);
				break;
		}
	}
   	
	/**
	 * @method getWidth
	 * 
	 * @return int - image width
	 */
   	protected function getWidth()
   	{
   		if ( empty($this->image) ){return false;}
   		if ( !empty($this->imageInfo[0]) )
   		{
   			return $this->imageInfo[0];
   		}
   		else
   		{
   			return imagesx($this->image);
   		}
   	}
   	
   	/**
   	 * @method getHeight
   	 *
   	 * @return int - image height
   	 */
   	protected function getHeight()
   	{
   		if ( empty($this->image) ){return false;}
   	   	if ( !empty($this->imageInfo[1]) )
   		{
   			return $this->imageInfo[1];
   		}
   		else
   		{
   			return imagesy($this->image);
   		}
   	}
   	
   	/**
   	 * @method resizeToHeight
   	 *
   	 * @return class object
   	 */
   	public function resizeToHeight($height)
   	{
		$ratio = $height / $this->getHeight();
		$width = $this->getWidth() * $ratio;
		return $this->resize($width, $height);
   	}
   	
   	/**
   	 * @method resizeToWidth
   	 *
   	 * @return class object
   	 */
   	public function resizeToWidth($width)
   	{
		$ratio 	= $width / $this->getWidth();
		$height = $this->getheight() * $ratio;
		return $this->resize($width, $height);
   	}

   	/**
   	 * @method ratio
   	 * @param percent
   	 * @desc ratio is used to change size in percent
   	 * 
   	 * @example ratio(150) will resize image to 150% from the original scale
   	 *
   	 * @return class object
   	 */
   	public function ratio($percent)
   	{
      	$width 	= $this->getWidth() * $percent/100;
      	$height = $this->getheight() * $percent/100;
      	return $this->resize($width, $height);
   	}

   	public function copy($path, $newName = null, $deleteOrigin = false)
   	{
   		if ( empty($this->image) ){
   			echo "<b>Copy error:</b> no source file<br />";
   			return $this;
   		}
		
   		
   		$pathArray = str_split($path);
   		if ( end($pathArray) != DIRECTORY_SEPARATOR )
   		{
   			$path .= DIRECTORY_SEPARATOR;
   		}

   		if ( !is_dir($path) || !is_writable($path) ){return false;}
		
   		if ( !empty($newName) )
   		{
   			$tmp = explode(".", $newName);
   			$ext = end($tmp);	
   		}
   		else
   		{
   			$ext = $this->originInfo['extension'];
   		}
   		

   		//if newName is empty or newName is missing extension
   		if ( empty($newName) || strpos($newName, ".")===false || empty($ext) )
   		{
   			$newName = $this->originInfo['basename'];
   		}
   		elseif( !in_array( $ext, $this->allowedExt ) )
   		{
   			$newName = $this->originInfo['basename'];	
   		}

   		//If file exist we add a number to the end of the file name
   		$i = 0;
   		$temp = $newName;
   		while ( file_exists($path.$newName) )
   		{
   			$i++;
   			$newName = $temp;
   			$newName = basename($newName, ".".$ext);
   			$newName .= "({$i})";
   			$newName .= ".".$ext;
   		}
   		
   		$dest = imagecreatetruecolor( $this->getWidth(), $this->getHeight() );
   		
   		imagecopy( $dest, $this->image, 0, 0, 0, 0, $this->getWidth(), $this->getHeight() );
   		
   		//Save copied image
   		$this->save( $path.$newName, $this->imageType, 100);
   		
   		if ( $deleteOrigin===true )
   		{
   			unlink($this->originInfo['dirname'].DIRECTORY_SEPARATOR.$this->originInfo['basename']);
   		}
   		
   		return $path.$newName;
   	}
   	
   	/**
   	 * @method resize
   	 * @param width	- new image width
   	 * @param height- new image height
   	 * 
   	 * @desc resize is used to resize image
   	 *
   	 * @example resize(150, 150)
   	 *
   	 * @return class object
   	 */
  	public function resize($width, $height)
   	{
    	$new_image = imagecreatetruecolor($width, $height);
   		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->image = $new_image;
      
		return $this;
   }    

   /*public function resizeTransparent($width,$height)
   {
		$new_image = imagecreatetruecolor($width, $height);
		if( $this->imageType == IMAGETYPE_GIF || $this->imageType == IMAGETYPE_PNG )
		{
   			$current_transparent = imagecolortransparent($this->image);
   			if($current_transparent != -1)
   			{
   				$transparent_color = imagecolorsforindex($this->image, $current_transparent);
   				$current_transparent = imagecolorallocate($new_image, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
   				imagefill($new_image, 0, 0, $current_transparent);
   				imagecolortransparent($new_image, $current_transparent);
   			}
   			elseif( $this->imageType == IMAGETYPE_PNG)
   			{
   				imagealphablending($new_image, false);
   				$color = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
   				imagefill($new_image, 0, 0, $color);
   				imagesavealpha($new_image, true);
   			}
   		}
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->image = $new_image;
   }*/
   
}



$directory = $_SERVER['DOCUMENT_ROOT'] . 'images/';
 
$files = scandir( $directory );
$uploader = new imageProcess();

$uploader->load($directory."IMG_7319.JPG")->copy($_SERVER['DOCUMENT_ROOT']."images2/", false, null);

/*if ($files !== false)
{
	foreach($files as $file)
	{
		if ( is_dir($file) || $file=="." || $file==".." )
		{
			continue;
		}
	
		$uploader->load($directory.$file)->resizeToWidth(800)->save($directory.$file, IMAGETYPE_JPEG, 100);
	}	
}*/
?>