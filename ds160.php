<?php
$secured = true;
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/inc/base.php');

if ( !$system->checkConnected() ) {
	header ( 'location:registration.php' );
}
 
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/inc/header.php');
?>
<style>
html{
	overflow-y:scroll !important; 
}
</style>
<div class="progressBar step1"></div>
<div class="dsbox"
	style="margin: 0em auto; width: 970px; margin-bottom: 4em; margin-top: 2em;">
	<form method="post" name="ds160frm" id="ds160frm" action="sendapp.php">
		<div id="part1">
			<div class="dsbox_right"
				style="width: 47%; padding: 6px; padding-bottom: 100%; margin-bottom: -100%;">
				<div class="dstitle">פרטים אישיים</div>
				<div class="dsline">
					<label class="dslabel" for="app_name">שם פרטי באנגלית <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="app_name" name="app_name"
							placeholder="כפי שמופיע בדרכון"
							value="<?php echo isset($_POST['app_name'])?$_POST['app_name']:''; ?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="app_fname">שם משפחה באנגלית <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="app_fname" name="app_fname"
							placeholder="כפי שמופיע בדרכון"
							value="<?php echo isset($_POST['app_fname'])?$_POST['app_fname']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="app_full_name">שם מלא בעברית <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="app_full_name" name="app_full_name"
							placeholder="כפי שמופיע בדרכון"
							value="<?php echo isset($_POST['app_full_name'])?$_POST['app_full_name']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="app_previous_name">שם פרטי קודם</label>
					<div class="dsfield">
						<input type="text" id="app_previous_name" name="app_previous_name"
							placeholder="במידה ויש"
							value="<?php echo isset($_POST['app_previous_name'])?$_POST['app_previous_name']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="app_previous_fnames">שמות משפחה קודמים</label>
					<div class="dsfield">
						<input type="text" id="app_previous_fnames"
							name="app_previous_fnames" placeholder="במידה ויש"
							value="<?php echo isset($_POST['app_previous_fnames'])?$_POST['app_previous_fnames']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="gender">מין</label>
					<div class="dsfield">
						<input type="radio" name="gender" id="gender" value="male" />&nbsp;זכר&emsp;<input
							type="radio" name="gender" id="gender" value="female" />&nbsp;נקבה
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="app_id">מספר תעודת זהות ישראלית <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="app_id" name="app_id" maxlength="9" numbersonly="true" placeholder="9 ספרות כולל ספ' ביקורת"
							value="<?php echo isset($_POST['app_id'])?$_POST['app_id']:''; ?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="app_bdate">תאריך לידה</label>
					<div class="dsfield" style="width:234px; display:inline;">
						<?php
						/*<input type="text" id="app_bdate" style="direction: ltr;"
							name="app_bdate"
							value="<?php echo isset($_POST['app_bdate'])?$_POST['app_bdate']:'';?>" />*/
						?>
						<select name="bday-year" style="display:inline; width:32%;">
							<option>שנה</option>
							<?php
							for($i=date("Y"); $i>=(date("Y")-110); $i--)
							{
								echo '<option value="'.$i.'">'.$i.'</option>';
							}
							?>
						</select>


						<select name="bday-month" style="display:inline; width:32%;">
							<option>חודש</option>
							<?php
							for($i=1; $i<=12; $i++)
							{
								echo '<option value="'.$i.'">'.$i.'</option>';
							}
							?>
						</select>

						<select name="bday-day" style="display:inline; width:32%;">
							<option>יום</option>
							<?php
							for($i=1; $i<=31; $i++)
							{
								echo '<option value="'.$i.'">'.$i.'</option>';
							}
							?>
						</select>
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="status">מצב משפחתי</label>
					<div class="dsfield">
						<select id="status" name="status">
							<option value="">בחר</option>
							<option value="נשוי">נשוי</option>
							<option value="ידוע בציבור">ידוע בציבור</option>
							<option value="נישואים אזרחיים\שותפות משפחתית">נישואים
								אזרחיים\שותפות משפחתית</option>
							<option value="רווק">רווק</option>
							<option value="אלמן">אלמן</option>
							<option value="גרוש">גרוש</option>
							<option value="פרוד באופן חוקי">פרוד באופן חוקי</option>
						</select>
					</div>
				</div>
				<!-------------Hidden------------נשוי, פרוד, ידוע בציבור, נישואים אזרחיים-->
				<div id="spouse_0"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div id="deceased" class="dsline"
						style="display: none; font-weight: bold;">פרטי בן הזוג המנוח</div>
					<div id="divorced" class="dsline"
						style="display: none; font-weight: bold;">פרטי בן הזוג לשעבר</div>
					<div class="dsline">
						<label class="dslabel" for="spouse_name">שם מלא של בן\בת הזוג</label>
						<div class="dsfield">
							<input type="text" id="spouse_name" name="spouse_name"
								value="<?php echo isset($_POST['spouse_name'])?$_POST['spouse_name']:''; ?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="spouse_bdate">תאריך לידת בן\בת הזוג</label>
						<div class="dsfield">
							<input type="text" id="spouse_bdate" name="spouse_bdate"
								value="<?php echo isset($_POST['spouse_bdate'])?$_POST['spouse_bdate']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="spouse_cityofbirth">עיר וארץ לידת
							בן\בת הזוג</label>
						<div class="dsfield">
							<input type="text" id="spouse_cityofbirth"
								name="spouse_cityofbirth"
								value="<?php echo isset($_POST['spouse_cityofbirth'])?$_POST['spouse_cityofbirth']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="spouse_citizenship">אזרחות בן\בת הזוג</label>
						<div class="dsfield">
							<input type="text" id="spouse_citizenship"
								name="spouse_citizenship"
								value="<?php echo isset($_POST['spouse_citizenship'])?$_POST['spouse_citizenship']:'';?>" />
						</div>
					</div>
				</div>
				<!-------- /Hidden -------------------->

				<div id="spouse_1"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="spouse_address">כתובת בן\בת הזוג</label>
						<div class="dsfield">
							<input type="text" id="spouse_address" name="spouse_address"
								value="<?php echo isset($_POST['spouse_address'])?$_POST['spouse_address']:'';?>" />
						</div>
					</div>
				</div>

				<!-------------Hidden------------עבור גרוש-->
				<div id="spouse_2"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="wedding_date">תאריך נישואין</label>
						<div class="dsfield">
							<input type="text" id="wedding_date" name="wedding_date"
								value="<?php echo isset($_POST['wedding_date'])?$_POST['wedding_date']:''; ?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="divorce_date">תאריך גירושין</label>
						<div class="dsfield">
							<input type="text" id="divorce_date" name="divorce_date"
								value="<?php echo isset($_POST['divorce_date'])?$_POST['divorce_date']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="divorce_how">כיצד הסתיימו הנישואין?</label>
						<div class="dsfield">
							<input type="text" id="divorce_how" name="divorce_how"
								value="<?php echo isset($_POST['divorce_how'])?$_POST['divorce_how']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="divorce_country">באיזו מדינה התגרשתם?</label>
						<div class="dsfield">
							<input type="text" id="divorce_country" name="divorce_country"
								value="<?php echo isset($_POST['divorce_country'])?$_POST['divorce_country']:'';?>" />
						</div>
					</div>
				</div>
				<!-------- /Hidden -------------------->

				<div class="dsline">
					<label class="dslabel" for="app_cityob">עיר לידה</label>
					<div class="dsfield">
						<input type="text" id="app_cityob" name="app_cityob"
							value="<?php echo isset($_POST['app_cityob'])?$_POST['app_cityob']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="app_countryob">ארץ לידה <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="app_countryob" name="app_countryob"
							value="<?php echo isset($_POST['app_countryob'])?$_POST['app_countryob']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="nationality">אזרחות</label>
					<div class="dsfield">
						<input type="text" id="nationality" name="nationality"
							placeholder="לדוגמה: ישראלית"
							value="<?php echo isset($_POST['nationality'])?$_POST['nationality']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="othernationality">האם קיימת אזרחות
						נוספת?</label>
					<div class="dsfield">
						<input type="radio" id="othernationality" name="othernationality"
							value="false" />&nbsp;לא&emsp;<input type="radio"
							id="othernationality" name="othernationality" value="true" />&nbsp;כן
					</div>
				</div>
				<!-------------- Hidden ---- אזרחות נוספת -->
				<div id="other_nationality"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="2ndnationality">של איזה מדינה?</label>
						<div class="dsfield">
							<input type="text" id="2ndnationality" name="2ndnationality"
								value="<?php echo isset($_POST['2ndnationality'])?$_POST['2ndnationality']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="2ndpassport">האם הנך מחזיק בדרכון עבור
							אזרחות זו?</label>
						<div class="dsfield">
							<input type="radio" id="2ndpassport" name="2ndpassport"
								value="false" />&nbsp;לא&emsp;<input type="radio"
								id="2ndpassport" name="2ndpassport" value="true" />&nbsp;כן
						</div>
					</div>
					<div id="2ndpassport_block"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
						<div class="dsline">
							<label class="dslabel" for="2ndpassportnum">מספר דרכון של האזרחות
								הנוספת</label>
							<div class="dsfield">
								<input type="text" id="2ndpassportnum" name="2ndpassportnum"
									value="<?php echo isset($_POST['2ndpassportnum'])?$_POST['2ndpassportnum']:'';?>" />
							</div>
						</div>
					</div>
				</div>
				<!--------------------------->
			</div>

			<div class="dsbox_left"
				style="width: 50%; padding: 6px; padding-bottom: 100%; margin-bottom: -100%;">
				<div class="dstitle">פרטי דרכון</div>
				<div class="dsline">
					<label class="dslabel" for="passport">מספר דרכון <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="passport" name="passport"
							value="<?php echo isset($_POST['passport'])?$_POST['passport']:''; ?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="passport_country">ארץ הנפקה</label>
					<div class="dsfield">
						<input type="text" id="passport_country" name="passport_country"
							placeholder="כפי שמופיע בדרכון"
							value="<?php echo isset($_POST['passport_country'])?$_POST['passport_country']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="passport_city">עיר הנפקה <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="passport_city" name="passport_city"
							placeholder="כפי שמופיע בדרכון"
							value="<?php echo isset($_POST['passport_city'])?$_POST['passport_city']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="passport_dateissued">תאריך הנפקה <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="passport_dateissued"
							name="passport_dateissued"
							value="<?php echo isset($_POST['passport_dateissued'])?$_POST['passport_dateissued']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="passport_dateexpire">תאריך פקיעת תוקף <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="passport_dateexpire"
							name="passport_dateexpire"
							value="<?php echo isset($_POST['passport_dateexpire'])?$_POST['passport_dateexpire']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="passport_lost">האם אי פעם דרכונך אבד או
						נגנב?</label>
					<div class="dsfield">
						<input type="radio" name="passport_lost" id="passport_lost"
							value="false" />&nbsp;לא&emsp;<input type="radio"
							name="passport_lost" id="passport_lost" value="true" />&nbsp;כן
					</div>
				</div>
				<!--------- Hidden ---- Lost passport------->
				<div id="lost_passport"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="lost_passport_num">מספר הדרכון
							שנגנב\אבד</label>
						<div class="dsfield">
							<input type="text" id="lost_passport_num"
								name="lost_passport_num" placeholder="במידה וידוע"
								value="<?php echo isset($_POST['lost_passport_num'])?$_POST['lost_passport_num']:''; ?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="lost_passport_country">ארץ בה הונפק
							הדרכון שנגנב</label>
						<div class="dsfield">
							<input type="text" id="lost_passport_country"
								name="lost_passport_country"
								value="<?php echo isset($_POST['lost_passport_country'])?$_POST['lost_passport_country']:''; ?>" />
						</div>
					</div>
				</div>
				<!---------------------------->

				<div class="dstitle">כתובות וטלפונים</div>
				<div class="dsline">
					<label class="dslabel" for="cellular">טלפון נייד <span
						class="asterisk">*</span></label>
					<div class="dsfield">
						<input type="text" id="cellular" name="cellular" maxlength="12"
							value="<?php echo isset($_POST['cellular'])?$_POST['cellular']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="phone">טלפון משני</label>
					<div class="dsfield">
						<input type="text" id="phone" name="phone"
							placeholder="במידה וקיים"
							value="<?php echo isset($_POST['phone'])?$_POST['phone']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="street">רחוב</label>
					<div class="dsfield">
						<input type="text" id="street" name="street"
							value="<?php echo isset($_POST['street'])?$_POST['street']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="housenum">מספר בית</label>
					<div class="dsfield">
						<input type="text" id="housenum" name="housenum"
							value="<?php echo isset($_POST['housenum'])?$_POST['housenum']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="apt_num">מספר דירה</label>
					<div class="dsfield">
						<input type="text" id="apt_num" name="apt_num"
							placeholder="במידה ורלוונטי"
							value="<?php echo isset($_POST['apt_num'])?$_POST['apt_num']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="city">עיר</label>
					<div class="dsfield">
						<input type="text" id="city" name="city"
							value="<?php echo isset($_POST['city'])?$_POST['city']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="country">מדינה</label>
					<div class="dsfield">
						<input type="text" id="country" name="country"
							value="<?php echo isset($_POST['country'])?$_POST['country']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="zipcode">מיקוד</label>
					<div class="dsfield">
						<input type="text" id="zipcode" name="zipcode"
							value="<?php echo isset($_POST['zipcode'])?$_POST['zipcode']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="other_address">להשתמש בכתובת למשלוח
						דואר?</label>
					<div class="dsfield">
						<input type="radio" name="other_address" id="other_address"
							value="true" />&nbsp;כן&emsp;<input type="radio"
							name="other_address" id="other_address" value="false" />&nbsp;לא
					</div>
				</div>
				<!--------- Hidden ---- Lost passport------->
				<div id="mail_address"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="mail_street">רחוב</label>
						<div class="dsfield">
							<input type="text" id="mail_street" name="mail_street"
								value="<?php echo isset($_POST['mail_street'])?$_POST['mail_street']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="mail_housenum">מספר בית</label>
						<div class="dsfield">
							<input type="text" id="mail_housenum" name="mail_housenum"
								value="<?php echo isset($_POST['mail_housenum'])?$_POST['mail_housenum']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="mail_apt_num">מספר דירה</label>
						<div class="dsfield">
							<input type="text" id="mail_apt_num" name="mail_apt_num"
								value="<?php echo isset($_POST['mail_apt_num'])?$_POST['mail_apt_num']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="mail_city">עיר</label>
						<div class="dsfield">
							<input type="text" id="mail_city" name="mail_city"
								value="<?php echo isset($_POST['mail_city'])?$_POST['mail_city']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="mail_country">מדינה</label>
						<div class="dsfield">
							<input type="text" id="mail_country" name="mail_country"
								value="<?php echo isset($_POST['mail_country'])?$_POST['mail_country']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="mail_zipcode">מיקוד</label>
						<div class="dsfield">
							<input type="text" id="mail_zipcode" name="mail_zipcode"
								value="<?php echo isset($_POST['mail_zipcode'])?$_POST['mail_zipcode']:'';?>" />
						</div>
					</div>
				</div>
				<!---------------------------->
			</div>

			<div class="bottom"
				style="display: block; position: relative; clear: both; height: 70px; margin: 1em auto; clear: both;">
				<button id="1next" class="ds_button_next"
					style="display: block; float: left; clear: both;"></button>
			</div>
			<div style="clear: both;"></div>
		</div>

		<div id="part2" style="display: none;">
			<div class="dsbox_right"
				style="width: 47%; padding: 6px; padding-bottom: 100%; margin-bottom: -100%;">
				<div class="dstitle">פרטי משפחה</div>
				<div class="dsline">
					<label class="dslabel" for="father_name">שם מלא של האב באנגלית</label>
					<div class="dsfield">
						<input type="text" id="father_name" name="father_name"
							placeholder="במידה וידוע"
							value="<?php echo isset($_POST['father_name'])?$_POST['father_name']:''; ?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="father_bdate">תאריך לידה</label>
					<div class="dsfield">
						<input type="text" id="father_bdate" name="father_bdate"
							placeholder="במידה וידוע"
							value="<?php echo isset($_POST['father_bdate'])?$_POST['father_bdate']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="father_in_usa">האם הוא נמצא כעת בארה"ב?</label>
					<div class="dsfield">
						<input type="radio" name="father_in_usa" id="father_in_usa"
							value="false" />&nbsp;לא&emsp;<input type="radio"
							name="father_in_usa" id="father_in_usa" value="true" />&nbsp;כן
					</div>
				</div>
				<!---- Hidden ------------->
				<div id="father"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="father_status">מה מעמדו?</label>
						<div class="dsfield">
							<select id="father_status" id="father_status">
								<option value="" selected="selected"></option>
								<option value="אזרח אמריקאי">אזרח אמריקאי</option>
								<option value="תושב קבע">תושב קבע - Green Card</option>
								<option value="תייר\סטודנט\עבודה">תייר\סטודנט\עבודה</option>
								<option value="לא ידוע">לא ידוע</option>
							</select>
						</div>
					</div>
				</div>
				<!------------------------->

				<div class="dsline">
					<label class="dslabel" for="app_name">שם מלא של האם באנגלית</label>
					<div class="dsfield">
						<input type="text" id="mother_name" name="mother_name"
							placeholder="במידה וידוע"
							value="<?php echo isset($_POST['mother_name'])?$_POST['mother_name']:''; ?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="mother_bdate">תאריך לידה</label>
					<div class="dsfield">
						<input type="text" id="mother_bdate" name="mother_bdate"
							placeholder="במידה וידוע"
							value="<?php echo isset($_POST['mother_bdate'])?$_POST['mother_bdate']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="mother_in_usa">האם היא נמצאת כרגע
						בארה"ב?</label>
					<div class="dsfield">
						<input type="radio" name="mother_in_usa" id="mother_in_usa"
							value="false" />&nbsp;לא&emsp;<input type="radio"
							name="mother_in_usa" id="mother_in_usa" value="true" />&nbsp;כן
					</div>
				</div>
				<!---- Hidden ------------->
				<div id="mother"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="mother_status">מה מעמדה?</label>
						<div class="dsfield">
							<select id="mother_status" name="mother_status">
								<option value=""></option>
								<option value="אזרח אמריקאי">אזרח אמריקאי</option>
								<option value="תושב קבע">תושב קבע - Green Card</option>
								<option value="תייר\סטודנט\עבודה">תייר\סטודנט\עבודה</option>
								<option value="לא ידוע">לא ידוע</option>
							</select>
						</div>
					</div>
				</div>
				<!------------------------->
				<div class="dsline" style="padding: 0.2em;">
					<label class="dslabel line" for="1st_relative_exist">האם קיים קרוב
						משפחה מדרגה ראשונה בארה"ב? (לא כולל הורים)</label>
					<div class="dsfield line">
						<input type="radio" id="1st_relative_exist"
							name="1st_relative_exist" value="false" />&nbsp;לא&emsp;<input
							type="radio" id="1st_relative_exist" name="1st_relative_exist"
							value="true" />&nbsp;כן
					</div>
				</div>
				<!---- Hidden ------------->
				<div id="1st_relative"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="1st_relative_name">שם מלא</label>
						<div class="dsfield">
							<input type="text" id="1st_relative_name"
								name="1st_relative_name"
								value="<?php echo isset($_POST['1st_relative_name'])?$_POST['1st_relative_name']:''; ?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="1st_relative_relation">קרבה</label>
						<div class="dsfield">
							<select id="1st_relative_relation" name="1st_relative_relation">
								<option value="">אח\ות</option>
								<option value="">בעל\אישה</option>
								<option value="">בן\בת</option>
							</select>
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="1st_relative_status">מה מעמדו?</label>
						<div class="dsfield">
							<select id="1st_relative_status" name="1st_relative_status">
								<option value=""></option>
								<option value="אזרח אמריקאי">אזרח אמריקאי</option>
								<option value="תושב קבע">תושב קבע - Green Card</option>
								<option value="תייר\סטודנט\עבודה">תייר\סטודנט\עבודה</option>
								<option value="לא ידוע">לא ידוע</option>
							</select>
						</div>
					</div>
				</div>
				<!------------------------->
				<div class="dsline" style="margin-top: 1em;">
					<label class="dslabel line" for="2nd_relative_exist">האם קיימים
						קרובי משפחה נוספים בארה"ב אשר אינם מקרבה ראשונה?</label>
					<div class="dsfield line">
						<input type="radio" id="2nd_relative_exist"
							name="2nd_relative_exist" value="false" />&nbsp;לא&emsp;<input
							type="radio" id="2nd_relative_exist" name="2nd_relative_exist"
							value="true" />&nbsp;כן
					</div>
				</div>

				<div class="dstitle">נסיעה</div>

				<div class="dsline">
					<label class="dslabel" for="travel_purpose">מטרת הנסיעה</label>
					<div class="dsfield">
						<input type="text" id="travel_purpose" name="travel_purpose"
							placeholder="לדוגמה: תיירות\עסקים\רפואי"
							value="<?php echo isset($_POST['travel_purpose'])?$_POST['travel_purpose']:''; ?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="estimated_doa">תאריך הגעה משוער</label>
					<div class="dsfield">
						<input type="text" id="estimated_doa" name="estimated_doa"
							value="<?php echo isset($_POST['estimated_doa'])?$_POST['estimated_doa']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="estimated_tos">זמן שהות משוער בארה"ב</label>
					<div class="dsfield">
						<input type="text" id="estimated_tos" name="estimated_tos"
							placeholder="לדוגמה: 19 יום, שבועיים, חודש"
							value="<?php echo isset($_POST['estimated_tos'])?$_POST['estimated_tos']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="know_where_staying">האם ידוע היכן תשהה
						בארה"ב?</label>
					<div class="dsfield">
						<input type="radio" id="know_where_staying"
							name="know_where_staying" value="false" />&nbsp;לא&emsp;<input
							type="radio" id="know_where_staying" name="know_where_staying"
							value="true" />&nbsp;כן
					</div>
				</div>
				<!---- Hidden ------------->
				<div id="staying_place"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em; margin-right: 0;">
					<div class="dsline">
						<label class="dslabel" for="staying_street">רחוב</label>
						<div class="dsfield">
							<input type="text" id="staying_street" name="staying_street"
								placeholder="במידה ולא ידוע ציין 'בתי מלון'"
								value="<?php echo isset($_POST['staying_street'])?$_POST['staying_street']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="staying_city">עיר</label>
						<div class="dsfield">
							<input type="text" id="staying_city" name="staying_city"
								placeholder="לדוגמה: לוס אנג'לס"
								value="<?php echo isset($_POST['staying_city'])?$_POST['staying_city']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="staying_state">מדינה</label>
						<div class="dsfield">
							<input type="text" id="staying_state" name="staying_state"
								placeholder="לדוגמה:קליפורניה"
								value="<?php echo isset($_POST['staying_state'])?$_POST['staying_state']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="staying_zipcode">מיקוד</label>
						<div class="dsfield">
							<input type="text" id="staying_zipcode" numbersonly="true" name="staying_zipcode"
								placeholder="במידה וידוע"
								value="<?php echo isset($_POST['staying_zipcode'])?$_POST['staying_zipcode']:'';?>" />
						</div>
					</div>
				</div>
				<!----------->
				<!---- Hidden ------------->
				<div id="favorite_dest"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em; margin-right: 0;">
					<div class="dsline">
						<label class="dslabel" for="favoritedest">יעד נסיעה מועדף</label>
						<div class="dsfield">
							<input type="text" id="favoritedest" name="favoritedest"
								placeholder="לדוגמה: מיאמי, פלורידה"
								value="<?php echo isset($_POST['favoritedest'])?$_POST['favoritedest']:'';?>" />
						</div>
					</div>
				</div>
				<!----------->

				<div class="dsline">
					<label class="dslabel" for="us_contact">האם קיים איש קשר בארה"ב?</label>
					<div class="dsfield">
						<input type="radio" id="us_contact" name="us_contact"
							value="false" />&nbsp;לא&emsp;<input type="radio" id="us_contact"
							name="us_contact" value="true" />&nbsp;כן
					</div>
				</div>
				<!-------------------->
				<div id="uscontact"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 0.5em; margin-right: 0;">
					<div class="dsline">
						<label class="dslabel" for="us_contact_name">שם מלא \ שם
							חברה\ארגון</label>
						<div class="dsfield">
							<input type="text" id="us_contact_name" name="us_contact_name"
								value="<?php echo isset($_POST['us_contact_name'])?$_POST['us_contact_name']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="us_contact_relation">יחס קרבה</label>
						<div class="dsfield">
							<input type="text" id="us_contact_relation"
								name="us_contact_relation" placeholder="לדוגמה: בן דוד"
								value="<?php echo isset($_POST['us_contact_relation'])?$_POST['us_contact_relation']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="us_contact_addr">כתובת מלאה בארה"ב</label>
						<div class="dsfield">
							<textarea
								style="width: 200px; height: 80px; overflow-x: hidden; resize: none;"
								name="us_contact_addr" id="us_contact_addr"><?php echo isset($_POST['us_contact_addr'])?$_POST['us_contact_addr']:'';?></textarea>
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="us_contact_phone">טלפון בארה"ב</label>
						<div class="dsfield">
							<input type="text" id="us_contact_phone" name="us_contact_phone"
								placeholder="במידה וקיים"
								value="<?php echo isset($_POST['us_contact_phone'])?$_POST['us_contact_phone']:'';?>" />
						</div>
					</div>
				</div>
				<!--------------->

				<div class="dsline">
					<label class="dslabel" for="sponser">מי מממן את נסיעתך?</label>
					<div class="dsfield">
						<select id="sponser" name="sponser">
							<option value="">אני</option>
							<option value="">מעסיק</option>
							<option value="ארגון">ארגון</option>
							<option value="אדם אחר">אדם אחר</option>
						</select>
					</div>
				</div>


				<!---- Hidden ------------->
				<div id="sponser_details"
					style="width: 100% -10px; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em; padding-right: 10px;">
					<div class="dsline">
						<label class="dslabel" for="sponser_name">שם מלא\שם הארגון</label>
						<div class="dsfield">
							<input type="text" id="sponser_name" name="sponser_name"
								placeholder="לדוגמה: הסוכנות הציונית"
								value="<?php echo isset($_POST['sponser_name'])?$_POST['sponser_name']:''; ?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="sponser_phone">טלפון</label>
						<div class="dsfield">
							<input type="text" id="sponser_phone" name="sponser_phone"
								value="<?php echo isset($_POST['sponser_phone'])?$_POST['sponser_phone']:''; ?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="sponser_addr">כתובת</label>
						<div class="dsfield">
							<input type="text" id="sponser_addr" name="sponser_addr"
								value="<?php echo isset($_POST['sponser_addr'])?$_POST['sponser_addr']:''; ?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="sponser_relation">יחס קרבה</label>
						<div class="dsfield">
							<input type="text" id="sponser_relation" name="sponser_relation"
								placeholder="לדוגמה: מעסיק, שותף עסקי"
								value="<?php echo isset($_POST['sponser_relation'])?$_POST['sponser_relation']:''; ?>" />
						</div>
					</div>
				</div>
				<!------------------------->
				<div class="dsline">
					<label class="dslabel" for="other_people">נוסעים איתך אנשים נוספים?</label>
					<div class="dsfield">
						<input type="radio" id="other_people" name="other_people"
							value="false" />&nbsp;לא&emsp;<input type="radio"
							id="other_people" name="other_people" value="true" />&nbsp;כן
					</div>
				</div>
				<!---- Hidden -------------->
				<div id="travelmates"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="partofgroup">האם אתה נוסע כחלק
							מקבוצה\ארגון?</label>
						<div class="dsfield">
							<input type="radio" id="partofgroup" name="partofgroup"
								value="false" />&nbsp;לא&emsp;<input type="radio"
								id="partofgroup" name="partofgroup" value="true" />&nbsp;כן
						</div>
					</div>
					<!---- Hidden -------------->
					<div id="organization"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
						<div class="dsline">
							<label class="dslabel" for="organization_name">שם הקבוצה\ארגון</label>
							<div class="dsfield">
								<input type="text" id="organization_name"
									name="organization_name" placeholder="לדוגמה: אופקים טורס"
									value="<?php echo isset($_POST['sponser_relation'])?$_POST['sponser_relation']:''; ?>" />
							</div>
						</div>
					</div>
					<!---- /Hidden ------------->
					<!---- Hidden -------------->
					<div id="addpeople"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em; min-height: 160px; height: 160px;">
						<div class="dsline">
							<label class="dslabel" for="people_names">שמות האנשים הנוסעים
								איתך ויחס הקרבה</label>
							<div class="dsfield">
								<textarea id="people_names" name="people_names"
									style="width: 210px; height: 150px; overflow-x: hidden; resize: none;"
									placeholder="לדוגמה: שמעון פרס (חבר), בנימין נתניהו (אבא)"><?php echo isset($_POST['sponser_relation'])?$_POST['sponser_relation']:''; ?></textarea>
							</div>
						</div>
					</div>
					<!---- /Hidden ------------->
				</div>
				<!---- /Hidden ------------->
			</div>

			<div class="dsbox_left"
				style="width: 50%; padding: 6px; padding-bottom: 100%; margin-bottom: -100%;">
				<div class="dstitle">ביקורים קודמים בארצות הברית</div>
				<div class="dsline wider">
					<label class="dslabel" for="visited_before">האם ביקרת בעבר בארה"ב?</label>
					<div class="dsfield">
						<input type="radio" id="visited_before" name="visited_before"
							value="false" />&nbsp;לא&emsp;<input type="radio"
							id="visited_before" name="visited_before" value="true" />&nbsp;כן
					</div>
				</div>
				<!----------- Previous visits - Hidden --------------->
				<div id="visits" style="display: none;">
					<div
						style="width: 100% -6px; padding-right: 6px; margin-bottom: 10px; color: #676767; font-weight: bold; background: #e8e8e8;">
						פרט 5 ביקורים אחרונים בארה"ב (נא להתחיל מהביקור האחרון)</div>

					<div id="visit1"
						style="display: inline-block; height: 120px; padding: 4px; border-bottom: 1px dashed #e5e5e5;">
						<div class="dsline">
							<label class="dslabel" for="visit1_doa">תאריך הגעה</label>
							<div class="dsfield">
								<input type="text" id="visit1_doa" name="visit1_doa"
									placeholder="במידה ולא ידוע במדוייק יש לציין בקירוב"
									value="<?php echo isset($_POST['visit1_doa'])?$_POST['visit1_doa']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="visit1_period">משך השהות</label>
							<div class="dsfield">
								<input type="text" id="visit1_period" name="visit1_period"
									placeholder="במידה ולא ידוע במדוייק יש לציין בקירוב"
									value="<?php echo isset($_POST['visit1_period'])?$_POST['visit1_period']:'';?>" />
							</div>
						</div>
						<input class="smallbtn" value="הוסף ביקור נוסף"
							onclick="$('#visit2').slideDown('quick'); return false;">

					</div>

					<div id="visit2"
						style="display: none; height: 120px; padding: 4px; border-bottom: 1px dashed #e5e5e5;">
						<div class="dsline">
							<label class="dslabel" for="visit2_doa">תאריך הגעה</label>
							<div class="dsfield">
								<input type="text" id="visit2_doa" name="visit2_doa"
									value="<?php echo isset($_POST['visit2_doa'])?$_POST['visit2_doa']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="visit2_period">משך השהות</label>
							<div class="dsfield">
								<input type="text" id="visit2_period" name="visit2_period"
									value="<?php echo isset($_POST['visit2_period'])?$_POST['visit2_period']:'';?>" />
							</div>
						</div>
						<input value="הוסף ביקור נוסף" class="smallbtn"
							onclick="$('#visit3').slideDown('quick');return false;"> <input
							class="smallbtn"
							onclick="$('#visit2').slideUp('quick');return false;"
							style="width: 50px;" value="הסר" />
					</div>

					<div id="visit3"
						style="display: none; height: 120px; padding: 4px; border-bottom: 1px dashed #e5e5e5;">
						<div class="dsline">
							<label class="dslabel" for="visit3_doa">תאריך הגעה</label>
							<div class="dsfield">
								<input type="text" id="visit3_doa" name="visit3_doa"
									value="<?php echo isset($_POST['visit3_doa'])?$_POST['visit3_doa']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="visit3_period">משך השהות</label>
							<div class="dsfield">
								<input type="text" id="visit3_period" name="visit3_period"
									value="<?php echo isset($_POST['visit3_period'])?$_POST['visit3_period']:'';?>" />
							</div>
						</div>
						<input value="הוסף ביקור נוסף" class="smallbtn"
							onclick="$('#visit4').slideDown('quick');return false;"> <input
							class="smallbtn"
							onclick="$('#visit3').slideUp('quick');return false;"
							style="width: 50px;" value="הסר" />
					</div>

					<div id="visit4"
						style="display: none; height: 120px; padding: 4px; border-bottom: 1px dashed #e5e5e5;">
						<div class="dsline">
							<label class="dslabel" for="visit4_doa">תאריך הגעה</label>
							<div class="dsfield">
								<input type="text" id="visit4_doa" name="visit4_doa"
									value="<?php echo isset($_POST['visit4_doa'])?$_POST['visit4_doa']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="visit4_period">משך השהות</label>
							<div class="dsfield">
								<input type="text" id="visit4_period" name="visit4_period"
									value="<?php echo isset($_POST['visit4_period'])?$_POST['visit4_period']:'';?>" />
							</div>
						</div>
						<input value="הוסף ביקור נוסף" class="smallbtn"
							onclick="$('#visit5').slideDown('quick');return false;"> <input
							class="smallbtn"
							onclick="$('#visit4').slideUp('quick');return false;"
							style="width: 50px;" value="הסר">
					</div>

					<div id="visit5"
						style="display: none; height: 80px; padding: 4px; border-bottom: 1px dashed #e5e5e5;">
						<div class="dsline">
							<label class="dslabel" for="visit5_doa">תאריך הגעה</label>
							<div class="dsfield">
								<input type="text" id="visit5_doa" name="visit5_doa"
									value="<?php echo isset($_POST['visit5_doa'])?$_POST['visit5_doa']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="visit5_period">משך השהות</label>
							<div class="dsfield">
								<input type="text" id="visit5_period" name="visit5_period"
									value="<?php echo isset($_POST['visit5_period'])?$_POST['visit5_period']:'';?>" />
							</div>
						</div>
						<input class="smallbtn"
							onclick="$('#visit5').slideUp('quick');return false;"
							style="width: 50px;" value="הסר" />
					</div>
				</div>
				<!----------- /Previous visits ------------------------->
				<div class="dsline wider">
					<label class="dslabel" for="had_dl">האם החזקת בעבר ברישיון נהיגה
						אמריקאי?</label>
					<div class="dsfield">
						<input type="radio" id="had_dl" name="had_dl" value="false" />&nbsp;לא&emsp;<input
							type="radio" id="had_dl" name="had_dl" value="true" />&nbsp;כן
					</div>
				</div>
				<!---- Hidden ------------->
				<div id="driving_license"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em; margin-right: 0;">
					<div class="dsline">
						<label class="dslabel" for="license_number">מספר רישיון נהיגה
							אמריקאי</label>
						<div class="dsfield">
							<input type="text" id="license_number" name="license_number"
								placeholder="במידה וידוע"
								value="<?php echo isset($_POST['license_number'])?$_POST['license_number']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="license_stateoi">מדינה בה הונפק</label>
						<div class="dsfield">
							<input type="text" id="license_stateoi" name="license_stateoi"
								placeholder="במידה וידוע, לדוגמה: קליפורניה"
								value="<?php echo isset($_POST['license_stateoi'])?$_POST['license_stateoi']:'';?>" />
						</div>
					</div>
				</div>
				<!-----/hidden ----------->

				<div class="dsline wider">
					<label class="dslabel" for="had_dl">האם הונפקה לך בעבר ויזה לארה"ב?</label>
					<div class="dsfield">
						<input type="radio" id="had_visa" name="had_visa" value="false" />&nbsp;לא&emsp;<input
							type="radio" id="had_visa" name="had_visa" value="true" />&nbsp;כן
					</div>
				</div>
				<!----------- Previous visa - Hidden --------------->
				<div id="previous_visa"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em; margin-right: 0;">
					<div class="dsline">
						<label class="dslabel" for="visa_doi">תאריך הנפקה</label>
						<div class="dsfield">
							<input type="text" id="visa_doi" name="visa_doi"
								placeholder="במידה ולא ידוע ציין שנת הנפקה בקירוב"
								value="<?php echo isset($_POST['visa_doi'])?$_POST['visa_doi']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="visa_num">מספר הויזה לארה"ב</label>
						<div class="dsfield">
							<input type="text" id="visa_num" name="visa_num"
								placeholder="במידה וידוע"
								value="<?php echo isset($_POST['visa_num'])?$_POST['visa_num']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="visa_type">סוג הויזה לארה"ב</label>
						<div class="dsfield">
							<input type="text" id="visa_type" name="visa_type"
								placeholder="לדוגמה: ויזת תייר"
								value="<?php echo isset($_POST['visa_type'])?$_POST['visa_type']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="visa_placeoi">עיר ומדינה בה הונפקה
							הויזה</label>
						<div class="dsfield">
							<input type="text" id="visa_placeoi" name="visa_placeoi"
								placeholder="לדוגמה: תל-אביב, ישראל"
								value="<?php echo isset($_POST['visa_placeoi'])?$_POST['visa_placeoi']:'';?>" />
						</div>
					</div>
					<div class="dsline wider">
						<label class="dslabel" for="finger_print">האם נלקחו ממך טביעות
							אצבע?</label>
						<div class="dsfield">
							<input type="radio" id="finger_print" name="finger_print"
								value="false" />&nbsp;לא&emsp;<input type="radio"
								id="finger_print" name="finger_print" value="true" />&nbsp;כן
						</div>
					</div>
					<div class="dsline wider">
						<label class="dslabel" for="visa_stolen">האם נגנבה ממך ויזה?</label>
						<div class="dsfield">
							<input type="radio" id="visa_stolen" name="visa_stolen"
								value="false" />&nbsp;לא&emsp;<input type="radio"
								id="visa_stolen" name="visa_stolen" value="true" />&nbsp;כן
						</div>
					</div>
					<!-------------------->
					<div id="stolen"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 0.5em; margin-right: 0;">
						<div class="dsline">
							<label class="dslabel" for="stolen_year">שנת גניבה</label>
							<div class="dsfield">
								<input type="text" id="stolen_year" name="stolen_year"
									placeholder="במידה וידוע"
									value="<?php echo isset($_POST['stolen_year'])?$_POST['stolen_year']:'';?>" />
							</div>
						</div>
					</div>
					<!--------------->


					<div class="dsline wider">
						<label class="dslabel" for="visa_revoked">האם נשללה\בוטלה לך ויזה?</label>
						<div class="dsfield">
							<input type="radio" id="visa_revoked" name="visa_revoked"
								value="false" />&nbsp;לא&emsp;<input type="radio"
								id="visa_revoked" name="visa_revoked" value="true" />&nbsp;כן
						</div>
					</div>
					<!-------------------->
					<div id="revoked"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 0.5em; margin-right: 0;">
						<div class="dsline">
							<label class="dslabel" for="revoked_reason">סיבה</label>
							<div class="dsfield">
								<input type="text" id="revoked_reason" name="revoked_reason"
									value="<?php echo isset($_POST['revoked_reason'])?$_POST['revoked_reason']:'';?>" />
							</div>
						</div>
					</div>
					<!--------------->
				</div>
				<!----------- /Previous visa ------------------------->

				<div class="dsline wider">
					<label class="dslabel" for="visa_rejected">האם אי פעם סורבה לך ויזה
						לארה"ב?</label>
					<div class="dsfield">
						<input type="radio" id="visa_rejected" name="visa_rejected"
							value="false" />&nbsp;לא&emsp;<input type="radio"
							id="visa_rejected" name="visa_rejected" value="true" />&nbsp;כן
					</div>
				</div>
				<!-------------------->
				<div id="visa_rejected_block"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 0.5em; margin-right: 0;">
					<div class="dsline">
						<label class="dslabel" for="rejected_reason">סיבה</label>
						<div class="dsfield">
							<input type="text" id="rejected_reason" name="rejected_reason"
								placeholder="פרט מדוע"
								value="<?php echo isset($_POST['rejected_reason'])?$_POST['rejected_reason']:'';?>" />
						</div>
					</div>
				</div>
				<!--------------->


				<div class="dsline wider">
					<label class="dslabel" for="entrance_denied">האם אי פעם סורבה
						כניסתך לארה"ב או נסוגת מבקשתך להכנס לארה"ב בנוקדת הכניסה בגבולות
						ארה"ב?</label>
					<div class="dsfield">
						<input type="radio" id="entrance_denied" name="entrance_denied"
							value="false" />&nbsp;לא&emsp;<input type="radio"
							id="entrance_denied" name="entrance_denied" value="true" />&nbsp;כן
					</div>
				</div>
				<!-------------------->
				<div id="entrance_denied_block"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 0.5em; margin-right: 0;">
					<div class="dsline">
						<label class="dslabel" for="entrance_denied_reason">סיבה</label>
						<div class="dsfield">
							<input type="text" id="entrance_denied_reason"
								name="entrance_denied_reason" placeholder="פרט מדוע"
								value="<?php echo isset($_POST['entrance_denied_reason'])?$_POST['entrance_denied_reason']:'';?>" />
						</div>
					</div>
				</div>
				<!--------------->

				<div class="dsline wider">
					<label class="dslabel" for="immigration_request">האם הוגשה בשמך
						בקשה להגירה לארה"ב?</label>
					<div class="dsfield">
						<input type="radio" id="immigration_request"
							name="immigration_request" value="false" />&nbsp;לא&emsp;<input
							type="radio" id="immigration_request" name="immigration_request"
							value="true" />&nbsp;כן
					</div>
				</div>
				<!-------------------->
				<div id="immigration_block"
					style="width: 100%; display: none; background: #FAFAFA; padding-top: 0.5em; margin-right: 0;">
					<div class="dsline">
						<label class="dslabel" for="immigration_info">פרט</label>
						<div class="dsfield">
							<input type="text" id="immigration_info" name="immigration_info"
								value="<?php echo isset($_POST['immigration_info'])?$_POST['immigration_info']:'';?>" />
						</div>
					</div>
				</div>
				<!--------------->
			</div>

			<div style="clear: both;">&nbsp;</div>

			<div class="bottom"
				style="display: block; position: relative; clear: both; height: 70px; margin: 1em auto; clear: both;">
				<button id="2previous" class="ds_button_previous" style="float: right;"></button> 
				<button id="2next" class="ds_button_next" style="float: left;"></button>
			</div>
		</div>

		<div id="part3" style="display: none;">
			<div class="dsbox_right" style="width: 47%; padding: 6px;">
				<div class="dstitle">עיסוק עיקרי</div>
				<div class="dsline">
					<label class="dslabel" for="work_primary">עיסוק עיקרי</label>
					<div class="dsfield">
						<input type="text" id="work_primary" name="work_primary"
							placeholder="במידה והינך מובטל פרט מדוע"
							value="<?php echo isset($_POST['work_primary'])?$_POST['work_primary']:''; ?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="employer">שם מעסיק \ מוסד לימודים</label>
					<div class="dsfield">
						<input type="text" id="employer" name="employer"
							value="<?php echo isset($_POST['employer'])?$_POST['employer']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="work_address">כתובת מלאה</label>
					<div class="dsfield">
						<input type="text" id="work_address" name="work_address"
							value="<?php echo isset($_POST['work_address'])?$_POST['work_address']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="work_phone">טלפון בעבודה</label>
					<div class="dsfield">
						<input type="text" id="work_phone" name="work_phone"
							placeholder="במידה ואין רשום מספר נייד"
							value="<?php echo isset($_POST['work_phone'])?$_POST['work_phone']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="average_salary">משכורת חודשית ממוצעת
						בשקלים</label>
					<div class="dsfield">
						<input type="text" id="average_salary" name="average_salary"
							placeholder="במידה ורלוונטי"
							value="<?php echo isset($_POST['average_salary'])?$_POST['average_salary']:'';?>" />
					</div>
				</div>
				<div class="dsline">
					<label class="dslabel" for="job_description">תיאור התפקיד בקצרה</label>
					<div class="dsfield">
						<input type="text" id="job_description" name="job_description"
							placeholder="במידה ורלוונטי"
							value="<?php echo isset($_POST['job_description'])?$_POST['job_description']:'';?>" />
					</div>
				</div>
				<div id="mensonly1" style="display: none;">
					<div class="dsline wider">
						<label class="dslabel" for="other_job">האם עבדת בעבודה נוספת ב-5
							השנים האחרונות?</label>
						<div class="dsfield">
							<input type="radio" name="other_job" id="other_job" value="true" />&nbsp;כן&emsp;<input
								type="radio" name="other_job" id="other_job" value="false" />&nbsp;לא
						</div>
					</div>
					<!----------------- Hidden -------------------->
					<div id="otherjob"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
						<div class="dsline">
							<label class="dslabel" for="previous_employer">שם חברה \ שם מעסיק</label>
							<div class="dsfield">
								<input type="text" id="previous_employer"
									name="previous_employer"
									value="<?php echo isset($_POST['previous_employer'])?$_POST['previous_employer']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="previous_work_address">כתובת מלאה</label>
							<div class="dsfield">
								<input type="text" id="previous_work_address"
									name="previous_work_address"
									value="<?php echo isset($_POST['previous_work_address'])?$_POST['previous_work_address']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="previous_work_phone">מספר טלפון</label>
							<div class="dsfield">
								<input type="text" id="previous_work_phone"
									name="previous_work_phone" placeholder="במידה ואין השאר ריק"
									value="<?php echo isset($_POST['previous_work_phone'])?$_POST['previous_work_phone']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="previous_work_manager">שם מנהל קודם</label>
							<div class="dsfield">
								<input type="text" id="previous_work_manager"
									name="previous_work_manager"
									value="<?php echo isset($_POST['previous_work_manager'])?$_POST['previous_work_manager']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="previous_work_start">תאריך התחלה</label>
							<div class="dsfield">
								<input type="text" id="previous_work_start"
									name="previous_work_start"
									value="<?php echo isset($_POST['previous_work_start'])?$_POST['previous_work_start']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="previous_work_end">תאריך התחלה</label>
							<div class="dsfield">
								<input type="text" id="previous_work_end"
									name="previous_work_end"
									value="<?php echo isset($_POST['previous_work_end'])?$_POST['previous_work_end']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="previous_job_description">תפקיד</label>
							<div class="dsfield">
								<input type="text" id="previous_job_description"
									name="previous_job_description"
									placeholder="מנהל שיווק \ עובד מחלקת ייצור"
									value="<?php echo isset($_POST['previous_job_description'])?$_POST['previous_job_description']:'';?>" />
							</div>
						</div>
					</div>
					<!-----------------/Hidden---------------------->
				</div>
			</div>

			<div class="dsbox_left" style="width: 50%; padding: 6px;">
				<div class="dstitle">לימודים</div>
				<div class="dsline wider">
					<label class="dslabel" for="is_student">האם הנך סטודנט\תלמיד?</label>
					<input type="radio" name="is_student" id="is_student" value="true" />&nbsp;כן&emsp;<input
						type="radio" name="is_student" id="is_student" value="false" />&nbsp;לא
				</div>
				<!-------------- Hidden ---- אזרחות נוספת -->
				<div id="student"
					style="display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
					<div class="dsline">
						<label class="dslabel" for="school_name">שם מוסד הלימודים</label>
						<div class="dsfield">
							<input type="text" id="school_name" name="school_name"
								value="<?php echo isset($_POST['school_name'])?$_POST['school_name']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="school_addr">כתובת</label>
						<div class="dsfield">
							<input type="text" id="school_addr" name="school_addr"
								value="<?php echo isset($_POST['school_addr'])?$_POST['school_addr']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="school_phone">טלפון</label>
						<div class="dsfield">
							<input type="text" id="school_phone" name="school_phone"
								value="<?php echo isset($_POST['school_phone'])?$_POST['school_phone']:'';?>" />
						</div>
					</div>
					<div class="dsline">
						<label class="dslabel" for="field_of_study">תחום לימודים</label>
						<div class="dsfield">
							<input type="text" id="field_of_study" name="field_of_study"
								placeholder="דוגמאות: תלמיד תיכון, לתואר ראשון בתקשורת"
								value="<?php echo isset($_POST['field_of_study'])?$_POST['field_of_study']:'';?>" />
						</div>
					</div>
				</div>
				<!--------------------------->
				<div id="mensonly" style="display: none;">
					<div class="dstitle">מוסדות השכלה גבוהה</div>
					<div class="dsline wider">
						<label class="dslabel" for="academic_degree">האם למדת בעבר במוסד
							להשכלה גבוהה?</label> <input type="radio" name="academic_degree"
							id="academic_degree" value="false" />&nbsp;לא&emsp;<input
							type="radio" name="academic_degree" id="academic_degree"
							value="true" />&nbsp;כן
					</div>
					<!-------------- Hidden ---- אזרחות נוספת -->
					<div id="academic"
						style="display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
						<div class="dsline">
							<label class="dslabel" for="institution_name">שם מוסד הלימודים</label>
							<div class="dsfield">
								<input type="text" id="institution_name" name="institution_name"
									value="<?php echo isset($_POST['institution_name'])?$_POST['institution_name']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="institution_addr">כתובת</label>
							<div class="dsfield">
								<input type="text" id="institution_addr" name="institution_addr"
									value="<?php echo isset($_POST['institution_addr'])?$_POST['institution_addr']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="institution_phone">טלפון</label>
							<div class="dsfield">
								<input type="text" id="institution_phone"
									name="institution_phone"
									value="<?php echo isset($_POST['institution_phone'])?$_POST['institution_phone']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="institution_start">שנת תחילת הלימודים</label>
							<div class="dsfield">
								<input type="text" id="institution_start"
									name="institution_start"
									value="<?php echo isset($_POST['institution_start'])?$_POST['institution_start']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="institution_end">שנת סיום הלימודים</label>
							<div class="dsfield">
								<input type="text" id="institution_end" name="institution_end"
									value="<?php echo isset($_POST['institution_end'])?$_POST['institution_end']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="institution_degree">תחום לימודים</label>
							<div class="dsfield">
								<input type="text" id="institution_degree"
									name="institution_degree"
									placeholder="לדוגמה: תואר ראשון בתקשורת"
									value="<?php echo isset($_POST['institution_degree'])?$_POST['institution_degree']:'';?>" />
							</div>
						</div>
					</div>
					<!--------------------------->

					<!------------------- Men only########################### ----------->

					<div class="dstitle">שונות</div>
					<div class="dsline wider">
						<label class="dslabel" for="other_countries">האם ביקרת במדינות
							נוספות ב-5 השנים האחרונות?</label>
						<div class="dsfield">
							<input type="radio" name="other_countries" id="other_countries"
								value="true" />&nbsp;כן&emsp;<input type="radio"
								name="other_countries" id="other_countries" value="false" />&nbsp;לא
						</div>
					</div>
					<!----------------- Hidden -------------------->
					<div id="othercountries"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
						<div class="dsline">
							<label class="dslabel" for="countries">באיזה מדינות?</label>
							<div class="dsfield">
								<input type="text" id="countries" name="countries"
									placeholder="לדוגמה: הודו, גרמניה, טורכיה"
									value="<?php echo isset($_POST['countries'])?$_POST['countries']:'';?>" />
							</div>
						</div>
					</div>
					<!-----------------/Hidden---------------------->
					<div class="dsline">
						<label class="dslabel" for="languages">איזה שפות הנך דובר?</label>
						<div class="dsfield">
							<input type="text" id="languages" name="languages"
								placeholder="לדוגמה: עברית, ערבית ואנגלית"
								value="<?php echo isset($_POST['languages'])?$_POST['languages']:'';?>" />
						</div>
					</div>

					<div class="dsline wider">
						<label class="dslabel" for="military_service">האם שירתת בצבא?</label>
						<div class="dsfield">
							<input type="radio" name="military_service" id="military_service"
								value="false" />&nbsp;לא&emsp;<input type="radio"
								name="military_service" id="military_service" value="true" />&nbsp;כן
						</div>
					</div>
					<!----------------- Hidden -------------------->
					<div id="military"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
						<div class="dsline">
							<label class="dslabel" for="military_nation">מדינה בה שירתת</label>
							<div class="dsfield">
								<input type="text" id="military_nation" name="military_nation"
									value="<?php echo isset($_POST['military_nation'])?$_POST['military_nation']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="military_corps">שם החיל</label>
							<div class="dsfield">
								<input type="text" id="military_corps" name="military_corps"
									value="<?php echo isset($_POST['military_corps'])?$_POST['military_corps']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="military_rank">דרגת שחרור</label>
							<div class="dsfield">
								<input type="text" id="military_rank" name="military_rank"
									value="<?php echo isset($_POST['military_rank'])?$_POST['military_rank']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="military_profession">תפקיד עיקרי</label>
							<div class="dsfield">
								<input type="text" id="military_profession"
									name="military_profession"
									placeholder="לדוגמה: חייל קרבי, חובש"
									value="<?php echo isset($_POST['military_profession'])?$_POST['military_profession']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="military_start">שנת גיוס</label>
							<div class="dsfield">
								<input type="text" id="military_start" name="military_start"
									value="<?php echo isset($_POST['military_start'])?$_POST['military_start']:'';?>" />
							</div>
						</div>
						<div class="dsline">
							<label class="dslabel" for="military_end">שנת שחרור</label>
							<div class="dsfield">
								<input type="text" id="military_end" name="military_end"
									value="<?php echo isset($_POST['military_end'])?$_POST['military_end']:'';?>" />
							</div>
						</div>
					</div>
					<!-----------------/Hidden---------------------->
					<div class="dsline wider">
						<label class="dslabel" for="expbioche">האם הנך בעל הכשרה על נשק או
							בעל ידע בחומרי חבלה\כימיה\ביולוגיה?</label>
						<div class="dsfield">
							<input type="radio" name="expbioche" id="expbioche" value="false" />&nbsp;לא&emsp;<input
								type="radio" name="expbioche" id="expbioche" value="true" />&nbsp;כן
						</div>
					</div>

					<!------------- Hidden -------------->
					<div id="explosives"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
						<div class="dsline">
							<label class="dslabel" for="explosives_explain">פרט</label>
							<div class="dsfield">
								<input type="text" id="explosives_explain"
									name="explosives_explain"
									placeholder="לדוגמה: נשק קל, הכשרה צבאית"
									value="<?php echo isset($_POST['explosives_explain'])?$_POST['explosives_explain']:'';?>" />
							</div>
						</div>
					</div>
					<!----------- /Hidden --------------->


					<div class="dsline wider">
						<label class="dslabel" for="volunteer">האם אתה מתנדב בארגון ללא
							מטרות רווח?</label>
						<div class="dsfield">
							<input type="radio" name="volunteer" id="volunteer" value="false" />&nbsp;לא&emsp;<input
								type="radio" name="volunteer" id="volunteer" value="true" />&nbsp;כן
						</div>
					</div>
					<!------------- Hidden -------------->
					<div id="volunteer_block"
						style="width: 100%; display: none; background: #FAFAFA; padding-top: 1em; padding-bottom: 1em;">
						<div class="dsline">
							<label class="dslabel" for="volunteer_org_name">שם הארגון</label>
							<div class="dsfield">
								<input type="text" id="volunteer_org_name"
									name="volunteer_org_name"
									value="<?php echo isset($_POST['volunteer_org_name'])?$_POST['volunteer_org_name']:'';?>" />
							</div>
						</div>
					</div>
					<!----------- /Hidden --------------->

					<!-- Mens only end --->
				</div>
			</div>

			<div style="clear: both;">&nbsp;</div>

			<div class="bottom" style="display: block; height: 70px; position: relative; clear: both; margin:1em auto">
				<button id="3previous" class="ds_button_previous"></button>
				<button type="submit" id="3next" name="3next" class="ds_button_next"></button>
			</div>
		</div>

	</form>
</div>
<?php
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/inc/footer.php');
?>