<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
$secured = true;

require_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/base.php" );

if ( !$system->checkConnected() )
{
  	header("location:registration.php");
  	exit;
}

if (strtolower($_SERVER['REQUEST_METHOD']) == "post")
{
	$fields = array(
		'app_name' => "שם פרטי באנגלית",
		'app_fname' => "שם משפחה באנגלית",
		'app_full_name' => "שם מלא בעברית",
		'app_previous_name' => "שם פרטי קודם",
		'app_previous_fnames' => "שמות משפחה קודמים",
		'gender' => "מין",
		'app_id' => "מספר תעודת זהות ישראלית",
		'status' => "מצב משפחתי",


		'spouse_name' => "שם מלא של בן\בת הזוג",
		'spouse_bdate' => "תאריך לידת בן\בת הזוג",
		'spouse_cityofbirth' => "עיר וארץ לידת בן\בת הזוג",
		'spouse_citizenship' => "אזרחות בן\בת הזוג",
		'spouse_address' => "כתובת בן\בת הזוג",
		'wedding_date' => "תאריך נישואין",
		'divorce_date' => "תאריך גירושין",
		'divorce_how' => "כיצד הסתיימו הגירושים",
		'divorce_country' => "מדינה בה התגרשו",

		/// בן\בת זוג
		'app_cityob' => "עיר לידה",
		'app_countryob' => "ארץ לידה",
		'nationality' => "אזרחות",

		'2ndnationality' => "אזרחות נוספת",
		'2ndpassportnum' => "מספר דרכון עבור האזרחות הנוספת",

		'passport' => "מספר דרכון",
		'passport_country' => "ארץ הנפקה",
		'passport_city' => "עיר הנפקה",
		'passport_dateissued' => "תאריך הנפקה",
		'passport_dateexpire' => "תאריך פקיעת תוקף",

		'passport_lost' => "דרכון אבד\נגנב?",
		'lost_passport_num' => "מספר דרכון שאבד או נגנב",
		'lost_passport_country' => "ארץ בה הונפק הדרכון שנגנב",

		//Phones and address
		'cellular' => "טלפון נייד",
		'phone' => "טלפון",
		'street' => "רחוב",
		'housenum' => "מספר בית",
		'apt_num' => "מספר דירה",
		'city' => "עיר",
		'country' => "מדינה",
		'zipcode' => "מיקוד",

		//Mail address
		'other_address' => "להשתמש בכתובת הראשית למשלוח דואר",
		'mail_street' => "דואר: רחוב",
		'mail_housenum' => "דואר: מספר בית",
		'mail_apt_num' => "דואר: מספר דירה",
		'mail_city' => "דואר: עיר",
		'mail_country' => "דואר: מדינה",
		'mail_zipcode' => "דואר: מיקוד",
	  /*----------------------------------- End of part 1 -------------------------------------*/
	  
	  /*----------------------------------- Start of part 2 -----------------------------------*/
	 
		//Mother-Father
		'father_name' => "שם מלא של האב באנגלית",
		'father_bdate' => "תאריך לידה של האב",
		'father_in_usa' => "אבא בארצות הברית?",
		'father_status' => "מעמדו של האבא",

		'mother_name' => "שם מלא של האם באנגלית",
		'mother_bdate' => "תאריך לידה של האם",
		'mother_in_usa' => "אמא בארצות הברית?",
		'mother_status' => "מעמדה של האמא",

		//Relatives
		'1st_relative_exist' => "קיים קרוב מדרגה ראשונה?",
		'1st_relative_name' => "שם מלא קרוב מדרגה ראשונה",
		'1st_relative_relation' => "קרבה לקרוב מדרגה ראשונה",
		'1st_relative_status' => "מעמד הקרוב מדרגה ראשונה",
		'2nd_relative_exist' => "קרוב לא מקרבה ראשונה?",

		//Travel
		'travel_purpose' => "מטרת הנסיעה",
		'estimated_doa' => "תאריך הגעה משוער",
		'estimated_tos' => "זמן שהות משוער",
		'know_where_staying' => "ידוע היכן ישהה?",
		'staying_street' => "שהייה - רחוב",
		'staying_city' => "שהייה - עיר",
		'staying_state' => "שהייה - מדינה",
		'staying_zipcode' => "שהייה - מיקוד",

		'favoritedest' => "יעד נסיעה מועדף",

		'us_contact' => "קיים איש קשר בארצות הברית?",
		'us_contact_name' => "איש קשר - שם",
		'us_contact_relation' => "איש קשר - יחס קרבה",
		'us_contact_addr' => "איש קשר - כתובת",
		'us_contact_phone' => "איש קשר - טלפון",

		'sponser' => "מממן הנסיעה",
		'sponser_name' => "מממן - שם\ארגון",
		'sponser_phone' => "מממן - טלפון",
		'sponser_addr' => "מממן - כתובת",
		'sponser_relation' => "מממן - יחס קרבה",

		'other_people' => "נסיעה - אנשים נוספים?",
		'partofgroup' => "נסיעה - חלק מקבוצה\ארגון?",
		'organization_name' => "נסיעה - שם קבוצה\ארגון",
		'people_names' => "נסיעה - שמות הנוסעים הנוספים",

		'visited_before' => "ביקר בעבר?",
		'visit1_doa' => "ביקור 1 - תאריך הגעה",
		'visit1_period' => "ביקור 1 - משך שהות",
		'visit2_doa' => "ביקור 2 - תאריך הגעה",
		'visit2_period' => "ביקור 2 - משך שהות",
		'visit3_doa' => "ביקור 3 - תאריך הגעה",
		'visit3_period' => "ביקור 3 - משך שהות",
		'visit4_doa' => "ביקור 4 - תאריך הגעה",
		'visit4_period' => "ביקור 4 - משך שהות",
		'visit5_doa' => "ביקור 5 - תאריך הגעה",
		'visit5_period' => "ביקור 5 - משך שהות",

		'had_dl' => "החזיק ברישיון נהיגה אמריקאי?",
		'license_number' => "רישיון אמריקאי - מספר",
		'license_stateoi' => "רישיון אמריקאי - מדינת הנפקה",

		'had_visa' => "הונפקה בעבר ויזה לארצות הברית?",
		'visa_doi' => "ויזה - תאריך הנפקה",
		'visa_num' => "ויזה - מספר",
		'visa_type' => "ויזה - סוג",
		'visa_placeoi' => "ויזה - מדינה ועיר הנפקה",
		'finger_print' => "ויזה - נלקחו טביעות אצבע?",
		'visa_stolen' => "ויזה - נגנבה?",
		'stolen_year' => "ויזה - שנת גניבה",

		'visa_revoked' => "ויזה - נשללה\בוטלה?",
		'revoked_reason' => "סיבת ביטול\שלילת ויזה",

		'visa_rejected' => "סורב לויזה?",
		'rejected_reason' => "סיבת סירוב ויזה",

		'entrance_denied' => "סורב כניסה?",
		'entrance_denied_reason' => "סיבת סירוב כניסה",

		'immigration_request' => "הוגשה בקשת הגירה?",
		'immigration_info' => "הגירה - פירוט",
		/*----------------------------------- End of part 2 -------------------------------------*/

		/*----------------------------------- Start of part 3 -----------------------------------*/
		'work_primary'			=>			"עיסוק עיקרי",
		'employer' => "שם מעסיק \ מוסד לימודים",
		'work_address' => "עבודה - כתובת",
		'work_phone' => "עבודה - טלפון",
		'average_salary' => "משכורת ממוצעת",
		'job_description' => "תיאור תפקיד",
		'other_job' => "עבד בעבודה נוספת ב-5 השנים האחרונות?",
		/****####################################***********/
		'previous_employer' => "מעסיק קודם",
		'previous_work_address' => "עבודה קודמת - כתובת",
		'previous_work_phone' => "עבודה קודמת - טלפון",
		'previous_work_manager' => "עבודה קודמת - שם מנהל",
		'previous_work_start' => "עבודה קודמת - תאריך התחלה",
		'previous_work_end' => "עבודה קודמת - תאריך סיום",
		'previous_job_description' => "עבודה קודמת - תיאור תפקיד",

		//לימודים - ירד - קיים בקובץ
		//לימודים.txt
	  	// 'is_student' => "תלמיד\סטודנט?",
	  	// 'school_name' => "מוסד לימודים - שם",
	  	// 'school_addr' => "מוסד לימודים - כתובת",
	  	// 'school_phone' => "מוסד לימודים - טלפון",
	  	// 'field_of_study' => "מוסד לימודים - תחום",
  
		'academic_degree' => "למד במוסד להשכלה גבוהה?",
		'institution_name' => "ה.ג - שם",
		'institution_addr' => "ה.ג - כתובת",
		'institution_phone' => "ה.ג - טלפון",
		'institution_start' => "ה.ג - שנת התחלה",
		'institution_end' => "ה.ג - שנת סיום",
		'institution_degree' => "ה.ג - תחום לימודים",

		'other_countries' => "ביקר במדינות נוספות -5 השנים האחרונות?",
		'countries' => "מדינות בהן ביקר",
		'languages' => "שפות",

		'military_service' => "שירת בצבא?",
		'military_nation' => "צבא - מדינה",
		'military_corps' => "צבא - חיל",
		'military_rank' => "צבא - דרגת שחרור",
		'military_profession' => "צבא - מקצוע",
		'military_start' => "צבא - תאריך גיוס",
		'military_end' => "צבא - תאריך שחרור",

		'expbioche' => "בעל הכשרה בנשק\חומרים מסוכנים?",
		'explosives_explain' => "נשק\חומרים מסוכנים - פירוט",

		'volunteer' => "מתנדב בארגון ללא מטרות רווח?",
		'volunteer_org_name' => "ארגון התנדבות - שם"
  	);


  	//If magic_quotes_gpc is off       
  	/*An example use of stripslashes() is when the PHP directive magic_quotes_gpc is on (it was on by default before PHP 5.4), 
  	and you aren't inserting this data into a place (such as a database) that requires escaping. 
  	For example, if you're simply outputting data straight from an HTML form.*/
  	if (get_magic_quotes_gpc()){
    	//If magic quotes is on then we will strip the slashes
    	foreach ($_POST as $key=>$value){
      	$_POST[$key] = stripslashes($value);
    	}
  	}
  
  	//ניקוי רווחים מיותרים בקצוות והחלפה של בוליאן לעברית
  	foreach ($_POST as $key => $value){
    	if ($value == "true"){
      		$_POST[$key]="כן";
    	}elseif($value == "false"){
      		$_POST[$key]="לא";
    	}else{
      		$_POST[$key]=trim($value);
    	}
  	}
  
  	//Translating gender to hebrew
  	if ( isset($_POST['gender']) )
  	{
    	if ($_POST['gender']=='male')
    	{
      		$_POST['gender']='זכר';
    	}
    	else
    	{
      		$_POST['gender']='נקבה';
    	}
  	}

  	$content = '<!DOCTYPE html>
	<html>
	<head>
		<style type="text/css">
			html {
				direction:rtl;
			}
			* {
				font-family:tahoma;
				font-size:13px;
			}
		</style>
		<meta charset="utf-8">
	</head>
	<body>
	<div style="font-weight:bold; font-size:24px; margin-bottom:1em; text-shadow:1px 1px 1px gray;">'.$_POST['app_name'].' '.$_POST['app_fname'].' - DS160</div>';
		

	$exportContent = array();

	//Printing $_POST items with values
  	foreach ($fields as $key=>$value)
  	{
    	if ( isset($_POST[$key]) && ($_POST[$key]!='') )
    	{
    		$exportContent[] = array("label"=>$value, "value"=>$_POST[$key]);
    	}
    	else
    	{
    		$exportContent[] = array("label"=>$value, "value"=>"");
			$_POST[$key]='';
		}

		if ( $key == "app_id" )
		{
			$bday = array(
				"day"	=> isset($_POST["bday-day"]) && !empty($_POST["bday-day"]) ? (int) $_POST["bday-day"] : 0,
				"month" => isset($_POST["bday-month"]) && !empty($_POST["bday-month"]) ? (int) $_POST["bday-month"] : 0,
				"year"	=> isset($_POST["bday-year"]) && !empty($_POST["bday-year"]) ? (int) $_POST["bday-year"] : 0
			);
			if ( checkdate($bday["month"], $bday["day"], $bday["year"]) )
			{
				$exportContent[] = array("label"=>"תאריך לידה", "value"=>$bday["day"]."-".$bday["month"]."-".$bday["year"]);
			}
		}
  	}

  	foreach($exportContent as $formLine)
  	{
  		$content .= '<div style="display:block; line-height:2em;">
      					<div class="dslabel" style="font-weight:bold; display:inline;">'.$formLine['label'].':</div>&nbsp;
      					<div class="dsfield" style="display:inline;">'.$formLine['value'].'</div></div>';
  	}

	$content .= '</body></html>';

	$home_address = $_POST['housenum'].' ';
	$home_address += $_POST['street']."\r\n";
	$home_address += $_POST['housenum']."\r\n";
	$home_address += $_POST['apt_num']."\r\n";
	$home_address += $_POST['city'].', ';
	$home_address += $_POST['zipcode'].'\n';
	$home_address += $_POST['country'];
	$home_address = nl2br($home_address);
	
	$mail_address = $_POST['mail_housenum'].' ';
	$mail_address += $_POST['mail_street'].'\n';
	$mail_address += $_POST['mail_housenum'].'\n';
	$mail_address += $_POST['mail_apt_num'].'\n';
	$mail_address += $_POST['mail_city'].', ';
	$mail_address += $_POST['mail_zipcode'].'\n';
	$mail_address += $_POST['mail_country'];
	$mail_address = nl2br($mail_address);
	
	$file_exist = true;
	
	while($file_exist)
	{
		$filename  = $_SESSION['USERNAME'].date("d_m_Y_h_i_s", time());
		
		if ( !file_exists("../forms/".$filename.".html") )
		{
			$file_exist = false;
		}
	}

  	//Saving the information to html file
  	file_put_contents("../forms/".$filename.".html", $content);

  	$PaymentHash = md5(time()+rand(1,100000));
  
  	//database connection instance
  	$mysqli = db::get_instance();
 
  	if ( !isset($_POST['cellular']) || empty($_POST['cellular']) )
  	{
      	$sql = "SELECT `phone` FROM `customers` WHERE `username`='".$_SESSION['USERNAME']."';";
      	$result = $mysqli->query($sql);
      
      	if ( $mysqli->numRows($result) > 0 )
      	{
          	$row = $result->fetch_object();
          
          	$cellular = $row->phone;
      	}
  	}else{
      	$cellular = $_POST['cellular'];
  	}
  
  	//Saving few details in the database
	
    $appData = array(
      
        "CustomerID"    => $_SESSION['USER_ID'],
        "ApplicantName" => $_POST['app_name']." ".$_POST['app_fname'],
        "FileName"      => $filename.".html",
        "Phone"         => $cellular,
        "IP"            => system::getUserIP(true),
        "filltime"      => time(),
        "nationalid"    => $_POST['app_id'],
        "address"       => $home_address,
        "PaymentHash"   => $PaymentHash
    );

	$_SESSION['dsform_id'] = $mysqli->insert("dsforms", $appData);

 	header('location:payment.php?AppID='.$mysqli->insert_id.'&Hash='.$PaymentHash);
 	exit;
}