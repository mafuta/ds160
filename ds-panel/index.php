<?php
$secured 	= true;
require_once($_SERVER["DOCUMENT_ROOT"]."/inc/base.php");

$mysqli 	= db::get_instance();

if (isset($_SESSION["is_admin"]) && isset($_SESSION["admin_id"])){
  header('location:users.php');
  exit;
}else{
  header('location:login.php');
  exit;
}