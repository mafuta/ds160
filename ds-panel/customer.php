<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

//Is admin session isn't set or the session value isn't true
if (!isset($_SESSION['is_admin']) || empty($_SESSION['is_admin'])){
	header('location:index.php');
	exit;
}

$CustomerID = intval($_GET['CustomerID']);

//If CustomerID wasn't sent to the page or the customerID doesn't exist
if (empty($CustomerID) || !customers::exist($CustomerID)){
	echo 'שגיאה מערכת';
  	exit;
}


$customer = customer::Get($_GET['CustomerID']);
$row = $customer->getInfo();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="admin.css">

	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/ds-panel/admin.js"></script>

    <script type="text/javascript" src="/ds-panel/assets/bootstrap-rtl/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/ds-panel/assets/bootstrap-rtl/bootstrap.min.css">

    <link class="type" rel="stylesheet" href="/ds-panel/assets/font-awesome/css/font-awesome.min.css">

	<script type="text/javascript">
	
	
	function addComment(){
		var note = $("#UserNote").val();
		
		$.ajax({
			url: './ajax/addComment.php',
			type: 'post',
			data: {'note' : note, 'CustID':<?=$CustomerID;?>},
			success: function(response){
				if (response=='okay')
				{
					window.location.reload();
				}
			}
		});
		
		return false;
	}
	</script>
</head>
<body>
  <form id="customer_data" method="post" autocomplete="off">
		<input type="hidden" name="CustID" id="CustID" value="<?php echo $row->id;?>">
		<table class="table sub-table">
            <thead>
                <tr>
                    <th colspan="2">כרטיס לקוח - <?php echo $row->full_name;?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="vertical-align:top !important; border-left:1px dashed #d7d7d7;">

                        <table class="CustTable">
                            <tr>
                                <td>שם הלקוח:</td>
                                <td><input type="text" name="full_name" id="full_name" value="<?=stripslashes($row->full_name)?>"></td>
                            </tr>

                            <tr>
                                <td>שם משתמש:</td>
                                <td><input type="text" name="username" id="username" value="<?=$row->username?>"></td>
                            </tr>

                            <tr>
                                <td>סיסמה חדשה:</td>
                                <td><input type="password" name="newpass" id="newpass"></td>
                            </tr>

                            <tr>
                                <td>אימות סיסמה חדשה:</td>
                                <td><input type="password" name="newpass2" id="newpass2"></td>
                            </tr>

                            <tr>
                                <td>מספר טלפון:</td>
                                <td><input type="text" name="phone" id="phone" value="<?=$row->phone?>"></td>
                            </tr>

                            <tr>
                                <td>כתובת דוא"ל:</td>
                                <td><input type="email" name="email" id="email" value="<?=$row->email?>"></td>
                            </tr>

                            <tr>
                                <td>תאריך הרשמה:</td>
                                <td><?=date('d-m-Y',$row->reg_date)?></td>
                            </tr>

                            <tr>
                                <td>התחברות אחרונה:</td>
                                <td><?=date('H:i <b>d-m-Y</b>',$row->last_login_date);?></td>
                            </tr>

                            <tr>
                                <td colspan="2"><button class="btn btn-block" onclick="return DsAdmin.Customers.update('customer_data');">עדכון פרטים</button></td>
                            </tr>
                        </table>
                    </td>

                    <td style="vertical-align: top !important;">
                        <strong>הערות ללקוח:</strong><br /><br />
                        <textarea id="UserNote" name="UserNote" style=""></textarea>
                        <button class="btn btn-block" style="width:70%" onclick="return addComment();">הוסף הערה</button>
                        <div style="margin-top:1em;">
                            <?php
                            $notes = $customer->getComments();
                            if (!empty($notes))
                            {
                                foreach($notes as $k=>$v)
                                {
                                    echo '<div class="well customer-note"><div class="date">'.date("d-m-Y H:i:s",$v->timestamp).'</div>'.nl2br($v->comment).'</div>';
                                }
                            }
                            ?>
                        </div>


                        <?php
                        $sql = "SELECT
                                    ds.id, cust.id AS CustomerID, cust.full_name, ds.FileName,
                                    ds.phone, ds.ip, ds.filltime, ds.ApplicantName,
                                    ds.nationalid
                                FROM
                                        dsforms AS ds
                                INNER JOIN
                                        customers AS cust ON (ds.CustomerID=cust.id)
                                WHERE
                                  cust.id = '$row->id'
                                ORDER BY
                                        ds.filltime DESC;";

                        $result = $mysqli->query($sql);

                        if ( $result->num_rows > 0 ) {
                        ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="5">טפסים</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            while( $row = $result->fetch_object() ) {
                            ?>
                            <tr>
                                <td><?=$row->id?></td>
                                <td><?=$row->full_name?></td>
                                <td><b><?=($row->ApplicantName ? $row->ApplicantName : '')?></b></td>
                                <td><?=date("d-m-Y H:i:s", $row->filltime)?></td>
                                <td><a href="getForm.php?FileName=<?=$row->FileName?>" target="_blank"><i class="icon-file-text" style="color:#808080; font-size:16px;"></i></a></td>
                            </tr>
                            <?php
                            }
                        ?>
                            </tbody>
                        </table>
                        <?php
                        }
                        ?>


                    </td>
                </tr>
            </tbody>

		</table>
  </form>
  
</body>
</html>