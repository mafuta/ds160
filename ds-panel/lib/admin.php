<?php
class admin
{
	public static function getPermissionText($permissionID)
	{
            $mysqli = db::get_instance();

            $sql = "SELECT text FROM permissions WHERE id='{$permissionID}';";

            $result = $mysqli->query($sql);

            if ( !$mysqli->numRows($result) )
            {
                    return false;
            }

            $row = $result->fetch_object();

            return $row->text;
	}
	
	public static function exist($adminID)
	{
            $mysqli = db::get_instance();

            $sql = "SELECT count(1) AS exist FROM admins WHERE id='{$adminID}';";

            $result = $mysqli->query($sql);

            if ( !$mysqli->numRows($result) )
            {
                return false;
            }

            $row = $result->fetch_object();

            if ( (int) $row->exist==1 )
            {
                return true;
            }

            return false;
	}
        
        public static function logConnection($adminID)
        {
            $mysqli = db::get_instance();

            $insertData = [
                "adminid"   => $adminID,
                "time"      => time(),
                "ip"        => system::getUserIP(true)
            ];

            if ( $mysqli->insert("adminlog", $insertData) )
            {
                return true;
            }

            return false;
        }
        
        public static function getPermission()
        {
            
        }
}