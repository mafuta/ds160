<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

if (!isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id'])){
  	unset($_SESSION);
  	header('location:login.php');
  	exit;
}

$page = isset($_GET["page"]) && is_numeric($_GET["page"]) ? (int) $_GET["page"] : 1;
if ( empty($page) ) {
    $page = 1;
}
$limit = 15;
$starting = ($page-1) * $limit;

$sql = "SELECT
          SQL_CALC_FOUND_ROWS
          m.id, m.name, m.email, m.phone, m.subject, m.message, m.time, m.params, m.reviewed
		FROM 
			messages AS m
		ORDER BY
			m.time DESC
		LIMIT $starting,$limit;";

$query  = $mysqli->query($sql);

$totalSql = "SELECT FOUND_ROWS() as total;";
$totalQuery = $mysqli->query($totalSql);
$totalResult = $totalQuery->fetch_object();
$total = $totalResult->total;

$totalPages = ceil($total / $limit);

if ( $page>$totalPages ) {
    header("location:".$_SERVER['PHP_SELF']."?page=1");
    exit;
}

require_once('header.php');
?>
<script type="text/javascript">
$(document).ready(function(){
        
	$(document).on('click', ".reviewCheckbox", function(){

		var newVal;

		newVal = $(this).is(':checked') ? 1 : 0;

		DsAdmin.Messages.toggleReviewed( $(this).prop('data'), newVal );

	});
        
        $(".message-header").on("click", function(){
           
           var nextRow = $(this).next("tr");
           
           if ( nextRow.is(":visible") )
           {
               nextRow.hide();
           }else{
               nextRow.show();
           }
 
        });

});
</script>

<table class="table striped-hidden">
  <thead>
    <tr>
      <td colspan="6">פניות מהאתר</td>
    </tr>
	
	<tr>
		<td style="width:10%;">ID</td>
		<td style="width:10%;">שם</td>
		<td style="width:45%;">נושא</td>
		<td style="width:15%;">טלפון</td>
		<td style="width:15%;">זמן שליחה</td>
		
	</tr>
  </thead>
  <tbody>
		<?php
		while ( $row = $query->fetch_object() )
		{

		?>
		<tr class="message-header">
			<td style="padding-right:5px;"><?=stripslashes($row->id)?></td>
			<td style="padding-right:5px;"><?=stripslashes($row->name)?></td>
			<td style="padding-right:5px;"><?=stripslashes($row->subject)?></td>
			<td style="padding-right:5px;"><?=$row->phone?></td>
			<td class="ltr" style="text-align:center;"><?php echo date("<b>d-m-Y</b> - H:i", $row->time);?></td>
		</tr>

		<tr class="message-content">
			<td style="border-bottom:1px solid #EEE;">הודעה:</td>
			<td style="border-bottom:1px solid #EEE;" colspan="5">
				<?php echo nl2br(stripslashes($row->message));?>
			</td>
		</tr>
		<?php
		}
		?>
  </tbody>
</table>

<div class="pagination">
    <ul>
        <li class="<?=($page <= 1 ? 'disabled' : '');?>"><a href="<?=($page <= 1 ? '#' : $_SERVER['PHP_SELF'].'?page='.($page-1));?>">הקודם</a></li>
        <?php
        for($i=1; $i<=$totalPages; $i++) {
            echo '<li '.($i == $page ? 'class="active"' : '').'><a href="'.$_SERVER['PHP_SELF'].'?page='.$i.'">'.$i.'</a></li>';
        }
        ?>
        <li class="<?=($page >= $totalPages ? 'disabled' : '');?>"><a href="<?=($page >= $totalPages ? '#' : $_SERVER['PHP_SELF'].'?page='.($page+1));?>">הבא</a></li>
    </ul>
</div>
