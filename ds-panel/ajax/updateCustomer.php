<?php
$secured = true;
include_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

if (strtolower($_SERVER['REQUEST_METHOD'])!='post'){
	echo 'error';
	exit;
}

$mysqli = db::get_instance();

if (!isset($_POST['CustID']) || empty($_POST['CustID'])){
	echo 'error';
	exit;
}

$updateData = array(
	"username" 	=> $_POST["username"],
	"full_name" => $_POST["full_name"],
	"phone"		=> $_POST["phone"],
	"email"		=> $_POST["email"]
);

if (!empty($_POST['newpass']) && !empty($_POST['newpass2']) && ($_POST["newpass"] == $_POST["newpass2"]) ){
	//$salt is defined in /inc/base.php
	$updateData["password"] = md5($_POST["newpass"] . $salt);
}

$result = $mysqli->where("CustID",$_POST['CustID'])->update("customers", $updateData);

if ($result){
	echo 'okay';
}