<?php
$secured = true;
include_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

if ( strtolower($_SERVER['REQUEST_METHOD'])!='post' )
{
	echo 'error';
	exit;
}

$mysqli = db::get_instance();

$AdminID = isset($_POST['AdminID']) ? trim($_POST['AdminID']) : null;

if ( empty($AdminID) )
{
	echo 'error';
	exit;
}

$Admin = (object) $_POST;

$updateData = array(

	"username" 	=> $Admin->username,
	"name"		=> $Admin->name,
	"email"		=> $Admin->email,
	"permission"=> $Admin->permission

);	
if ( !empty($Admin->password) )
{
	$updateData["password"] = md5($Admin->password);
}

if ( $result = $mysqli->where("id", $AdminID)->update("admins", $updateData) );
{
	echo 'okay';
	exit;
}

echo "error";