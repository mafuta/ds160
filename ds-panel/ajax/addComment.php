<?php
$secured = true;
include_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

if ( !isset($_POST['CustID']) || empty($_POST['CustID']) )
{
	echo 'error';
	exit;
}

$insertData = array(
	"customerid" 	=> (int) $_POST['CustID'],
	"time"		 	=> time(),
	"comment"		=> $_POST['note'],
	"adminid"		=> (int) $_SESSION['admin_id']
);

if ( $mysqli->insert("customers_notes", $insertData) )
{
	echo "okay";
	exit;
}
echo "error";