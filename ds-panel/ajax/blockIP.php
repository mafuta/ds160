<?php
$secured = true;
require_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/base.php" );


if ( !isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id']) ){
	/**
	*
	* @TODO clear admin login data
	*
	**/
  unset($_SESSION);
  exit;
}

if (!isset($_POST["ip"]) || empty($_POST["ip"]))
{
	//No IP address provided
	exit;
}
$ipAddress 		= $_POST["ip"];

if ( filter_var($ipAddress, FILTER_VALIDATE_IP) === false )
{
	//Not valid ip address
	exit;
} 

$blockData = array(
			//Converting the IP address to long integer
			"ip"			=> ip2long($ipAddress),
			//Reason for blocking the IP address
			"reasonID" 		=> (int) $_POST["reason"],
			//The id of the admin who blocked the IP address
			"adminID"		=> (int) $_SESSION['admin_id'],
			//Set block to be active
			"active"		=> 1,
			"time_blocked"	=> time()
);

$mysqli = db::get_instance();
if ( !$mysqli->insert("blocked_ips", $blockData) )
{
	echo "error";exit;
}

echo 'okay';