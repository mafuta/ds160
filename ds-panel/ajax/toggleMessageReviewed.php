<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

if ( !isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id']) )
{
  header('location:login.php');
  exit;
}

if ( strtolower($_SERVER['REQUEST_METHOD'])!='post' )
{
	echo 'error';
	exit;
}

$conn = db::get_instance();

$data->id 		= (int) $_POST['MessageID'];
$data->reviewed = (int) $_POST['reviewed'];

$sql = "UPDATE messages SET reviewed='{$data->reviewed}' WHERE messages.id='{$data->id}';";

if ( !$conn->query($sql) )
{
	echo 'error';
	exit;
}

echo 'okay';