<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

if ( !isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id']) ){
  unset($_SESSION);
  exit;
}

$required = array('username','password','email','name');

foreach($required as $requiredValue)
{
	if (!isset($_POST[$requiredValue]) || empty($_POST[$requiredValue]))
	{
		echo 'error: '.$requiredValue;
		exit;
	}
}


$newAdmin = (object) $_POST;

$newAdmin->password = md5($newAdmin->password.$salt);

$mysqli = db::get_instance();

$insertData = array(

	"username" 		=> $newAdmin->username,
	"password"		=> $newAdmin->password,
	"email"			=> $newAdmin->email,
	"name"			=> $newAdmin->name,
	"permission"	=> $newAdmin->permission,
	"reg_date"		=> time(),
	"last_login"	=> 0
);


if ( !$mysqli->insert("admins", $insertData) )
{
	echo 'error';
	exit;
}

echo 'okay';