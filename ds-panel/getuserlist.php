<?php
header('Content-Type: text/html; charset=utf-8'); 

$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

if ( !isset($_SESSION['is_admin']) || $_SESSION['is_admin'] != true){
	exit;
}

$searchtext = isset($_POST['searchtext']) ? $mysqli->escape(trim($_POST['searchtext'])) : null;

if ( empty($searchtext) )
{
	exit;
}

$phonetext = str_replace("-", "", $searchtext);

$sql = "SELECT 
			customers.id AS CustomerID,
			customers.full_name,
			customers.username,
			customers.phone,
			
			customers.last_login_ip,
			customers.last_login_date
		FROM 
			customers 
		WHERE 
			customers.full_name like '%$searchtext%'
			OR
			customers.phone like '%$phonetext%'
		GROUP BY
			customers.id
		LIMIT 10;";
if ($result = $mysqli->query($sql))
{
	if ($result->num_rows == 0){ exit; }
	
	echo '<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>שם לקוח</th>
			<th>שם משתמש</th>
			<th>מספר טלפון</th>
			<th>תאריך הרשמה</th>
		</tr>
	</thead><tbody>';
	
	$i = 0;
	while ($row = $result->fetch_object()){
	
		$class = ($i%2==1 ? 'grayline ' : '');
	
		$phone = "";
		if (strlen($row->phone) > 9 )
		{
			$phone = substr($row->phone, 0, 3).'-'.substr($row->phone, 3);
		}else{
			$phone = substr($row->phone, 0, 2).'-'.substr($row->phone, 2);
		}
		
		
		echo '
		<tr class="'.$class.'pointer customer-row" custid="'.$row->CustomerID.'">
			<td>'.$row->CustomerID.'</td>
			<td>'.stripslashes($row->full_name).'</td>
			<td style="direction:rtl; text-align:right;">'.$row->username.'</td>
			<td>'.$phone.'</td>
			<td style="direction:ltr; text-align:right;">'.date('<b>d.m.Y</b>  H:i:s',$row->last_login_date).'</td>
		</tr>
		
		<tr id="customer_'.$row->CustomerID.'" class="customer-info-row">
			<td colspan="5"></td>
		</tr>
		';
		
		$i++;
  }
	echo '</tbody></table>';
}else{
	echo "failure";
}	
?>