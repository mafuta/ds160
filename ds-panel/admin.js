(function()
{
	
	var dsadmin = 
	{
		
		Messages:
		{

			toggleReviewed: function(MessageID, newVal)
			{
				var checkBox = $("#message_review_"+MessageID);

				$.ajax({
					type: 'POST',
					url: "./ajax/toggleMessageReviewed.php",
					data: {'MessageID':MessageID, 'reviewed':newVal},
					success: function(response){

					}
				});
			}

		},

		Permissions:
		{
			addAdmin: function(frm){

				var frmdata = $("#"+frm).serialize();
				if (frmdata=='')
				{
					return false;
				}
				$.ajax({
					type: 'POST',
					url: './ajax/addAdmin.php',
					data: frmdata,
					success: function(response){

						$("#adminList").load('./controls/adminList.php');
						alert('מנהל נוסף בהצלחה');
					
					}
				});
				
				return false;
				
			},
			
			updateAdmin: function(frm){
				var formdata = $("#"+frm).serialize();
				if ( $("#password").val() != $("#password_r").val() )
				{
					alert('סיסמאות לא תואמות');
					$("#password").focus();
					return false;
				}
				
				$.ajax({
					url: './ajax/updateAdmin.php',
					type: 'POST',
					data: formdata,
					success: function(response){
						alert('לקוח עודכן בהצלחה');
						window.location.reload();
					}
				});
				
				return false;
			},
			
			toggleAdminFrame: function(AdminID, searchContainer){
			
				if (searchContainer)
				{
					var CustRow = $("#admin_"+AdminID+"_search");
					var CustColumn = $("#admin_"+AdminID+"_search td");
				}else{
					var CustRow = $("#admin_"+AdminID);
					var CustColumn = $("#admin_"+AdminID+" td");
				}

				if (CustRow.is(':visible')){
					CustRow.slideUp('slow');
				}else{
                    var $frame = $("<iframe/>", {src:"admin.php?AdminID="+AdminID}).css({border:0, width:"100%", display:"inline-block", height:"490px"});

					CustColumn.html($frame);
					
					CustRow.slideDown('slow');
				}
				

			}
		},
		
		Customers:
		{
			update: function updateCustomer(frm){
							var formdata = $("#"+frm).serialize();
							
							if ($("#newpass").val() != $("#newpass2").val()){
								alert('סיסמאות לא תואמות');
								$("#newpass").focus();
								return false;
							}
							
							$.ajax({
								url: './ajax/updateCustomer.php',
								type: 'post',
								data: formdata,
								success: function(response){
									alert('לקוח עודכן בהצלחה');
									window.location.reload();
								}
							});
							
							return false;
					}
		},
		
		Features: 
		{
			SupportCheck:
			{				
				HTML5:
				{
					localStorage: function()
					{
						try 
						{
							return 'localStorage' in window && window['localStorage'] !== null;
						}
						catch (e)
						{
						    return false;
						}
					}
				}
			}
		},
		
		Stats:{},
		
		Site:
		{

			Back: function() 
			{
					history.back();
			}

		},

//##########################################################################################################
		
		Forms : {
		
			DigitsOnly: function(evt, value)
			{
				var keycode = evt.keyCode ? evt.keyCode : evt.charCode;

				if (evt.shiftKey===true || evt.ctrlKey===true){return false;}
				
				if (keycode==8 || keycode==9){return true;}
				
				if ((keycode==96 || keycode==48) ){
					return true;
				}
				if (keycode<48 || keycode>105){
					return false;
				}
				if (keycode>57 && keycode<96){return false;}

			},

			CheckPhone: function(value)
			{
				var stripped = value.replace(/[\(\)\.\-\ ]/g, '');    
	
				if (value != "")
				{
					if (isNaN(parseInt(stripped)))
					{
						return false;
					}
					else if (!(stripped.length >= 9 && stripped.length <= 10))
					{
						return false;
					}
				}
				
				return true;
			},
			
			MailOnly: function(src)
			{
				if(!src) return false;
				if(src.length==0)
				{
					return false;
				}
				else
				{
					var emailReg = "^[\\w-_\.]*[\\w-_\.]\@[\\w]\.+[\\w]+[\\w]$";
					var regex = new RegExp(emailReg);
					return regex.test(src);
				}
			}
		
		
		},
		
		System: {
		
			blockIP: function(){
				
				var ipAddress = $("#ip").val();
				var blockReason = $("#blockReason").val();
				
				if ( typeof ipAddress == "undefined" || ipAddress == "" || ipAddress == null ){return false;}
				
				$.ajax({
				
					url: "./ajax/blockIP.php",
					type: "POST",
					data: {ip: ipAddress, reason: blockReason},
					success: function(response){
						if ( response == "invalid" )
						{
							alert("כתובת IP לא תקנית");
						}else{
							alert("כתובת נחסמה");
							$("#ip").val('');
							$("#blockReason").prop("selectedIndex", 0);
						}
					}
				
				});
				
				return false;
			}
		
		}


	}
	if (window.DsAdmin==null)window.DsAdmin=dsadmin;
})();