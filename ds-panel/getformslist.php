<?php
header('Content-Type: text/html; charset=utf-8'); 

$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

if ( !isset($_SESSION['is_admin']) || $_SESSION['is_admin'] != true){
	exit;
}

$searchtext = isset($_POST['searchtext']) ? $mysqli->escape(trim($_POST['searchtext'])) : null;

if ( empty($searchtext) )
{
	exit;
}

$sql = "SELECT
            ds.id, cust.id AS CustomerID, cust.full_name, ds.FileName,
            ds.phone, ds.ip, ds.filltime,
            ds.nationalid, ds.ApplicantName, ds.usedPaymentHash
        FROM
                dsforms AS ds
        INNER JOIN
                customers AS cust ON (ds.CustomerID=cust.id)
        WHERE
          ";

if ( is_numeric($searchtext) ) {
	$sql .= "\nds.nationalid=" . ((int) $searchtext);

} else {
	$sql .= "\nds.phone like '%$searchtext%'
			OR cust.full_name like '%$searchtext%'
			OR ds.ApplicantName like '%$searchtext%'";
}

$sql .= "\nORDER BY
                ds.filltime DESC;";



if ($result = $mysqli->query($sql))
{
	if ($result->num_rows == 0){ exit; }
	
	echo '<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>שם לקוח</th>
			<th>שם מגיש הבקשה</th>
			<th>מספר תעודת זהות</th>
			<th>קובץ</th>
			<th>מספר טלפון</th>
			<th>שולם</th>
			<th>תאריך שליחת בקשה</th>
		</tr>
	</thead><tbody>';

	while ($row = $result->fetch_object()){

		$phone = "";
		if (strlen($row->phone) > 9 )
		{
			$phone = substr($row->phone, 0, 3).'-'.substr($row->phone, 3);
		}else{
			$phone = substr($row->phone, 0, 2).'-'.substr($row->phone, 2);
		}

		echo '
		<tr>
			<td>'.$row->id.'</td>
			<td>'.$row->full_name.'</td>
			<td>'.ucwords($row->ApplicantName).'</td>
			<td>'.$row->nationalid.'</td>
			<td class="ltr center"><a href="getForm.php?FileName='.$row->FileName.'" class="icon-file-text" style="font-size:17px; color:#808080" target="_blank"></a></td>
			<td>'.$row->phone.'</td>
			<td>'.($row->usedPaymentHash == 1 ? "<i class=\"icon-money\"></i>" : "").'</td>
			<td style="direction:ltr; text-align:right;">'.date('<b>d.m.Y</b>  H:i:s',$row->filltime).'</td>
		</tr>
		
		<tr id="customer_'.$row->CustomerID.'" class="customer-info-row">
			<td colspan="5"></td>
		</tr>
		';
  }
	echo '</tbody></table>';
}