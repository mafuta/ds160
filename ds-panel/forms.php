<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

if (!isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id'])){
    unset($_SESSION);
    header('location:login.php');
    exit;
}

$page = isset($_GET["page"]) && is_numeric($_GET["page"]) ? (int) $_GET["page"] : 1;
if ( empty($page) ) {
    $page = 1;
}
$limit = 15;
$starting = ($page-1) * $limit;


$sql = "SELECT
            SQL_CALC_FOUND_ROWS
            ds.id, cust.id AS CustomerID, cust.full_name, ds.FileName,
            ds.phone, ds.ip, ds.filltime, ds.ApplicantName,
            ds.nationalid, ds.usedPaymentHash
        FROM 
                dsforms AS ds
        INNER JOIN 
                customers AS cust ON (ds.CustomerID=cust.id)
        ORDER BY
                ds.filltime DESC
        LIMIT $starting,$limit;";

$query  = $mysqli->query($sql);

$totalSql = "SELECT FOUND_ROWS() as total;";
$totalQuery = $mysqli->query($totalSql);
$totalResult = $totalQuery->fetch_object();
$total = $totalResult->total;

$totalPages = ceil($total / $limit);

$diff = 5;
$nav_first = $page - $diff < 1 ? 1 : $page - $diff;
$nav_last = $page + $diff > $totalPages ? $totalPages : $page + $diff;

if ( $page>$totalPages ) {
    header("location:".$_SERVER['PHP_SELF']."?page=1");
    exit;
}

require_once('header.php');
?>
<form action="" onsubmit="getFormsSearch(); return false;" autocomplete="off">
    <div class="input-append">
        <input class="span2" style="margin:0em 1em 0em 0; width:150px;" id="searchtext" name="searchtext" type="text" placeholder="חיפוש טופס" />
        <button class="btn" type="submit">חפש</button>
    </div>
</form>

<div id="forms_list">
</div>

<table class="table table-striped">
  <thead>
    <tr>
      <td colspan="8">טפסים אחרונים</td>
    </tr>
	
	<tr>
		<td>ID</td>
		<td>שם לקוח</td>
        <td>שם מגיש הבקשה</td>
        <td>מספר תעודת זהות</td>
		<td>קובץ</td>
		<td class="center">מספר טלפון</td>
        <td>שולם</td>
		<td style="width:200px;" class="center">תאריך שליחת בקשה</td>
	</tr>
  </thead>
  <tbody>
		<?php
		while ( $row = $query->fetch_assoc() )
		{
		?>
		<tr>
			<td class="center"><?php echo $row['id'];?></td>
			<td><?=$row['full_name'];?></td>
            <td><?=($row['ApplicantName'] ? ucwords($row['ApplicantName']) : '');?></td>
            <td><?=($row['nationalid'] ? $row['nationalid'] : '');?></td>
			<td class="ltr center"><a href="getForm.php?FileName=<?=$row['FileName'];?>" class="icon-file-text" style="font-size:17px; color:#808080" target="_blank"></a></td>
			<td class="center"><?php echo $row['phone'];?></td>

            <td><?=($row['usedPaymentHash'] == 1 ? '<i class="icon-money"></i>' : '')?></td>
			<td class="center"><?php echo date("H:i, d-m-Y",$row['filltime']);?></td>
		</tr>
		<?php
		}
		?>
  </tbody>

</table>

<div class="pagination">
    <ul>
        <li class="<?=($page <= 1 ? 'disabled' : '');?>"><a href="<?=($page <= 1 ? '#' : $_SERVER['PHP_SELF'].'?page='.($page-1));?>">הקודם</a></li>
        <?php
        for($i=$nav_first; $i<=$nav_last; $i++) {
            echo '<li '.($i == $page ? 'class="active"' : '').'><a href="'.$_SERVER['PHP_SELF'].'?page='.$i.'">'.$i.'</a></li>';
        }
        ?>
        <li class="<?=($page >= $totalPages ? 'disabled' : '');?>"><a href="<?=($page >= $totalPages ? '#' : $_SERVER['PHP_SELF'].'?page='.($page+1));?>">הבא</a></li>
    </ul>
</div>
