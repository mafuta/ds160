<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="robots" content="no-cache, no-follow" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="he-il" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-cache" />

	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/ds-panel/admin.js"></script>

    <script type="text/javascript" src="/ds-panel/assets/bootstrap-rtl/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/ds-panel/assets/bootstrap-rtl/bootstrap.min.css">

    <link class="type" rel="stylesheet" href="/ds-panel/assets/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="admin.css" />
		
	<script type="text/javascript">	
	function getuserlist()
	{
        var $userlist = $("#user_list");
        $userlist.slideUp("quick");
		var searchtext = document.getElementById("searchtext").value;

		if (searchtext.length<2){return false;}

		$.ajax({
			type: 'POST',
			url: 'getuserlist.php',
			data: {'searchtext':searchtext},
			success: function(response){

                if (response!=""){
                    $userlist.html(response);
                }
                else
                {
                    var $error = $("<div/>", {"class":"alert alert-block"}).html("לא נמצאו תוצאות לחיפוש").css({"color":"#808080"});
                    $userlist.html($error);
                }
                $userlist.slideDown('slow');
			}
		});
	
	}

    function getFormsSearch()
    {
        var $formsList = $("#forms_list");

        $formsList.slideUp("quick");
        var searchtext = document.getElementById("searchtext").value;

        if (searchtext.length<2){return false;}

        $.ajax({
            type: 'POST',
            url: 'getformslist.php',
            data: {'searchtext':searchtext},
            success: function(response){
                if (response!=""){
                    $formsList.html(response);
                }
                else
                {
                    var $error = $("<div/>", {"class":"alert alert-block"}).html("לא נמצאו תוצאות לחיפוש").css({"color":"#808080"});
                    $formsList.html($error);
                }
                $formsList.slideDown('slow');
            }
        });

    }
	</script>
  
	<title><?php echo $base['SITE_TITLE']; ?></title>
</head>
<body>
  <div id="overlay" onclick="return TooltipClose();"></div>
  <div id="Tooltip"></div>

	<div id="admin_container" class="container">
		<?php
		if (isset($_SESSION['is_admin']))
		{
		?>
		<div class="fixed-menu">
			<div>
			<?php
				echo 'ברוך הבא '.$_SESSION['AdminName'].',&nbsp;';
				echo '<a style="cursor:pointer;" onclick="window.location.href=\'logout.php\';">התנתק</a>';
			?>
			</div>
		</div>
		<?php
		}
		?>
		
		<div style="margin:1em auto; margin-top:3em; width:200px; font-weight:bold; height:25px; font-size:20px;">
		DS160 - מערכת ניהול
		</div>
		
		<?php
		if (isset($_SESSION['is_admin']))
		{
			$menuitems =[
                ['משתמשים','users.php'],
                ['טפסים','forms.php'],
                ['הודעות','messages.php'],
                ['הרשאות ניהול','privileges.php'],
                ['מערכת','system.php']
            ];
		?>
		<ul class="nav nav-tabs">
			<?php
			foreach($menuitems as $menu_item)
			{
				$checked = false;
				if (basename($_SERVER['PHP_SELF'])==$menu_item[1]) {
                    $checked = true;
                }
			?>	
			<li class="<?=($checked ? 'active' : '');?>"><a href="<?=$menu_item[1];?>"><?=$menu_item[0];?></a></li>
			<?php
			}
			?>	
		</ul>
		<?php
		}
		?>