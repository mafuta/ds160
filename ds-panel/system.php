<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();


if (!isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id'])){
  	unset($_SESSION);
  	header('location:login.php');
  	exit;
}

require_once('header.php');
?>

<form id="blockIP" autocomplete="off">
	<table class="table table-striped">
		<thead>
			<tr>
				<td colspan="2">חסימת כתובת IP</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><label for="ip">כתובת לחסימה:</label></td>
				<td><input type="text" id="ip" name="ip" /></td>
			</tr>

			<tr>
				<td><label for="blockReason">סיבה</label></td>
				<td>
					<select id="blockReason" name="blockReason">
						<option value="">בחר</option>
						<?php
						$mysqli = db::get_instance();
						$sql = "SELECT * FROM blockReason ORDER BY reasonID ASC;";
						$result = $mysqli->query($sql);
						
						while ( $row = $result->fetch_object() )
						{
						?>
						<option value="<?=$row->reasonID;?>"><?=$row->title;?></option>
						<?php
						}
						?>
					</select>
				</td>
			</tr>
			
			<tr>
				<td colspan="2"><button class="btn btn-primary" onclick="return DsAdmin.System.blockIP();">הוספה</button></td>
			</tr>
		</tbody>
	</table>
</form>

<table class="table table-striped">
	<thead>
		<tr>
			<td colspan="4">10 חסימות אחרונות</td>
		</tr>
		
		<tr>
			<td>כתובת</td>
			<td>סיבה</td>
			<td>מנהל חוסם</td>
			<td>תאריך\שעה</td>
		</tr>
	</thead>
	<tbody>
		<?php
		$sql = "SELECT 
					blocked_ips.ip, blockReason.title as blockReason, admins.name as admin_name, blocked_ips.time_blocked
				FROM
					blocked_ips
				LEFT JOIN
					blockReason USING(reasonID)
				INNER JOIN
					admins ON(blocked_ips.adminID = admins.id)
				WHERE
					active=1
				ORDER BY blockID DESC LIMIT 0,10;";
				
		$result = $mysqli->query($sql);
		
		while ($row = $result->fetch_object())
		{
		?>
		<tr>
			<td><?=long2ip($row->ip);?></td>
			<td><?=( !empty($row->blockReason) ? $row->blockReason : "לא צויין");?></td>
			<td><?=$row->admin_name;?></td>
			<td><?=date("H:i:s <b>d-m-Y</b>", $row->time_blocked);?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
<?php
require_once('footer.php');
?>