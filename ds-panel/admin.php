<?php

$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

//Is admin session isn't set or the session value isn't true
if ( !isset($_SESSION['is_admin']) || empty($_SESSION['is_admin']) )
{
	header('location:index.php');
	exit;
}

$AdminID = (int) $_GET['AdminID'];

//If CustomerID wasn't sent to the page or the customerID doesn't exist
if ( empty($AdminID) || !admin::exist($AdminID) )
{
	echo 'error';
	exit;
}

$sql = "SELECT
			id, username, email, name, permission, reg_date, last_login
		FROM
			admins
		WHERE
			id='{$AdminID}';";
			
$result = $mysqli->query($sql);

$row = $result->fetch_object();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="admin.css">

	<script type="text/javascript" src="https://<?php echo $_SERVER['SERVER_NAME'];?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="https://<?php echo $_SERVER['SERVER_NAME'];?>/ds-panel/admin.js"></script>


    <script type="text/javascript" src="/ds-panel/assets/bootstrap-rtl/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/ds-panel/assets/bootstrap-rtl/bootstrap.min.css">

    <link class="type" rel="stylesheet" href="/ds-panel/assets/font-awesome/css/font-awesome.min.css">

</head>

<body>
  <form id="admin_data" autocomplete="off">
		<input type="hidden" name="AdminID" id="AdminID" value="<?php echo $row->id;?>">
		<table class="table sub-table">
            <thead>
                <tr>
                    <th>כרטיס מנהל - <?php echo $row->name;?></th>
                </tr>
            </thead>
			<tr>
				<td style="vertical-align:top; border-left:1px dashed #d7d7d7;">
				
					<table class="CustTable">
						<tr>
						  <td>שם מלא:</td>
						  <td><input type="text" name="name" id="name" value="<?php echo $row->name;?>"></td>
						</tr>
						
						<tr>
							<td>שם משתמש:</td>
							<td><input type="text" name="username" id="username" value="<?php echo $row->username;?>"></td>
						</tr>
						
						<tr>
							<td>סיסמה חדשה:</td>
							<td><input type="password" name="password" id="password"></td>
						</tr>
						
						<tr>
							<td>אימות סיסמה חדשה:</td>
							<td><input type="password" name="password_r" id="password_r"></td>
						</tr>
	
						<tr>
							<td>דוא"ל:</td>
							<td><input type="email" name="email" id="email" value="<?php echo $row->email;?>"></td>
						</tr>
				
						<tr>
							<td>דוא"ל:</td>
							<td>
								<select id="permission" name="permission">
									<?php
									$sql = "SELECT id, text FROM permissions ORDER BY priority ASC;";
									$result = $mysqli->query($sql);
									
									while ($permissionRow = $result->fetch_object() )
									{
										echo '<option value="'.$permissionRow->id.'" '.($permissionRow->id==$row->permission ? 'selected="selected"' : '').'>'.$permissionRow->text.'</option>';
									}
									?>
								</select>

							</td>
						</tr>
				
						<tr>
							<td>תאריך הרשמה:</td>
							<td><?php echo date('d-m-Y', $row->reg_date);?></td>
						</tr>
						
						<?php
						if ( !empty($row->last_login) )
						{
						?>
						<tr>
							<td>התחברות אחרונה:</td>
							<td><?php echo date('H:i <b>d-m-Y</b>',$row->last_login);?></td>
						</tr>
						<?php	
						}
						?>


						
						
						<tr>
							<td colspan="2"><button class="btn" onclick="return DsAdmin.Permissions.updateAdmin('admin_data');">עדכון פרטים</button></td>
						</tr>
					</table>
				</td>

			</tr>
		</table>
  </form>
  
</body>
</html>