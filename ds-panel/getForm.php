<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

if (!isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id'])){
    unset($_SESSION);
    header('location:login.php');
    exit;
}



/**
*
* @Used to copy all ds forms from site root to server root
*
*
// Identify directories
$source = $_SERVER['DOCUMENT_ROOT'].'/forms/';
$destination = "../../../forms/";

// Get array of all source files
$files = scandir($source);


// Cycle through all source files
foreach ($files as $file)
{
  if (is_dir($file) || in_array($file, array(".",".."))) continue;
  
  $filemtime = filemtime($source.$file);

  // If we copied this successfully, mark it for deletion
  if (copy($source.$file, $destination.$file)) {
 	touch($destination.$file, $filemtime);
    $delete[] = $source.$file;
  }
}

// Delete all successfully-copied files
foreach ($delete as $file) {
  unlink($file);
}*/



if (!file_exists('../../forms/'.$_GET['FileName']))
{
	echo 'Application file does not exist';
	exit;
}else{
	readfile('../../forms/'.$_GET['FileName']);
}