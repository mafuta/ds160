<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();


if (!isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id'])){
  	unset($_SESSION);
  	header('location:login.php');
  	exit;
}

require_once('header.php');

?>

<div id="adminList">
	<?php require_once("./controls/adminList.php"); ?>
</div>

	
	
<form id="newAdmin" autocomplete="off">

	<table class="table table-striped">
		<thead>
			<tr>
				<th colspan="2">יצירת חשבון מנהל</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><label for="username">שם משתמש:</label></td>
				<td><input type="text" id="username" name="username" /></td>
			</tr>
			
			<tr>
				<td><label for="name">שם מלא:</label></td>
				<td><input type="text" id="name" name="name" /></td>
			</tr>
			
			<tr>
				<td><label for="password">סיסמה:</label></td>
				<td><input type="password" id="password" name="password" /></td>
			</tr>
			
			<tr>
				<td><label for="password_r">אימות סיסמה:</label></td>
				<td><input type="password" id="password_r" name="password_r" /></td>
			</tr>
			
			<tr>
				<td><label for="email">דוא"ל:</label></td>
				<td><input type="email" id="email" name="email" placeholder="example@ds160.co.il" /></td>
			</tr>
			
			<tr>
				<td><label for="permission">הרשאה</label></td>
				<td>
					<select id="permission" name="permission">
						<option value="2" selected="selected">נציג</option>
						<option value="1">מנהל מערכת</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td colspan="2"><button class="btn btn-primary" onclick="return DsAdmin.Permissions.addAdmin('newAdmin');">הוספה</button></td>
			</tr>
		</tbody>
	</table>
</form>

<table class="table table-striped">
	<thead>
		<tr>
			<td colspan="3">לוג 10 התחברויות אחרונות</td>
		</tr>
		
		<tr>
			<td>שם נציג\מנהל</td>
			<td>זמן התחברות</td>
			<td>כתובת IP</td>
		</tr>
	</thead>
	<tbody>
		<?php
		$sql = "SELECT 
					adminlog.adminid, adminlog.time AS TimeConnected, adminlog.ip, admins.name 
				FROM 
					adminlog 
				LEFT JOIN
					admins ON (admins.id = adminlog.adminid)
				ORDER BY adminlog.id DESC LIMIT 10;";
				
		$result = $mysqli->query($sql);
		
		while ($row = $result->fetch_object())
		{
		?>
		<tr>
			<td><?=$row->name;?></td>
			<td><?=date('H:i - <b>d-m-Y</b>', $row->TimeConnected);?></td>
			<td><?=long2ip($row->ip);?></td>
		</tr>
		<?php
		}
		?>
	</tbody>
</table>
<?php
require_once('footer.php');
?>