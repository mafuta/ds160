<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

if ( isset($_SESSION['is_admin']) && isset($_SESSION['admin_id']) )
{
    header('location:main.php');
    exit;
}

if ( isset($_POST['connect']) )
{
    $username = $mysqli->escape($_POST['username']);
    $password = !empty($_POST['password']) ? $_POST['password'] : "";

    $sql = "SELECT * FROM admins WHERE username='{$username}' AND password='".md5($password)."' LIMIT 1;";

    if ($result = $mysqli->query($sql)) {
        if ( $result->num_rows ) {
            $row = $result->fetch_object();

            $ip = system::getUserIP(true);

            $_SESSION['is_admin']   = true;
            $_SESSION['admin_id']   = $row->id;
            $_SESSION['AdminName']  = $row->name;

            $sql = "UPDATE admins SET last_ip='{$ip}', last_login='".time()."' WHERE admins.id='{$row->id}';";
            $mysqli->query($sql);

            admin::logConnection($row->id);

            header('location:users.php');
			exit;
        }
    }
}

require_once('header.php');
?>

<style type="text/css">
    input[type=text], input[type=password]{
        min-width:200px;
        height:30px;
        line-height:30px;
        
        box-shadow:inset -2px 2px 1px 0px rgb(233, 233, 233);
        
        padding:3px 6px;
        border-radius:5px;
        border:1px solid rgb(219, 219, 219);
    }
    
    input[type=text]:focus, input[type=password]:focus{
        box-shadow:0px 0px 9px -1px #2989d8, inset -2px 2px 1px 0px rgb(233, 233, 233);
        outline:0;
    }
    
    .admin-connect{
        width:270px; margin:2em; margin:0 auto; text-align:center;
    }
    
    .admin-connect .title{
        font-size:14px; font-weight:bold; text-align:center; margin:1em auto;
    }
    
    .admin-connect .line{
        margin-bottom:1em;
    }
    
</style>
<div class="admin-connect">
	<div class="title">התחברות</div>
	
	<form autocomplete="off" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
            <div class="line">
                <input type="text" placeholder="שם משתמש" id="username" name="username" />
            </div>
            
            <div class="line">
                <input type="password" placeholder="סיסמה" id="password" name="password" />
            </div>
            
            <div class="line">
                    <button class="btn btn-primary" type="submit" id="connect" name="connect" value="1">התחבר</button>
            </div>
	</form>
</div>
<?php
require_once('footer.php');
?>