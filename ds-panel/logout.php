<?php
session_start();
unset($_SESSION['is_admin']);
unset($_SESSION['admin_id']);
unset($_SESSION['AdminName']);
session_destroy();
header('location:login.php');
exit;
