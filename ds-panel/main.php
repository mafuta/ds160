<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

if (!isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id'])){
  	unset($_SESSION);
  	header('location:login.php');
  	exit;
}

require_once('header.php');


$query  = $mysqli->query("SELECT id,username,email,phone,reg_date,full_name FROM customers ORDER BY id DESC LIMIT 0,5;");
?>

<table class="dstable">
  <thead>
		<tr>
			<td colspan="6">משתמשים אחרונים</td>
		</tr>
	
		<tr>
			<td style="width:50px;">ID</td>
			<td>שם לקוח</td>
			<td>שם משתמש</td>
			<td>מספר טלפון</td>
			<td>תאריך הרשמה</td>
		</tr>
  </thead>

  <tbody>

	  
		<?php
		$i=0;
		while ($row = $query->fetch_object())
		{
			$class = ($i%2==1 ? 'grayline' : '');

			/*openWindow('customer.php?CustomerID='+<?php echo $row['id'];?>, '<?php echo str_replace("'","",$row['full_name']);?>', '900', '700');*/
			?>
			<tr class="<?=$class;?>" style="cursor:pointer;" onclick="toggleCustomerFrame(<?php echo $row->id;?>);">
				<td><?php echo $row->id;?></td>
				<td><?php echo $row->full_name;?></td>
				<td><?php echo $row->username;?></td>
				<td><?php echo $row->phone;?></td>
				<td><?php echo date("d-m-Y",$row->reg_date);?></td>
			</tr>
			
			<tr id="customer_<?php echo $row->id;?>" style="display:none;">
				<td colspan="5"></td>
			</tr>
		<?php
			$i++;
		}
		?>
  </tbody>
</table>

<div style="display:block; position:relative; clear:both; margin:5em 1em 0 0;">
  <input id="user_name" name="user_name" type="text" placeholder="חיפוש טופס" onkeydown="javascript:getuserlist();" />
  <div id="user_list">
    
  </div>
</div>
<?php
require_once('footer.php');
?>
