<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();

if (!isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id'])){
  	unset($_SESSION);
  	header('location:login.php');
  	exit;
}

require_once('header.php');

$page = isset($_GET["page"]) && is_numeric($_GET["page"]) ? (int) $_GET["page"] : 1;
if ( empty($page) ) {
    $page = 1;
}
$limit = 15;
$starting = ($page-1) * $limit;

$sql	= "SELECT SQL_CALC_FOUND_ROWS id,username,email,phone,reg_date,full_name FROM customers ORDER BY id DESC LIMIT $starting, $limit;";
$query  = $mysqli->query($sql);

$totalSql = "SELECT FOUND_ROWS() as total;";
$totalQuery = $mysqli->query($totalSql);
$totalResult = $totalQuery->fetch_object();
$total = $totalResult->total;

$totalPages = ceil($total / $limit);

$diff = 5;
$nav_first = $page - $diff < 1 ? 1 : $page - $diff;
$nav_last = $page + $diff > $totalPages ? $totalPages : $page + $diff;

if ( $page>$totalPages ) {
    header("location:".$_SERVER['PHP_SELF']."?page=1");
    exit;
}
?>
  <script type="text/javascript">
  $(document).ready(function(){
    $(document).on("click", ".customer-row", function(){
        var CustID = $(this).attr("custid");

        if ( $(this).next("#customer_"+CustID).length > 0 )
        {
            var customerRow = $(this).next("#customer_"+CustID);
        }else{
            return false;
        }

        if ( customerRow.is(":visible") )
        {
            customerRow.hide();
        }else{
            var $frame = $("<iframe/>", {src:"https://<?=$_SERVER['SERVER_NAME'];?>/ds-panel/customer.php?CustomerID="+CustID}).css({border:0, width:"100%", display:"inline-block", height:"500px"});
            customerRow.find("td").html($frame);
            customerRow.show();
        }

    });
  });
  </script>

    <form action="" onsubmit="getuserlist(); return false;" autocomplete="off">
        <div class="input-append">
            <input class="span2" style="margin:0em 1em 0em 0; width:150px;" id="searchtext" name="searchtext" type="text" placeholder="חיפוש משתמש">
            <button class="btn" type="submit">חפש</button>
        </div>
    </form>

    <div id="user_list">
    </div>

    <table class="table striped-hidden">
      <thead>
            <tr>
                <td colspan="6">משתמשים אחרונים</td>
            </tr>

            <tr>
                <td>ID</td>
                <td>שם לקוח</td>
                <td>שם משתמש</td>
                <td>מספר טלפון</td>
                <td>תאריך הרשמה</td>
            </tr>
      </thead>

      <tbody>
            <?php
            while ( $row = $query->fetch_object() )
            {
                ?>
                <tr class="customer-row pointer" custid="<?=$row->id?>">
                    <td><?=$row->id?></td>
                    <td><?=stripslashes($row->full_name)?></td>
                    <td><?=$row->username?></td>
                    <td><?=$row->phone?></td>
                    <td><?=date("d-m-Y",$row->reg_date)?></td>
                </tr>

                <tr id="customer_<?=$row->id;?>" class="customer-info-row">
                    <td colspan="5"></td>
                </tr>
            <?php
            }
            ?>
      </tbody>
    </table>


    <div class="pagination">
        <ul>
            <li class="<?=($page <= 1 ? 'disabled' : '');?>"><a href="<?=($page <= 1 ? '#' : $_SERVER['PHP_SELF'].'?page='.($page-1));?>">הקודם</a></li>
            <?php
            for($i=$nav_first; $i<=$nav_last; $i++) {
                echo '<li '.($i == $page ? 'class="active"' : '').'><a href="'.$_SERVER['PHP_SELF'].'?page='.$i.'">'.$i.'</a></li>';
            }
            ?>
            <li class="<?=($page >= $totalPages ? 'disabled' : '');?>"><a href="<?=($page >= $totalPages ? '#' : $_SERVER['PHP_SELF'].'?page='.($page+1));?>">הבא</a></li>
        </ul>
    </div>
<?php
require_once('footer.php');
?>