<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

if ( !isset($_SESSION['is_admin']) || !isset($_SESSION['admin_id']) ){
  	unset($_SESSION);
  	exit;
}

$mysqli = db::get_instance();

$sql = "SELECT 
			admins.id, admins.username, admins.email, admins.last_ip, admins.last_login, admins.name, admins.permission,
			permissions.text AS PermissionText
		FROM 
			admins
		LEFT JOIN
			permissions ON(admins.permission = permissions.id)
		ORDER BY 
			permissions.priority ASC, admins.id ASC";

$query  = $mysqli->query($sql);
?>
<table class="table striped-hidden">
  <thead>
		<tr>
			<td colspan="7">הרשאות ניהול</td>
		</tr>
	
		<tr>
			<td style="width:50px;">ID</td>
			<td>שם</td>
			<td>שם משתמש</td>
			<td>אימייל</td>
			<td>IP אחרון</td>
			<td>תאריך התחברות אחרון</td>
			<td>הרשאה</td>
		</tr>
  </thead>

  <tbody>

	  
		<?php
		while ($row = $query->fetch_object())
		{
			?>
			<tr class="pointer" onclick="DsAdmin.Permissions.toggleAdminFrame(<?php echo $row->id;?>);">
				<td><?php echo $row->id;?></td>
				<td><?php echo $row->name;?></td>
				<td><?php echo $row->username;?></td>
				<td><?php echo $row->email;?></td>
				<td><?php echo long2ip($row->last_ip);?></td>
				<td>
				<?php 
				if ( !empty($row->last_login) )
				{
					echo date("d-m-Y",$row->last_login);	
				}else{
					echo "לא התחבר";
				}
				?></td>
				<td><?php echo $row->PermissionText;?></td>
			</tr>
			
			<tr id="admin_<?php echo $row->id;?>" style="display:none;">
				<td colspan="7"></td>
			</tr>
		<?php
		}
		?>
  </tbody>
</table>