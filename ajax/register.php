<?php
$secured = true;
require_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/base.php");

if ( !empty($_POST['username']) ){
  //Getting connection to the database using the db class
	$mysqli = db::get_instance();

	$name     = trim($_POST['app_name']);
	$username = trim($_POST['username']);
	$password = trim($_POST['pass']);
	$email    = trim($_POST['email']);
	$phone    = trim($_POST['phone']);

	$errors = [];

	if (!filter_var($email,FILTER_VALIDATE_EMAIL)){
		echo "invalid email";
		exit;
	}elseif (ds160::email_exist($email)){
		echo "email exist";
		exit;
	}

	if (!preg_match('/^[a-zA-Z][a-zA-Z0-9_\-\.]+$/',$username)){
		echo "invalid username";
		exit;	
	}elseif (ds160::username_exist($username)){
		echo "username exist";
		exit;
	}
    
	if (strlen($password)<5){
		$errors[] = "password short";
	}

	$reg_ip         = system::getUserIP(true);
	$reg_date       = time();

	$customerData = [
		"email"             => $email,
		"username"          => $username,
		"password"          => md5($password.$salt),
		"full_name"         => $name,
		"phone"             => $phone,
		"reg_date"          => $reg_date,
		"last_login_date"   => $reg_date,
		"reg_ip"            => $reg_ip,
		"last_login_ip"     => $reg_ip
	];

	$result = $mysqli->insert("customers", $customerData);

	if ( !empty($result) ){
		unset($_POST);
		$CustID = $mysqli->insert_id;
		$_SESSION['USER_ID']   	 	= $CustID;
		$_SESSION['USERNAME']   	= $username;
		$_SESSION['CustomerName']	= $name;
		$_SESSION['USER_HASH']  	= md5($mysqli->insert_id.$email.$reg_date.$reg_ip);

        $ds160 = new ds160();
        $ds160->sendRegistrationMail($CustID);

		echo "success";
		exit;
	}else{
		echo "error";
		exit;
	}
}