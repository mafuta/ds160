<?php

//checkSecured will take care that the message is sent from secured page
//$secured = true;

require_once( $_SERVER['DOCUMENT_ROOT'] . '/inc/base.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/mailer/swift_required.php' );

if ( strtolower($_SERVER["REQUEST_METHOD"]) == "post" )
{

	$conn = db::get_instance();

  	$sender = new stdClass();
  	$sender->ip       = $_POST['contact_ip'];
  	$sender->email    = $_POST['contact_email'];
  	$sender->name     = $_POST['contact_name'];
  	$sender->phone    = $_POST['contact_phone'];
  	$sender->subject  = $_POST['contact_subject'];
  	$sender->message  = $_POST['contact_message'];   

  	$errors = "";

  	if ( !filter_var($sender->email, FILTER_VALIDATE_EMAIL) ) {
      	echo "email";
      	exit;
  	}

  	//שליחת הודעה במידה ואין שגיאות
  	if ( empty($errors) ) {
	    $params = print_r($_SESSION, true) ."\n". print_r($_COOKIE, true) ."\n". print_r($_SERVER, true);

	    $IP = system::getUserIP(true);
	
		$insertData = [
            "name" 		=> $sender->name,
            "email" 	=> $sender->email,
            "phone" 	=> $sender->phone,
            "subject" 	=> $sender->subject,
            "message" 	=> $sender->message,
            "time" 		=> time(),
            "params" 	=> $params,
            "ip" 		=> $IP
		];

	    $result = $conn->insert("messages", $insertData);

        $transport = Swift_SendmailTransport::newInstance();
	    //יצירת התקשרות לשרת
	    /*$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com',587,'tls')
            ->setUsername($base["SERVICE_MAIL"])
            ->setPassword($base["SERVICE_PASS"]);*/
	    
	    //יצירת אובייקט לשליחת המייל
	    $mailer = Swift_Mailer::newInstance($transport);
	    
		//Send auto respond email
	    $message = Swift_Message::newInstance()
	      ->setSubject("מענה אוטומטי - ds160")
	      ->setFrom([$base["SERVICE_MAIL"] => "שירות לקוחות DS160"])
	      ->setSender([$base["SERVICE_MAIL"] => "שירות לקוחות DS160"])
	      ->setReplyTo([$base["SERVICE_MAIL"] => "שירות לקוחות DS160"])
	      ->setTo($sender->email)
	      ->setReturnPath($sender->email)
	      ->setCharset('utf-8')
	      ->setBody(
	      	'<!DOCTYPE html>
			<html dir="rtl">
			<head>
				<meta charset="utf-8" />
			</head>
			<body>
				<table cellpadding="0" style="direction: rtl; margin: 0 auto; height: 100% !important; width: 100%; background: #ffffff;" align="center">
					<tr align="center">
						<td align="right">
	      		
					    <span style="font-weight:bold; font-size:14px;">שלום,</span>
						<br /><br />
					    תודה שפנית לשירות הלקוחות של <a href="https://www.ds160.co.il" style="text-decoration:underline; font-weight:bold; color:#000;">ds160.co.il</a>
						<br /><br />
						אנו מטפלים בפנייתך ונענה לך בהקדם האפשרי!
						<br /><br />
						בברכה,<br />
						צוות השרות והתמיכה
						<br />
						<a href="https://www.ds160.co.il" style="text-decoration:underline; font-weight:bold; color:#000;">www.DS160.co.il</a>
						
						<div style="margin:1em auto; height:1px; width:100%; border-top:1px solid gray;"></div>
				
						<img src="https://www.ds160.co.il/img/mail_logo.png" />
					</tr>
				</table>
	      		
			</body>
			</html>',	
	        'text/html' // Mark the content-type as HTML
	    );
		
		$mailer->send($message, $failures);
		
	    //Send message to customer service department
	    $message = Swift_Message::newInstance()
	      ->setSubject("הודעה ממשתמש באתר")
	      ->setFrom([$sender->email => $sender->name])
	      ->setSender([$sender->email => $sender->name])
	      ->setReplyTo([$sender->email => $sender->name])
	      ->setTo($base["SERVICE_MAIL"])
	      ->setReturnPath($sender->email)
	      ->setCharset('utf-8')
	      ->setBody(
	      	'<!DOCTYPE html>
			<html dir="rtl">
			<head>
				<meta charset="utf-8" />
			</head>
			<body>
				<table cellpadding="0" style="direction: rtl; margin: 0 auto; height: 100% !important; width: 100%; background: #ffffff;" align="center">
					<tr align="center">
						<td align="right">
	      		
					    <strong>שם השולח: '. $sender->name . '<br />' . '
	      				<strong>דוא"ל: '. $sender->email . '<br />' . '
					    נושא: '. $sender->subject . '<br /><br />' . '
						טלפון: '. $sender->phone . '<br /><br />' . '
						
					    הודעה:</strong><br /><br />'. nl2br($sender->message) . '

						<div style="margin:1em auto; height:1px; width:100%; border-top:1px solid gray;"></div>
				
						<img src="https://www.ds160.co.il/img/mail_logo.png" />
					</tr>
				</table>
	      		
			</body>
			</html>',	
	        'text/html' // Mark the content-type as HTML
	    );
	
	    $failures = null;
	    if ( $mailer->send($message, $failures) || $result )
	    {
	    	echo 'okay';
			exit;
	    }
  	}
}

echo 'error';