<?php
$secured = true;
include_once($_SERVER["DOCUMENT_ROOT"]."/inc/base.php");

$mysqli = db::get_instance();

$email = !empty($_POST["email"]) ? $_POST["email"] : "";

/**
 * error1 = the given email address is not a valid adress
 */
if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ){
  echo "error1";
  exit;
}

$domain = explode("@", $email);
if (!getmxrr($domain[1], $mxhosts)){
  echo "error1";
  exit;
}

$email = $mysqli->escape($email);

$sql = "SELECT id FROM customers WHERE email='{$email}';";
if ($result = $mysqli->query($sql)){
  $row = $result->fetch_object();

  if ($result->num_rows){
    $md5_hash = md5(time().$salt.$_POST["email"].rand(50,1000));
    $md5_hash = substr($md5_hash,0,16);

    $sql = "UPDATE customers SET UserHash='{$md5_hash}', UserHashUsed=0 WHERE email='{$email}';";
    $mysqli->query($sql);

    if (ds160::SendPassResetMail($row->id)){
      echo "okay";
      exit;
    }else{
      echo "error3";
      exit;
    }
  } else {
    //error2 = email not found in the database
    echo "error2";
    exit;
  }
}

echo "error3";