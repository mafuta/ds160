<?php
$secured = true;
include_once($_SERVER["DOCUMENT_ROOT"]."/inc/base.php");

if (!isset($_GET["user"])){
	echo false;
	exit;
}

$mysqli = db::get_instance();

$username = trim($_GET["user"]);

if (empty($username)){
	echo "true";
	exit;
}

$sql = "SELECT id FROM customers WHERE username='{$username}';";

if ($result = $mysqli->query($sql)){
	if ($result->num_rows > 0){
		echo "true";
		exit;
	}
}
echo "false";