<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/header.php');



$conn = db::get_instance();

$Order = ( isset($_GET['Order']) ? $_GET['Order'] : null );

if ( empty($Order) )
{
	header("location: index.php");
	exit;
}

list($paymentHash, $CustID) = explode(";;", $Order);

$sql = "SELECT id FROM dsforms WHERE PaymentHash='{$paymentHash}' AND CustomerID='{$CustID}';";

$result = $conn->query($sql);

if ( !empty($conn->errno) || $result->num_rows <= 0 )
{
	header("location: index.php");
	exit;
} 

$row = $result->fetch_object();

$logPaymentError = array(
	"formID" 		=> $row->id,
	"errorCode"		=> (int) $_GET['CCode'],
	"amount"		=> $_GET['Amount'],
	"paymentPhone"	=> $_GET['Fild3'],
	"paymentEmail"	=> $_GET['Fild2']
);

$conn->insert("paymentErrors", $logPaymentError);
?>
<style type="text/css">
    .payment-error .error-sign{
        background:url('/img/paymentError.jpg') no-repeat;
        width:291px;
        height:157px;
        margin:1em auto;
    }
    
    .payment-error .error-info
    {
        background:#ffffcb;
        height:100px;
        width:100%;  
    }
    
    .payment-error .error-info .error-info-text{
        font-size:22px;
        font-weight:bold;
        margin-right:20px;
        line-height:100px;
		text-align:center;
    }
    
    .payment-error .instructions
    {
        font-size: 13.5px;
        color: #52505e;
        margin: 2em auto;
        width: 100%;
        font-weight: bold;
        text-align: center;
    }
    
    .payment-error .dsbox_right
    {
        min-height:480px;
    }
    
    .payment-error .left-text{
        font-size: 12px;
        color: #52505e;
        font-weight: bold;
        width:100%;
        margin-bottom:1em;
    }
    
    .payment-error .try-again-button
    {
        border:1px solid #d2d2d2;
        width:150px;
        height:40px;
        line-height:40px;
        text-align:center;
        margin:0 auto;
        display:block;
        
        font-size: 13px;
        color: #52505e;
        font-weight:bold;
        
        border-radius:4px;
        
        box-shadow:0px 0px 4px 1px rgb(196, 196, 196);
        
    }
    
    .try-again-button:active{
        border:1px solid #c3c3c3;
    }
</style>
<div class="progressBar step6"></div>  
<div id="rgs" class="dsbox payment-error" style="width:950px; margin-top:1em;">
    <div class="dsbox_left" style="width:220px; word-wrap:break;">
            <div style="font-weight:bold; font-size:15px; margin-bottom:1em;">צריכים עזרה?</div>
        
            <div class="left-text">
                התקשרו לקבלת נציג שירות: 052-8518665
                <br />
                <br />
                או שלחו לנו מייל: info@ds160.co.il
            </div>
        
            <span class="mail-address" onclick="Tooltip('contact');">טופס יצירת קשר</span>    
        </p>
    </div>
    
    <div class="dsbox_right" style="width:670px;">
        <div class="error-sign"></div>
    
        <div class="error-info">
            <div class="error-info-text">
            <?php
			$errorCode = (int) ( isset($_GET['CCode']) ? $_GET['CCode'] : 0 );
			
			switch($errorCode)
			{
				case 6:
					echo "מספר תעודת הזהות או שלוש ספרות הבטחון בגב הכרטיס שגויים";
				break;
				
				case 36:
					echo "כרטיס האשראי שהזנת פג תוקף";
				break;
				
				case 57:
					echo "לא הוקלד מספר תעודת הזהות של בעל הכרטיס";
				break;
				
				case 58:
					echo "יש להזין 3 ספרות בטחון בגב כרטיס האשראי";
				break;
				
				case 59:
					echo "לא הוקלדו מספר תעודת הזהות ושלוש ספרות הבטחון בגב הכרטיס";
				break;
				
				case 64:
					echo "לא ניתן לזהות את סוג כרטיס האשראי";
				break;
				
				default:
					echo "אירעה שגיאה בחיוב הכרטיס, אנא פנה לחברת האשראי";
				
			}
			?>
            </div>
        </div>
           
        
        <div class="instructions">
            בקשתך נקלטה במערכת, באפשרותך לנסות בשנית או ליצור קשר עם נציג שירות על מנת לבצע תשלום טלפוני.
        </div>
       
        <button class="try-again-button" onclick="history.back();">נסה שוב</button>
    </div>
    
	<br /><br />
</div>
<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php');