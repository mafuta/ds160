<?php
$secured = true;

require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/header.php');
?>
	<div class="progressBar step2"></div>  
  	<div class="returning-page dsbox" style="width:950px; margin-top:1em;">
    	<div class="dsbox_left" style="width:310px; height:405px;">
            <div class="left-title">ברוך שובך, <?=(!empty($_SESSION['CustomerName']) ? $_SESSION['CustomerName'] : "")?></div>
			<div class="left-text GrayTitle">
		על מנת שנוכל לטפל בבקשתך, יש להשלים את מילוי השאלון המתורגם באתר.
		נא וודא כי דרכון מגיש הבקשה מונח לפניך על מנת למנוע טעויות.
			</div>
			<div class="bottom-image">
	
			</div>
    	</div>
    
    <div class="dsbox_right" style="width:610px; height:405px;">
		<div class="right-upper">
			<div class="right-title">התקדמות הבקשה שלך לויזה</div>
			<div class="right-title-gray GrayTitle">השלמת בהצלחה 25% מהתהליך, נא המשך</div>
		</div>
		<div class="progressbox">
			<div class="progress-line">
                                <span style="background:url('/img/greenvi.png'); position:absolute; top:2em; right:1.2em; display:inline-block; width:18px; height:14px;"></span>
				<div class="title" style="margin-right:2.4em;">
					שלב ראשון: הרשמה
				</div>
				<div class="description" style="margin-right:2.4em;">
					הושלם בהצלחה
				</div>
			</div>
			<div class="progress-line lightyellow">
				<div class="title">
					שלב שני: פרטים אישיים ופרטי נסיעה
				</div>
				<div class="description">
					שלב לא הושלם
				</div>		
			</div>
			<div class="progress-line">
				<div class="title">
					שלב שלישי: תשלום
				</div>
				<div class="description">
					שלב לא הושלם
				</div>
				</div>
				<div class="progress-line">
					<div class="title" style="margin-top:0.5em;">
					סיום
					</div>
					<div class="description">
					
					</div>
				</div>
			</div>
			<button class="continue-button gradient" onclick="window.location.href='ds160.php';"></button>
		</div>
	</div>
<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php');
?>
