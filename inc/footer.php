	<?php
	if ( !strpos($_SERVER['PHP_SELF'],"index.php") )
	{
	?>
    <span id="siteseal" style="display:block; width:200px; text-align:center; margin:0 auto; margin-bottom:3em;">
        <img src="https://www.ds160.co.il/img/comodo_secure_100x85_transp.png" />
	</span>
	<?php
	}
	?>
	</div>

	<div id="footer_menu">
		<div style="width:940px; margin:0 auto;">
			<div class="footer-warning"></div>
		
			<div id="footer_right" style="color:#FFF; float:right; text-align:right; width:400px; margin-right:8px;">
				<span onclick="Tooltip('terms')" id="terms" style="color:#FFF; cursor:pointer;">תקנון</span>
				&emsp;
				<span onclick="Tooltip('contact')" id="contact" style="color:#FFF; cursor:pointer;">יצירת קשר</span>
				&emsp;
				<a href="howItWorks.php?newWindow=true" target="_blank" style="color:#fff !important;" alt="איך זה עובד">איך זה עובד</a>
                <?php
                $time = mktime(23, 00, 00, 02, 20, 2014);
                if ( date("Y-m-d H:i:s", time()) < date("Y-m-d H:i:s", $time) ) {
                ?>
                &emsp;
				<span onclick="Tooltip('coupon84terms')" id="coupon84terms" target="_blank" style="color:#fff !important;  cursor:pointer;" alt="תקנון מבצע 84">תקנון מבצע 84</span>
                <?php
                }
                ?>
			</div>

			<div id="copyright" style="float:left; width:50%; text-align:left; direction:ltr; color:white; font-size:11px;">
				&copy; All rights reserved, ds160.co.il <?=date("Y");?>&nbsp;|&nbsp; Site built by <a href="mailto:matan@hafuta.com" class="footer-matan">Matan Hafuta</a>
			</div>
		</div>
	</div>

</body>
</html>