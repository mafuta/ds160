<?php 
header('Content-type: text/html; charset=utf-8');

$mobileDetect = new Mobile_Detect();
if ( $mobileDetect->isMobile() && !$mobileDetect->isTablet() )
{
	header("location:index_m.php");
	exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title><?php echo $base['SITE_TITLE'];?></title>

    <?php
    if ( basename($_SERVER['PHP_SELF']) == "index.php" || basename($_SERVER['PHP_SELF']) == "registration.php" )
    {
    ?>
<meta name="robots" content="index, follow" />
    <?php
    }else{
    ?>
<meta name="robots" content="noindex, nofollow" />
    <?php
    }
    ?>

    <meta name="keywords" content="ארצות הברית, ויזה, אשרה, תייר, ארהב, ds160, שגרירות, אמריקה" >
    <meta name="title" content="ds160 - הוצאת ויזה לארצות הברית בקלות!">
    <meta name="description" content="תאום פגישה בשגרירות ארה״ב במהירות! מילוי טופס הבקשה לויזה בעברית! יעוץ הדרכה וליווי צמוד עד ליום הראיון בשגרירות!">
    <meta property="og:title" content="הוצאת ויזה לארה״ב בקלות!">
    <meta property="og:description" content="תאום פגישה בשגרירות ארה״ב במהירות! מילוי טופס הבקשה לויזה בעברית! יעוץ הדרכה וליווי צמוד עד ליום הראיון בשגרירות!">
    <meta property="og:type" content="company">
    <meta property="og:url" content="https://www.ds160.co.il">
    <meta property="og:image" content="http://www.ds160.co.il/img/fb_share.jpg">
    <meta property="og:site_name" content="&#x5d2;&#039;&#x5d5;&#x5e0;&#x5ea;&#x5df; &#x5d5;&#x5d9;&#x5d6;&#x5d4;">
    <meta property="fb:admins" content="1059865675,696819251">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" type="text/css" href="<?=SiteURL;?>/getResource.php?Type=css&File=ds160.min">

    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <?php
    //strcasecmp - binary safe case insensetive comprasion
    if ( strcasecmp( basename($_SERVER['PHP_SELF']), 'PaymentConfirmation.php' )==0 || strcasecmp( basename($_SERVER['PHP_SELF']), 'PaymentError.php' )==0 )
    {
        echo '<link rel="stylesheet" type="text/css" href="'.SiteURL.'/getResource.php?Type=css&File=finish" />';
    }
    elseif( basename($_SERVER['PHP_SELF']) == 'index.php' )
    {
        echo '<link rel="stylesheet" type="text/css" href="'.SiteURL.'/getResource.php?Type=css&File=index" />';
    }
    ?>

    <script src="<?=SiteURL;?>/getResource.php?Type=js&File=ds160.min&ver=2"></script>

    <!--[if lt IE 9]>
        <script src="<?=SiteURL;?>/getResource.php?Type=js&File=PIE.min"></script>
        <script>

        $(function(){
            if (window.PIE){
                $('.pie').each(function(){
                    try{
                        PIE.attach(this);
                    }catch(e){}
                });

                $('input[type=text]').each(function(){
                    try{
                        PIE.attach(this);
                        $(this).css('position','relative');
                    }catch(e){}
                });
            }
        });

        </script>
    <![endif]-->

    <!--[if gte IE 9]>
      <style type="text/css">
        .gradient {
           filter: none;
        }
      </style>
    <![endif]-->

    <script>
    window.onload = function(){

        var preLoad = 	[
                                    '/img/icons.png',
                                    '/img/ajax-loader.gif',
                                    '/img/connectButtons.png'
                ];

        preLoad.forEach(function(item){
            $('<img/>',{'src':item});
        });
    };
    </script>
	
    <script type="text/javascript">
    
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-33291151-1']);
      _gaq.push(['_trackPageview']);
    
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    
    </script>

    <?php
    /*if ( $_SERVER["REMOTE_ADDR"] == "213.57.253.169" ) {
    ?>
    <script>
        $(function(){

            $(window).resize(function(){
                $(".width").html("width: " + $(this).width() + "  height : " + $(this).height());
            });

        });
    </script>
    <?php
    }*/
    ?>
</head>

<body>
<div class="width"></div>
<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
        f[z]=function(){
            (a.s=a.s||[]).push(arguments)};var a=f[z]._={
        },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
            f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
            0:+new Date};a.P=function(u){
            a.p[u]=new Date-a.p[0]};function s(){
            a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
            hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
            return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
            b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
            b.contentWindow[g].open()}catch(w){
            c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
            var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
            b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
        loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('9605-642-10-8900');/*]]>*/</script><noscript><a href="https://www.olark.com/site/9605-642-10-8900/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->
	<div id="fb-root"></div>
	<script>

		(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id; js.async = true;
			  js.src = "//connect.facebook.net/he_IL/all.js#xfbml=1";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
	</script>

	<div id="overlay"></div>


	<div id="tooltipoverlay"></div>
	<div id="Tooltip"></div>

	<div class="errorBar">
		<div class="errorClose"></div> <span class="error-bar-text">שגיאה במילוי טופס</span>
	</div>
	
	<div id="loading">
		<img src="<?=SiteURL?>/img/238.gif" width="220" height="28">
	</div>
	
	<?php
	if (!strpos($_SERVER['PHP_SELF'],'index.php')){
	?>
	<div id="header_menu">

		<div class="user-login-status">
			<div class="header-logo"></div>
            <div class="user-login-status-text">
            <?php
            if (!isset($_SESSION['USER_ID'])){
                    echo 'ברוך הבא אורח,<span onclick="Tooltip(\'login\');" class="pointer"> <u>התחבר כאן</u></span>';
                  }else{
                    echo "ברוך הבא ".$_SESSION['CustomerName'].',&nbsp;';
                    echo '<a style="cursor:pointer;" onclick="window.location.href=\''.SiteURL.'/logout.php\';"><u>התנתק</u></a>';
                  }
            ?>
            </div>
		</div>
	</div>
	<?php
	}
	?>
        
        <?php
        if ( $_SERVER["PHP_SELF"] == "/index.php" )
        {
        ?>
        <div class="fb_like_wrapper" style="width:980px; margin:0 auto; text-align:right; position:relative; z-index:200; top:1em; display:none;">
        <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.ds160.co.il%2F&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;font=arial&amp;colorscheme=light&amp;action=like&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
        </div>
        <?php
        }
        ?>
        
	<div id="container">
