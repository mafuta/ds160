<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html dir="rtl" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="he-IL">
<head>
	<meta name="robots" content="noindex, nofollow" />

	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />

	<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
	
	<script type="text/javascript" src="<?php echo SiteURL;?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo SiteURL;?>/js/jquery.placeholder.js"></script>

	<script type="text/javascript" src="<?php echo SiteURL;?>/js/ds160.js?Ver=<?=rand(1,100);?>"></script>
	<script type="text/javascript" src="<?php echo SiteURL;?>/js/main.js?Ver=<?=rand(1,100);?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo SiteURL;?>/css/main.css?Ver=<?=rand(1,100);?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo SiteURL;?>/css/main.css?Ver=<?=rand(1,100);?>" />

	<title><?php echo $base['SITE_TITLE'];?></title>

</head>
<body <?=(strpos($_SERVER['PHP_SELF'], "404.php") ? 'class="page-404" style="position:absolute;"' : '');?>>
	<div id="tooltipoverlay" onclick="return TooltipClose();"></div>
	<div id="Tooltip"></div>