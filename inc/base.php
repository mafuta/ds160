<?php
error_reporting(0);
ini_set('display_errors', 0);

define("DEFAULT_TIME_ZONE", "Asia/Tel_Aviv");
date_default_timezone_set(DEFAULT_TIME_ZONE);

function getErrorType($errorNumber = 0) {
    if ( empty($errorNumber) ) { return false; }

    switch ($errorNumber) {
        case E_ERROR:               return "E_ERROR";             break;
        case E_WARNING:             return "E_WARNING";           break;
        case E_PARSE:               return "E_PARSE";             break;
        case E_NOTICE:              return "E_NOTICE";            break;
        case E_CORE_ERROR:          return "E_CORE_ERROR";        break;
        case E_CORE_WARNING:        return "E_CORE_WARNING";      break;
        case E_COMPILE_ERROR:       return "E_COMPILE_ERROR";     break;
        case E_COMPILE_WARNING:     return "E_COMPILE_WARNING";   break;
        case E_USER_ERROR:          return "E_USER_ERROR";        break;
        case E_USER_WARNING:        return "E_USER_WARNING";      break;
        case E_USER_NOTICE:         return "E_USER_NOTICE";       break;
        case E_STRICT:              return "E_STRICT";            break;
        case E_RECOVERABLE_ERROR:   return "E_RECOVERABLE_ERROR"; break;
        case E_DEPRECATED:          return "E_DEPRECATED";        break;
        case E_USER_DEPRECATED:     return "E_USER_DEPRECATED";   break;
        default:                    return "Unknown";             break;
    }
}

set_error_handler(function( $errorNumber , $errorString, $errorFile, $errorLine ){
    $error = [
        "number"    => $errorNumber,
        "message"   => $errorString,
        "file"      => $errorFile,
        "line"      => $errorLine,
        "type"      => getErrorType($errorNumber)
    ];

    ksort($error);
    file_put_contents(__DIR__ . "/errorLog.log", json_encode($error) . "\n", FILE_APPEND);
});

register_shutdown_function(function() {
    $error = error_get_last();

    if ( empty($error) ) return false;
    $error["type"] = getErrorType($error["type"]);

    ksort($error);
    file_put_contents(__DIR__ . "/errorLog.log", json_encode($error) . "\n", FILE_APPEND);
});


if (extension_loaded("zlib") && (ini_get("output_handler") != "ob_gzhandler")) {
    ini_set("zlib.output_compression", 1);
}

/**
 * Autoloading classes when required
 */
spl_autoload_register ( function ($class_name) {
       
	if ( strpos($class_name, "Swift_")===0 ) {
		return;
	}
	   
	$lib_folders = [
        'lib',
        'lib/Compression',
        'ds-panel/lib',
	];
	
	$path = '';
	foreach ( $lib_folders as $folder ) {
		
		$file = $_SERVER ['DOCUMENT_ROOT'] . '/' . $folder . '/' . $class_name . '.php';
		
		if (file_exists ( $file )) {
			$path = $file;
			break;
		}
	}
	
	if (empty ( $path )) {
		die ( 'Fatal error: could not find class: ' . $class_name );
	} else {
		require_once ($file);
	}
} );

if (isset ( $_GET['ShowErrors'] ) || (isset( $ShowErrors ) && $ShowErrors == 1)) {
    ini_set ( 'display_errors', 1 );
    error_reporting ( E_ALL );
}

define ( '__ROOT__', $_SERVER ['DOCUMENT_ROOT'] ); 

define('SiteURL', system::getProtocol().$_SERVER['SERVER_NAME']);

require_once (__ROOT__ . '/mailer/swift_required.php');

if ( session_status() == PHP_SESSION_NONE ) {
    session_start ();
}




$salt = 'This is something to make the password retrieval harder';

$base = [
    "SITE_TITLE" 	=> 'ds160 ויזה לארה"ב',
    "SYSTEM_MAIL" 	=> 'hafuta@ds160.co.il',
    "SERVICE_MAIL" 	=> 'service@ds160.co.il',
    "SERVICE_PASS"  => 'jvisaco@ds160',
    "SERVICE_PRICE" => "155"
];

$time = mktime(23, 59, 00, 02, 20, 2014);
if ( date("Y-m-d H:i:s", time()) < date("Y-m-d H:i:s", $time) ) {
    $base["SERVICE_PRICE"] = 84;
}

// Makes sure the user is on secured protocol if neccesary


$system = new system();

if ( strpos($_SERVER["PHP_SELF"], "404.php") === false ) {
    $system->checkBlocked();
}

$qaSecured = false;
if ( $_SERVER["SERVER_ADDR"] == "127.0.0.1" || $_SERVER["SERVER_ADDR"] == "192.168.1.100" ) {
    if ( $qaSecured == true && isset($secured) ) {
        $system->checkSecured($secured);        
    }
} elseif ( isset($secured) ) {
    $system->checkSecured($secured);    
}


if ( $system->checkConnected ()) {
    if (! $system->validateUser ()) {
            session_destroy ();
            header ( 'location:registration.php' );
            exit;
    }

    if (strpos ( $_SERVER ['PHP_SELF'], 'registration.php' )) {
            header ( 'location:ds160.php' );
            exit;
    }
}