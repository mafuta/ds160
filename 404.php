<?php
header ( "HTTP/1.0 404 Not Found" );
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/inc/base.php');
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/inc/cleanHeader.php');
?>
<div class="dsbox" style="display: block; width: 600px; height: 100px !important; position: relative; margin-top: 10em; margin-left: auto; margin-right: auto; padding: 2em 2em 0 2em; min-height:250px !important; font-size: 16px; font-weight: bold;">
	<p>
		זה נראה שאתה מנסה למצוא משהו שלא קיים, ייתכן ונכנסת דרך קישור ישן או
		שקרתה תקלה. במידה והינך מעוניין להגיע לעמוד הראשי <a href="https://www.ds160.co.il" style="color:#000; text-decoration: underline;">לחץ כאן</a>
	</p>

	<p style="margin-top: 1.5em;">
		במידה והינך חושב שזוהי תקלה נשמח אם תוכל לעדכן אותנו בכתובת הדוא"ל
		הבאה: <a class="mail-address" style="font-size: 16px !important;"
			href="mailto:service@ds160.co.il">service@ds160.co.il</a>
	</p>

	<p style="margin-top: 1.5em;">
		אנו מתנצלים על התקלה,<br />צוות ds160.
	</p>

</div>

<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . '/inc/cleanFooter.php' );