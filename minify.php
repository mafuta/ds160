<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/inc/base.php");

$ServerRoot = $_SERVER['DOCUMENT_ROOT'];

$JavaScript = array(
	'addSupport.js',
	'jquery.min.js',
	'jquery.placeholder.js',
	'ds160.js',
	'main.js'
);

$JSoutput = '';
foreach($JavaScript as $file){
	if (strpos($file,'min')){
		$JSoutput .= "\r\n".file_get_contents($ServerRoot.'/js/'.$file);
	}else{
		$JSoutput .= "\r\n".JShrink::minify(file_get_contents($ServerRoot.'/js/'.$file),array('flaggedComments' => false));
	}
}
file_put_contents($ServerRoot.'/js/ds160.min.js',$JSoutput,LOCK_EX);
echo 'ds160.min.js updated';


//PIE
file_put_contents($ServerRoot.'/js/PIE.min.js',file_get_contents($ServerRoot.'/js/PIE.js'));


echo nl2br("\r\n");



$CSS = array(

	'main.css'

);

$CSSoutput = '';
foreach($CSS as $file){
	if (strpos($file,'min')){
		$CSSoutput .= "\r\n".file_get_contents($ServerRoot.'/css/'.$file);
	}else{
		$CSSoutput .= "\r\n".CssMin::minify(file_get_contents($ServerRoot.'/css/'.$file));
	}
}
file_put_contents($ServerRoot.'/css/ds160.min.css',$CSSoutput,LOCK_EX);
echo 'ds160.min.css updated';
