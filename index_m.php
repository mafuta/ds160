<?php
require_once( $_SERVER["DOCUMENT_ROOT"] . "/inc/base.php" );

$mobileDetect = new Mobile_Detect();
if ( !$mobileDetect->isMobile() || $mobileDetect->isTablet() )
{
	header("location:index.php");
	exit;
}
?>
<!DOCTYPE html>
<html dir="rtl">
<head>
	<meta charset="utf-8">
	<title>ds160 - ויזה לארצות הברית</title>
	
	<meta name="keywords" content="ארצות הברית, ויזה, אשרה, תייר, ארהב, ds160, שגרירות, אמריקה" />
	<meta name="title" content="הוצאת ויזה לארצות הברית">
	<meta name="description" content="ds160 - טיפול בבקשה לויזה לארצות הברית">
	<meta property="og:title" content="הוצאת ויזה לארה״ב בקלות!">
	<meta property="og:description" content="תאום פגישה בשגרירות ארה״ב במהירות! מילוי טופס הבקשה לויזה בעברית! יעוץ הדרכה וליווי צמוד עד ליום הראיון בשגרירות!">
	<meta property="og:type" content="company" />
	<meta property="og:url" content="https://www.ds160.co.il">
	<meta property="og:image" content="http://www.ds160.co.il/img/fb_share.jpg">
	<meta property="og:site_name" content="&#x5d2;&#039;&#x5d5;&#x5e0;&#x5ea;&#x5df; &#x5d5;&#x5d9;&#x5d6;&#x5d4;">
	<meta property="fb:admins" content="1059865675,696819251">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" type="text/css" href="<?=SiteURL;?>/getResource.php?Type=css&File=mobile&ver=3">
	<script type="text/javascript" src="<?=SiteURL;?>/getResource.php?Type=js&File=jquery.min"></script>

	<script type="text/javascript">
    
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-33291151-1']);
      _gaq.push(['_trackPageview']);
    
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    
    </script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
	<div class="mobile-wrapper">
		<div class="logo"></div>
		<div class="title">שירות להוצאת ויזת תיירות ועסקים לארצות הברית</div>
		
		<ul class="intro-note">
	        <li>מילוי טופס הבקשה לויזה לארה"ב</li>
	        <li>תאום מועד ראיון בשגרירות ארה"ב</li>
	        <li>יעוץ והכוונה ליום הראיון בשגרירות ארה"ב </li>
	    </ul>

	    <div class="contact-text">השאר פרטיך ואחד המומחים שלנו יצור איתך קשר בהקדם לייעוץ חינם!</div>
		<div class="contact">
			<div class="contact-answer">הודעה בשליחה, אנא המתן...<br><br><img src="/img/ajax_loader_gray_32.gif"></div>
			<form id="contactf" name="contactf">
				<input type="hidden" name="contact_ip" id="contact_ip" value="<?=system::getUserIP();?>" />
				
				<div class="field">
					<label for="contact_name">שם מלא</label>
		      		<input type="text" id="contact_name" name="contact_name" placeholder="הקלד שם מלא" />
		      	</div>

				<div class="field">
					<label for="contact_email">דוא"ל</label>
			     	<input type="email" id="contact_email" name="contact_email" placeholder="הקלד כתובת דואר אלקטרוני" class="ltr-field" />
		     	</div>

				<div class="field">
					<label for="contact_phone">טלפון</label>
			      	<input type="tel" id="contact_phone" name="contact_phone" placeholder="הקלד טלפון ללא סימני הפרדה" class="ltr-field" />
		     	</div>

				<div class="field">
					<label for="subject">נושא</label>
			      	<input type="text" id="contact_subject" name="contact_subject" placeholder="הקלד נושא" />
				</div>

				<div class="field">
					<textarea name="contact_message" id="contact_message" placeholder="הקלד כאן הודעתך"></textarea>
				</div>

				<div class="field" style="text-align:center;">
					<button id="contact_send">שליחת הודעה</button>
				</div>
			</form>
		</div>

	    <div class="fb-like-wrapper">
	    	<div class="fb-like" data-href="http://www.ds160.co.il" data-send="false" data-width="250" data-show-faces="false" data-font="verdana"></div>
	    </div>

	    <div style="width:100%; height:5px; clear:both;"></div>

	    <div class="copyrights">
	    	&copy; All rights reserved, ds160.co.il <?=date("Y");?>&nbsp;|&nbsp; Site built by <a href="mailto:matan@hafuta.com" class="footer-matan">Matan Hafuta</a>
		</div>

	</div>
	

	<script type="text/javascript" src="<?=SiteURL;?>/getResource.php?Type=js&File=mobile"></script>

    <!-- Google Code for mobile lead Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 984070199;
        var google_conversion_language = "en";
        var google_conversion_format = "2";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "-ousCLGAxgYQt_Ce1QM";
        var google_conversion_value = 0;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/984070199/?value=0&amp;label=-ousCLGAxgYQt_Ce1QM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>

</body>
</html>