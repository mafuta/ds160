<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/header.php');
?>

<style type="text/css">
.ds-loader{
	z-index: 9999;
	margin: 0 auto;
	right: 0;
	left: 0;
	display: none;
	opacity: 0.4;
	filter: alpha(opacity=40);
	background: #000;
	position: fixed;
	height: 100%;
	width: 100%;
}

.loader-container{
	position: absolute;
	top: 50%;
	width: 100%;
	height: 30px;
	text-align: center;
}

.ds-loader-gif{
	border: none;
}
</style>
<div class="ds-loader">
	<div class="loader-container">
		<img class="ds-loader-gif" src="/img/loader.gif">
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('form#registration input').each(function(){
		$(this).prop('autocomplete','off');
	});
});
</script>

<div class="progressBar step1"></div>  
  <div id="rgs" class="dsbox" style="width:950px; margin-top:1em;">
    <div class="dsbox_left" style="width:220px; text-align:justify;">
      <div style="font-weight:bold; font-size:15px; margin-bottom:1em;">לקוח יקר!</div>

השלמת השאלון המקוון הינה השלב הראשוני בטיפול בבקשתך לוויזה לארה"ב.
<br /><br />
לאחר השלמת השאלון תופנה לעמוד תשלום, לאחר קבלת אישור התשלום נציגנו יצרו עמך קשר
להשלמת פרטים חסרים ותאום מועד ראיונך בשגרירות.
<br />
<span style="margin-top:8px; display:inline-block;"><b>חשוב לדעת!</b></span>
<br />
<style type="text/css">
	.registration-left-list li{
		margin-bottom:4px;
	}
	
	.registration-left-list li:first-child{
		margin-top:4px;
	}
</style>

<ul class="registration-left-list" style="list-style-type:none;">
    <li>יש למלא שאלון עבור כל מגיש בקשה בנפרד.</li>
    <li>בעת מילוי השאלון יש להימנע ממתן מידע כוזב.</li>
    <li>שדות המסומנים ב<span style="color:red; font-weight:bold;">(*)</span> הינם שדות חובה.</li>
    <li>ניתן לדלג על שאלות שאינכם יודעים את התשובות עליהן.</li>
</ul>
<br />

<style type="text/css">
.registration-lock{
	display: inline-block;
	width: 14px;
	height: 21px;
	margin-top: -10px;
	background: url('/img/icons.png');
	background-position: 0 -121px;
}
</style>

<span class="registration-lock"></span>
לידיעתך, כל העמודים באתר מוצפנים ומוגנים תחת פרוטוקול SSL למען שמירה על פרטיותך.

    </div>
    <div class="dsbox_right" style="width:670px; height:442px;">
    	<div id="register_form" style="padding:20px;">
			<form method="post" name="registration" id="registration" onsubmit="return false;">
				<div class="rgs_field"> 
					<label for="app_name">שם מלא בעברית</label>
					<input type="text" id="app_name" name="app_name" placeholder="כפי שמופיע בדרכון" />
				</div>          
				<div class="rgs_field" style="margin-right:4.5em;">
					<label for="email">אימייל</label>
					<input type="text" id="email" name="email" placeholder="הזן כתובת מייל תקינה אליה ישלחו המסמכים"  />
				</div>
				<div class="rgs_field">
					<label for="username">בחר שם משתמש</label>
					<input type="text" id="username" name="username" title="שם משתמש כבר תפוס" onchange="checkUsername();" maxlength="10" placeholder="אותיות אנגליות בלבד ומספרים, ללא רווחים" />
				</div>
				<div class="rgs_field" style="margin-right:4.5em;">
					<label  for="pass">בחר סיסמה</label>
					<input type="password" id="pass" name="pass" placeholder="לפחות 5 תווים" />
				</div>
				<div class="rgs_field">
					<label  for="pass">טלפון</label>
					<input type="text" id="phone" name="phone" maxlength="10" onkeypress="return isNumberKey(event);" placeholder="על מנת שנוכל ליצור עמך קשר בסיום התהליך" />
				</div>      
                            
                <div class="agree-terms-line">

                    <script>
                        $(document).ready(function(){
                            $("[checkboxToggle]").on("click", function(){
                                var checkboxId = $(this).attr("for");

                                if ( $("#"+checkboxId+"[type='checkbox']").length > 0 )
                                {
                                    var inputBoxx = $("#"+checkboxId);
                                    //inputBoxx.click();
                                    inputBoxx.prop("checked", !inputBoxx.prop("checked"));
                                }
                                
                                $(this).toggleClass('check');

                            });                                            
                        });

                    </script>
					
                    <span class="takanon-red-text">סמן כאן</span>
                    
                    <span checkboxToggle="true" for="agreeTerms" class="checkboxLabel"></span>אני מאשר\ת כי קראתי והבנתי את תקנון האתר
                    
                    <script>
                    
                    $(document).ready(function(){
                       $(".checkboxLabel[for='agreeTerms']").on("click", function(){
                              
                              if ( $("#agreeTerms").is(":checked") && $(".takanon-red-text").is(":visible") )
                              {
                                  $(".takanon-red-text").fadeOut('quick');
                              }

                       });
                       
                    });
                    </script>
					
                    <input type="checkbox" id="agreeTerms" name="agreeTerms" value="1" />

                </div>
                            
				<div class="separator-line"></div>
				<button name="register_button" id="register_button" class="ds_button_next pointer" style="position: absolute;bottom: 1em;left: 25em;"></button>
			</form>
      </div>
    </div>
  </div>
<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php');
?>
