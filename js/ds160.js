var $dsLoader = $(".ds-loader");
var ds160 = {

	UI: {
	
		Loader: {
		
			show: function(){
				if ( $dsLoader.length > 0 )
					$dsLoader.fadeIn('quick');
			},
			
			hide: function(){
				if ( $dsLoader.length > 0 && $dsLoader.is(":visible") )
					$dsLoader.fadeOut('quick');
			}
		
		}
	
	}

};

/** Adwards conversion code **/
var google_conversion_id 		= 984070199;
var google_conversion_language 	= "en";
var google_conversion_format 	= "2";
var google_conversion_color 	= "ffffff";
var google_conversion_label 	= "";
var google_conversion_value 	= 0;

var conversionLabels = {
  "contact": "iJ3FCLmO-wUQt_Ce1QM",
  "finish": "QY87CLGP-wUQt_Ce1QM"
};

function trackConversion(label) {
  google_conversion_label = conversionLabels[label];

  document.write = function(text) {
    $("#content").append(text);
  };
  $.getScript("//www.googleadservices.com/pagead/conversion.js");
}
/** Conversion code **/

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode;

    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

function trim(stringToTrim)
{
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function checkUsername()
{
	var username = $("#username");
	var UsernameRegExp = /^([\w]{1}[\w\d]{5,9})$/;

	if ( username.val()=="" || username.val()==null || !UsernameRegExp.test( username.val() ) )
	{
		username.css({"background":"url('/img/usernameWrong.png') no-repeat scroll", "background-position":"7px 12px"});
		username.css("background-color","white");
		username.addClass('hilight');
		username.focus();
	}
	else
	{
		var user = username.val();
		
		$.ajax({
			type: 'GET',
			url: "./ajax/username.php?user="+escape(user),
			success: function(response){
						
					if (response=='true')
					{
						//Username is already taken
						username.css({"background":"url('/img/usernameWrong.png') no-repeat scroll", "background-position":"7px 12px"});
						username.css("background-color","white");
						username.addClass('hilight');
						username.focus();
					}
					else
					{
						//Username is available for registration
						username.css({"background":"url('/img/usernameGood.png') no-repeat scroll", "background-position":"6px 15px"});
						username.css("background-color","white");
						username.removeClass('hilight');
					}

			}
		});
	}
}

function makeLogin()
{  
  var loginError = false;
  
  $("#loginform :input[type=text], #loginform :input[type=password]").each(function(){
    if ($(this).is('.hilight'))
    {
      $(this).removeClass('hilight');
    }

    if(!$(this).val() || $(this).val().length <3)
    {
      $(this).addClass('hilight');
      loginError = true;
    }
  });

  if (!loginError)
  {
    var $loginAns = $("#loginans");

    $loginAns.fadeOut('slow');
    $loginAns.html('הפרטים נבדקים, אנא המתן...');
    $loginAns.fadeIn('quick');
  
    //Another way to send the data
    //var data = {login_user: $('#login_user').val(), login_pass: $('#login_pass').val()};
    var data = $("#loginform").serialize();
    $.post("./inc/login.php",data,function(data)
    {

      //Removes text color
      if ($loginAns.is('.redtext'))
      {
        $loginAns.removeClass('redtext');
      }
      if ($loginAns.is('.greentext'))
      {
        $loginAns.removeClass('greentext');
      }
     
      if (data=="0")
      {
        $loginAns.addClass('redtext');
        $loginAns.html('שם משתמש או סיסמה אינם נכונים');
      }
      else if (data=="1")
      {
        $loginAns.addClass('greentext');
    
        $loginAns.html('התחברת בהצלחה, אנא המתן...');
       
        $('.password-remainder').slideUp('quick', function(){
        	$("#loginform").fadeOut('slow');
        });
        
        setTimeout(function()
        {
          window.location.replace("Returning.php");
        }, 2000);
      }
      else
      {
        $loginAns.html(data);
      }
    });
  }

  return false;
}

function resetPassword(email)
{
  if (email==''){return false;}

    var $ajaxLoader = $("#ajax-loader");

  $.ajax({
  	type: 'post',
  	url: './ajax/reset_password.php',
  	data: {'email' : email},
	beforeSend: function()
	{
		$("#reminder_form").css('display','none');
		$ajaxLoader.css('display','block');
	},
  	success: function(response)
  	{
  		var response_field = $('#reset-loginans');
		response_field.css({'color':'red','font-size':'13px'});

		if (response=='okay')
		{
			$ajaxLoader.fadeOut('quick');
			response_field.css({'color':'green','font-size':'13px'});
			setTimeout(function(){response_field.html('הוראות לאיפוס סיסמה נשלחו לכתובת הדוא"ל שהוזנה.');},400);
			
			return;
		}
		else if(response=='error1')
		{
			response_field.html('כתובת הדוא"ל שהוזנה אינה תקנית. אנא בדוק את הכתובת ונסה שנית.');
		}
		else if(response=='error2')
		{
			response_field.html('כתובת הדוא"ל שהוזנה איננה קיימת במערכת, אנא נסה שנית.');
		}
		else if(response=='error3')
		{
			response_field.html('עקב תקלה טכנית לא ניתן לאפס את הסיסמה. אנא צור קשר עם שירות הלקוחות. מצטערים על אי הנוחות');
		}

		$ajaxLoader.css('display','none');
		$("#reminder_form").css('display','block');
		
		
    }
  });
  
  return false;
}

function Tooltip(data)
{
	var tooltip = $("#Tooltip");
	
	var file    = '';

	if (data=='login')
	{
		file = 'login.php';
	}
	else if(data=='terms')
	{
		file = 'terms.php';
	}
	else if(data=='contact')
	{
		file = 'contact.php';
	}
	else if(data=='HowItWorks')
	{
		file = 'howItWorks.php';
	}else if(data=='price')
    {
        file = 'price.php';
    }else if(data=='coupon84terms')
    {
        file = 'terms_coupon_84.php';
    }

	if (file!='') {

        $.ajax({
            type: 'post',
            url: '/tooltipdata/'+file,
            success: function(response){
                tooltip.html(response);

                //sWidth is the tooltip Content's width
                var sWidth = tooltip.children().outerWidth();
                tooltip.css('width', sWidth);

                //Appending a close sign
                tooltip.append('<span class="tooltip_close" onclick="return TooltipClose();" style="z-index:500000; display:inline;"></span>');


                //{IE hack} Setting the opacity using JS instead of CSS in order to avoid 100% opacity
                $("#tooltipoverlay").css({'filter':'alpha(opacity=80)','opacity':'0.8'}).fadeIn('quick');

                setTimeout(function(){
                    var sTop 	= $(window).height()/2 - tooltip.height()/2;
                    var sLeft 	= $(window).width()/2 - tooltip.width()/2;
                    tooltip.css({'top' : sTop, 'left' : sLeft, 'position':'fixed'}).fadeIn('quick');

                },300);


            }
        });

    }

}

function TooltipClose()
{
	//Fading out and emptying the tooltip container
  $("#Tooltip").fadeOut("fast").html('');
  
  //Fading out the background layer after 0.4seconds
  setTimeout(function(){
    $("#tooltipoverlay").fadeOut('quick');
  },400);
}

$(window).resize(function()
{
    var $tooltip = $("#Tooltip");
    if ( $tooltip.length > 0 ) {
        if ( $tooltip.is(":visible") )
        {
            $tooltip.css("top",  ($(window).height() / 2) -  ($tooltip.height() / 2) );
            $tooltip.css("left", ($(window).width() / 2) -  ($tooltip.width() / 2));
        }
    }

});


$(document).ready(function(){

	$(document).on("keyup blur", "input[numbersonly]" ,function(){ 
	   $(this).val( $(this).val().replace(/[^0-9]/g,'') ); }
	);

	$(document).on("change", ".hilight:input" ,function(){
	
		$(this).removeClass("hilight");
	
	});

	$(document).on('click', '#startbutton', function(){
		window.location.href = "registration.php"; //Acts like link, not as http redirect
	});
	
	$(document).on('click', '#contact_send', function(){
		
		var contactErrors = false;
		var contactf = $('#contactf');
		contactf.fadeOut('quick');
		
		var contactAnswer = $('.contact-answer');
		contactAnswer.html('הודעה בשליחה, אנא המתן<br />').append('<img src="/img/smallLoading.gif" />').fadeIn('quick');

		$("form#contactf :input[type=text][name!=contact_phone], #message").each(function(){
	  		if ( $(this).is('.hilight') )
		  	{
				$(this).removeClass('hilight');
		  	}
	  
			if( !$(this).val() )
			{
				$(this).addClass('hilight');
				contactErrors = true;
			}
		});
		
		var email = $("#contact_email");

		// "\w" in regular expression includes A-Z a-z 0-9 and "_" (underscore)
		var emailReg = /^([0-9a-zA-Z]([\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,4})$/;
		if( !emailReg.test(email.val()) || email.val() == '' )
		{
			 $(email).addClass('hilight');
			 contactErrors = true;
		}

		//If errors shake page
		if (contactErrors)
		{
			contactAnswer.html('שגיאה בשליחת הודעה!');
			
			setTimeout(function(){
				contactAnswer.fadeOut('quick');
				contactf.fadeIn('quick');
			},1500);
		}
		else
		{
			var data = $("#contactf").serialize();

		  	$.ajax({
			  	type: 'POST',
			  	data: data+'&contact_send=send',
			  	url:'/ajax/sendMessage.php',
		  		success: function(response)
		  		{
					  	if (response=="okay")
					  	{
					  		var hideInterval = 8;
					  		
			  				if ( contactAnswer.is(':visible') )
			  				{
			  					contactAnswer.fadeOut('quick');
			  				}
					  		
					  		$("#contactf").slideUp('quick').html('').remove();
					  		
					  		var answer = $('<div/>', { 'id':'contact-thanks', 'class':'contact-thanks' })
					  		.append('<div class="line1">הודעתך התקבלה בהצלחה!</div>')
					  		.append('<div class="line2">נציגינו יחזרו אליך בהקדם האפשרי</div>')
					  		.append('<div class="line3">חלון זה יסגר בעוד <span class="contact-close-counter">'+hideInterval+'</span> שניות</div>');

					  		$(".contact-form").append(answer);
					  		$("#contact-thanks").fadeIn('quick').css('display','block');

                            var $contactCloseCounter = $('.contact-close-counter');
					  		window.contactInterval = setInterval(function(){
                                $contactCloseCounter.html( parseInt($contactCloseCounter.html(), 10)-1 );
					  		},1000);
					  		
					  		setTimeout(function(){
					  			TooltipClose();
					  			window.clearInterval(contactInterval);
					  		}, hideInterval*1000);
					  	}
					  	else if( response.indexOf('email') >= 0 )
					  	{
					  		contactAnswer.html('שגיאה בשליחת הודעה!');
					  		
							setTimeout(function(){
								contactAnswer.fadeOut('quick');
								contactf.fadeIn('quick');
							},1500);
							
							$('#contact_email').addClass('hilight');
					  	}
					  	else if( response.indexOf('error') >= 0 )
					  	{
							contactAnswer.html('שגיאה בשליחת הודעה!');
							
							setTimeout(function(){
								contactAnswer.fadeOut('quick');
								contactf.fadeIn('quick');
							},1500);
					  	}

		  		}

		  });
		}
		return false;
	});


	$(document).on('click', '#register_button', function(){

		var errors = false;

		//Checking for empty fields
		$('form#registration :input[type=text], form#registration :input[type=password]').each(function(){
			if ( $(this).is('.hilight') )
			{
				$(this).removeClass("hilight");
			}
		  
			if( $(this).val() == "" || $(this).val() == null )
			{
				$(this).addClass("hilight");
				if ( $(this).attr("id") != "phone" )
				{
					errors = true;
				}				
				
			}else{
				$(this).val( trim( $(this).val() ) );
			}
		});

		var errorBarText = "",
            errorCounter = 0,
            email = $("#registration").find(":input[name='email']"),
            emailReg = /^([0-9a-zA-Z]([\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,4})$/,
            $pass = $("#pass");

		if( !emailReg.test(email.val()) || email.val() == '' )
		{
			 $(email).addClass('hilight');
			 errorBarText = "כתובת דואר אלקטרוני אינה תקינה";
			 errors = true;
			 errorCounter++;
		}

		if ( $pass.val().length<5 )
		{
            $pass.addClass('hilight');
			errorBarText = "נא הזמן סיסמה באורך של לפחות 5 תווים";
			errors = true;
			errorCounter++;
		}
		
		var NameReg = /^[א-ת]{1}[א-ת\s']+$/,
            $appName = $("#app_name");

		var appName = trim( $appName.val() );
		if ( !NameReg.test( appName ) ) {
            $appName.addClass('hilight');
			errorBarText = "נא הזן שם מלא בעברית";
			errors = true;
			errorCounter++;
		}
		
		var $username = $("#username"),
            UsernameRegExp = /^([\w]{1}[\w\d]{5,9})$/;

		if ( !UsernameRegExp.test( $username.val() ) )
		{
			$username.addClass('hilight');
			errorBarText = "נא הזן שם משתמש באורך של לפחות 6 תווים, מספרים ואותיות באנגלית בלבד";
			errors = true;
			errorCounter++;
		}
		
		
		var takanon = $("#agreeTerms");
		
		
		if (errors)
		{
			if ( errorCounter == 1 )
			{
				$(".error-bar-text").html(errorBarText);
			}else{
				$(".error-bar-text").html("שגיאה במילוי טופס, נא מלא את כל השדות");
			}
			
			$(".errorBar").slideDown('quick', function(){
				setTimeout(function()
				{
					$(".errorBar").slideUp('quick');
				}, 7000);
			// Animation complete.
			});
		}
		else if ( !takanon.is(":checked") )
		{
			$(".error-bar-text").html("נא אשר הסכמתך לתקנון");
			
			$(".errorBar").slideDown('quick', function(){
				$(".takanon-red-text").fadeIn('quick');
				setTimeout(function()
				{
					$(".errorBar").slideUp('quick');
					$(".takanon-red-text").fadeOut('quick');
				}, 7000);
			// Animation complete.
			});
			return false;
		}
		else
		{
		
			var regdata = "app_name="+$appName.val()+"&email="+$("#email").val()+"&username="+$username.val()+"&pass="+$pass.val()+"&phone="+$("#phone").val();

			$.ajax({
				type: 'POST',
				url: '/ajax/register.php',
				data: regdata,
				beforeSend: function(){
					ds160.UI.Loader.show();
				},
				success: function(Response){

                    var $email = $("#email");

                    if (Response=="success")
                    {
                        window.location.href = 'ds160.php';
                    }
                    else if (Response=="invalid email")
                    {
                        alert('כתובת מייל לא תקינה');
                        $email.addClass('hilight');
                        $email.focus();
                    }
                    else if (Response=="email exist")
                    {
                        alert('המייל כבר קיים במערכת');
                        $email.addClass('hilight');
                        $email.focus();
                    }
                    else if (Response=="invalid username")
                    {
                        alert('שם משתמש לא תקין');
                        $username.addClass('hilight');
                        $username.focus();
                    }
                    else if (Response=="username exist")
                    {
                        alert('שם המשתמש קיים במערכת, אנא בחר שם משתמש אחר.');
                        $username.addClass('hilight');
                        $username.focus();
                    }

				}
			}).done(function(){
				ds160.UI.Loader.hide();
			});

			
		}
		return false;
	});
    
    
	$(document).on('click', '.errorClose' ,function(){
		$(".errorBar").fadeOut('slow');
	});
  

});





$(document).ready(function()
{
		
	var swidth 	= screen.width,
        sheight = screen.height,
        background = "";

	if (swidth>2560)
	{
		background = 'background3280_1600.jpg';
	}
	else if (swidth==2560 && sheight==1600)
	{
		background = 'background2560_1600.jpg';
	}
	else if (swidth==2048 && sheight==1536)
	{
		background = 'background2048_1536.jpg';
	}
	else if (swidth==1920 && sheight==1440)
	{
		background = 'background1920_1440.jpg';
	}
	else if (
		(swidth==1920 && sheight==1200) ||
		(swidth==1680 && sheight==1050) ||
		(swidth==1440 && sheight==900)	||
		(swidth==1280 && sheight==800)	||
		(swidth==1920 && sheight==1080) ||
		(swidth==1280 && sheight==720)
	)
	{
		background = 'background1920_1080.jpg';
	}
	else if (
		((swidth == 1360 || swidth==1366) && sheight==768) ||
		(swidth==1280 && sheight==768)
	){
		background = 'background1360_768.jpg';
	}
	else if(
		(swidth==1600 && sheight==1200) || 
		(swidth==1400 && sheight==1050) || 
		(swidth==1280 && sheight==1024) ||
		(swidth==1152 && sheight==864) 	|| 
		(swidth==1024 && sheight==768) 	|| 
		(swidth==1280 && sheight==960) 	|| 
		(swidth==800 && sheight==600)
	)
	{
		background = 'background1680_1200.jpg';
	}
	else
	{
		background = 'background1920_1080.jpg';
	}
	
	$("body").css('background-image',"url('/img/"+background+"')");
	
	$(document).on('mouseover','.footer-warning', function(){
		
		if ( !$('.ds-tip').is(':visible') )
		{
            var tooltip = $('<div/>',{'class' : 'ds-tip'});
            var warningPos = $('.footer-warning').offset();

            var tooltipText = $('<span/>');
            tooltipText.css({'font-weight':'bold', 'color':'#FFF'});

            tooltip.css({
                            position:'absolute',
                            bottom: '32px',
                            right: parseInt($(window).width()) - ( parseInt(warningPos.left) + 30 ),
                            top: parseInt(warningPos.top) - 38,
                            border: '2px solid #FFF',
                            background : '#000',
                            opacity: '0.8',
                            'border-radius': '10px',
                            width: 'auto',
                            height: '30px',
                            'line-height' : '30px',
                            padding: '0 10px',
                            display: 'none',
                            filter:'alpha(opacity=80)'
                        });



            tooltipText.html('האתר ds160.co.il אינו פועל בתיאום ו/או בשיתוף עם שגרירות ארה"ב ו/או מי מטעמה ו/או כל גוף ממלכתי ו/או רשמי אחר.');
            tooltip.append(tooltipText);

            $('body').append(tooltip);

            tooltip.fadeIn('quick');
            return true;
        }

        return false;
		
	}).on('mouseout', '.footer-warning', function(){
		
		$('.ds-tip').animate({'width':'0px', 'height':'0px'}, function(){
			$('.ds-tip').hide();
		});

	});
});

$(window).load(function(){

	$("#loading").fadeOut('quick',function() {
        var $fbLikeWrapper = $(".fb_like_wrapper");

        if ( $fbLikeWrapper.length > 0 )
        {
            $fbLikeWrapper.fadeIn('quick');
        }
        $("#container").fadeIn('slow');
	});

});
