function showlayer(layertoshow,layertohide,stage){
	$("#part"+layertohide).fadeOut('slow',function(){
		changeProgress(stage);
		$("#part"+layertoshow).fadeIn('slow', function(){
			//
		});	  	
	});
	return false;
}

function changeProgress(stage)
{
	$('.progressBar').prop('class','progressBar step'+stage);
}


function testEngLetters(src){
	var pattern = new RegExp(/^[\w]+[\w\s]{2,}$/gi);
	return pattern.test(src);
}

function CheckPhone(src){
	//Israeli phones validation
	
	//Maximum phone length including one dash is 11 digits
	if (src==null || src=='' || src.length>11){return false;}
	
	//Splitting by the dash seperator
	var arr = src.split("-"),
        phoneReg;

	if (arr.length>2){
		//More than one "-"
		return false;
	}else if (arr.length==1){
		//Empty phone string or no dash(-)
		phoneReg = /^([\d]{9,10})?$/;
	}else if (arr.length==2){
		//Exactly one "-"
		if (arr[0].length>=2 && arr[0].length<=4){
			//Valid prefix, getting the right regex pattern for the prefix
			if (arr[0].length==2){
				//prefix length == 2
				//example: 04-9513535
				//Must start with 0, one dash(-) and 7 following digits
				phoneReg = /^([0]{1}[2-9]{1}[-]{1}[6-9]{1}[0-9]{6})?$/;
			}else if (arr[0].length==3){
				//prefix length == 3
				//example 054-7844516
				//Must start with 0, one dash(-) and 7 following digits
				phoneReg = /^([0]{1}[2-9]{2}[-]{1}[\d]{7})?$/;
			}else{
				//prefix length == 4
				//Example: 1800-600600
				//Must start with 1, contain one dash(-) and 6 following digits
				phoneReg = /^([1]{1}[\d]{3}[-]{1}[\d]{6})?$/;
			}
		}
	}
	//Checking the phone against the regular expression
	return phoneReg.test(src);
}



$(document).ready(function(){

	$("#status").change(function(){
		var status = $("#status").find("option:selected").val(),
            $spouse0 = $("#spouse_0");

        $spouse0.slideUp('slow',function(){
			$("#spouse_1, #spouse_2").slideUp('quick');
		});
		setTimeout(function(){return false;}, 1000);
        $spouse0.find(":input[type=text]").each(function(){
			$(this).val('');
		});
		
		if (status == "אלמן"){
			$("#deceased").show();
		}else{
			$("#deceased").hide();
		}
		
		if (status == "גרוש"){
			$("#divorced").show();
		}else{
			$("#divorced").hide();
		}
		
		if (status != "רווק"){
            $spouse0.slideDown('quick',function(){
				if (status == "גרוש"){
						$("#spouse_2").slideDown('slow');	
				}
				if (status != "אלמן"){
						$("#spouse_1").slideDown('slow');	
				}
			});
		}else{
            $spouse0.slideUp('quick',function(){
				$("#spouse_2").slideUp('quick',function(){
					$("#spouse_1").slideUp();
				});		
			});
		}
	});
  
  
	$("#1next").click(function(){

        $("#ds160frm").find("input, textarea").each(function(){

            $(this).val( $(this).val().trim() );

        });

		var name 	= $("#app_name");
		var fname 	= $("#app_fname");
		var app_id 	= $("#app_id");
		var app_countryob = $("#app_countryob");
		var passport = $("#passport");
		var passport_city = $("#passport_city");
		var passport_dateissued = $("#passport_dateissued");
		var passport_dateexpire = $("#passport_dateexpire");
		var cellular = $("#cellular");
		var phone = $("#phone");
		var errors 	=	false;
		//var errorList;
		
		if (name.val()==null || name.val()=='' || name.val().length<2 || !testEngLetters(name.val())){
			errors = true;
			name.addClass('hilight');
		}else {
			name.removeClass('hilight');	
		}

		if (fname.val()==null || fname.val()=='' || fname.val().length<2 || !testEngLetters(fname.val())){
			errors = true;
			fname.addClass('hilight');
		}else{
			fname.removeClass('hilight');
		}



		if (app_id.val()==null || app_id.val()=='' || !/\d/gi.test(app_id.val()) || app_id.val().length!=9){
			errors = true;
			app_id.addClass('hilight');
		}else{
			app_id.removeClass('hilight');
		}

		if (app_countryob.val()==null || app_countryob.val()=='' || app_countryob.val().length<2){
			errors = true;
			app_countryob.addClass('hilight');
		}else{
			app_countryob.removeClass('hilight');
		}
		
		if (passport.val()==null || passport.val()=='' || passport.val().length<5){
			errors = true;
			passport.addClass('hilight');
		}else{
			passport.removeClass('hilight');
		}	

		if (passport_city.val()==null || passport_city.val()=='' || passport_city.val().length<2){
			errors = true;
			passport_city.addClass('hilight');
		}else{
			passport_city.removeClass('hilight');
		}

		
		if (passport_dateissued.val()==null || passport_dateissued.val()=='' || passport_dateissued.val().length<6){
			errors = true;
			passport_dateissued.addClass('hilight');
		}else{
			passport_dateissued.removeClass('hilight');
		}

		
		if (passport_dateexpire.val()==null || passport_dateexpire.val()=='' || passport_dateexpire.val().length<6){
			errors = true;
			passport_dateexpire.addClass('hilight');
		}else{
			passport_dateexpire.removeClass('hilight');
		}		

		if (!CheckPhone(cellular.val())){
			errors = true;
			cellular.addClass('hilight');
		}else{
			cellular.removeClass('hilight');
		}
		
		if (phone.val()!=null && phone.val()!=''){
			if (!CheckPhone(phone.val())){
				errors = true;
				phone.addClass('hilight');
			}else{
				phone.removeClass('hilight');
			}
		}
		
		if (!errors)
		{
			//Layer to show | Layer to hide | stage to show
			return showlayer(2,1,3);
		}
		
		return false;
	});
  
	$("#2previous").click(function(){
		return showlayer(1,2,2);
	});
	
	$("#2next").click(function(){
		return showlayer(3,2,4);
	});
  
	$("#3previous").click(function(){
		return showlayer(2,3,3);
	});
	
	$("#3next").click(function(){
		
	});

  
	$("select#sponser").change(function(){
		if ($(this).val() == "אדם אחר" || $(this).val() == "ארגון"){
			$("#sponser_details").slideDown('slow',function(){
				//
			});
		}else{
			$("#sponser_details").slideUp('quick',function(){
				//
			});
		}
	});
	//can also write input[name=passport_lost]:radio - but not recommended for modern browsers

	//Passport lost
  $('input[name="passport_lost"][type="radio"]').change(function(){
  	if ($(this).val() == 'true'){
  		$("div#lost_passport").slideDown('slow',function(){
  			//
  		});
  	}else{
  		$("div#lost_passport").slideUp('quick',function(){
  			//
  		});
		}
  });
  
  //Traveling with other people
  $('input[name="other_people"][type="radio"]').change(function(){
  	if ($(this).val() == 'true'){
  		$("div#travelmates").slideDown('slow',function(){
  			//
  		});
  	}else{
  		$("div#travelmates").slideUp('quick',function(){
  			//
  		});
		}
  });

  $('input[name="gender"][type="radio"]').change(function(){
  	if ($(this).val() == 'male'){
  		$("#mensonly").show();
  		$("#mensonly1").show();
  		//document.getElementById("mensonly").style.display = "block";
  	}else{
  		$("#mensonly").hide();
  		$("#mensonly1").hide();
  		//JS way
  		//document.getElementById("mensonly").style.display = "none";
		}
  });
  
  $('input[name="expbioche"][type="radio"]').change(function(){
  	if ($(this).val() == 'true'){
  		$("#explosives ").slideDown('quick');
  	}else{
			$("#explosives ").slideUp('quick');
		}
  });
  
  //Part of group
  $('input[name="partofgroup"][type="radio"]').change(function(){
  	if ($(this).val() == 'true'){
  		$("div#addpeople").slideUp('quick',function(){
	  		$("div#organization").slideDown('slow',function(){
	  			//
	  		});
  		});

  	}else{
  		$("div#organization").slideUp('slow',function(){
	  		$("div#addpeople").slideDown('quick',function(){
	  			//
	  		});
  		});
		}
  });
  
  //Other nationality
  $("input[name=othernationality][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#other_nationality").slideDown('slow',function(){
				//
			});
		}else{
			$("div#other_nationality").slideUp('quick',function(){
				//
			});
		}
  });
  
  //Visits
  $("input[name=visited_before][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#visits").slideDown('slow',function(){
				//
			});
		}else{
			$("div#visits").slideUp('quick',function(){
				//
			});
		}
  });
  
  
	$("input[name=know_where_staying][type=radio]").change(function(){
		if ($(this).val() == 'true'){
				$("div#favorite_dest").slideUp('quick',function(){
					$("div#staying_place").slideDown('slow');	
				});
						
		}else{
				$("div#staying_place").slideUp('quick',function(){
					$("div#favorite_dest").slideDown('slow');		
				});
				
		}
  });
  
  //Other address for mail delivery
  $("input[name=other_address][type=radio]").change(function(){
		if ($(this).val() == 'false'){
			$("div#mail_address").slideDown('slow',function(){
				//
			});
		}else{
			$("div#mail_address").slideUp('quick',function(){
				//
			});
		}
  });
  
  //Father is in the USA
  $("input[name=father_in_usa][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#father").slideDown('slow',function(){
				//
			});
		}else{
			$("div#father").slideUp('quick',function(){
				//
			});
		}
  });
  
  //Mother is in the USA
  $("input[name=mother_in_usa][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#mother").slideDown('slow',function(){
				//
			});
		}else{
			$("div#mother").slideUp('quick',function(){
				//
			});
		}
  });
  
  //Has 1st degree relative in the USA
  $("input[name=1st_relative_exist][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#1st_relative").slideDown('slow',function(){
				//
			});
		}else{
			$("div#1st_relative").slideUp('quick',function(){
				//
			});
		}
  });

  $("input[name=had_dl][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#driving_license").slideDown('slow',function(){
				//
			});
		}else{
			$("div#driving_license").slideUp('quick',function(){
				//
			});
		}
  });
  $("input[name=had_visa][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#previous_visa").slideDown('slow',function(){
				//
			});
		}else{
			$("div#previous_visa").slideUp('quick',function(){
				//
			});
		}
  });
 
  $("input[name=visa_stolen][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#stolen").slideDown('slow',function(){
				//
			});
		}else{
			$("div#stolen").slideUp('quick',function(){
				//
			});
		}
  });
  
  $("input[name=visa_revoked][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#revoked").slideDown('slow',function(){
				//
			});
		}else{
			$("div#revoked").slideUp('quick',function(){
				//
			});
		}
  });
  
  $("input[name=visa_rejected][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#visa_rejected_block").slideDown('slow',function(){
				//
			});
		}else{
			$("div#visa_rejected_block").slideUp('quick',function(){
				//
			});
		}
  });
  
  $("input[name=entrance_denied][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#entrance_denied_block").slideDown('slow',function(){
				//
			});
		}else{
			$("div#entrance_denied_block").slideUp('quick',function(){
				//
			});
		}
  });
  
  
  $("input[name=other_countries][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#othercountries").slideDown('slow',function(){
				//
			});
		}else{
			$("div#othercountries").slideUp('quick',function(){
				//
			});
		}
  });
  $("input[name=other_job][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#otherjob").slideDown('slow',function(){
				//
			});
		}else{
			$("div#otherjob").slideUp('quick',function(){
				//
			});
		}
  });
    
  $("input[name=us_contact][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#uscontact").slideDown('slow',function(){
				//
			});
		}else{
			$("div#uscontact").slideUp('quick',function(){
				//
			});
		}
  });
  
  $("input[name=is_student][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#student").slideDown('quick');
		}else{
			$("div#student").slideUp('quick');
		}
  });  
  
  $("input[name=academic_degree][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#academic").slideDown('slow',function(){
				//
			});
		}else{
			$("div#academic").slideUp('quick',function(){
				//
			});
		}
  });  

  $("input[name=military_service][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#military").slideDown('slow',function(){
				//
			});
		}else{
			$("div#military").slideUp('quick',function(){
				//
			});
		}
  });  

  $("input[name=2ndpassport][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#2ndpassport_block").slideDown('slow',function(){
				//
			});
		}else{
			$("div#2ndpassport_block").slideUp('quick',function(){
				//
			});
		}
  }); 

  $("input[name=immigration_request][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#immigration_block").slideDown('slow',function(){
				//
			});
		}else{
			$("div#immigration_block").slideUp('quick',function(){
				//
			});
		}
  });  
  $("input[name=volunteer][type=radio]").change(function(){
		if ($(this).val() == 'true'){
			$("div#volunteer_block").slideDown('slow',function(){
				//
			});
		}else{
			$("div#volunteer_block").slideUp('quick',function(){
				//
			});
		}
  });  

});



function stopRKey(evt) { 
    evt = evt ? evt : ((event) ? event : null);
    var node = evt.target ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    return !( (evt.keyCode == 13) && (node.type=="text") );
}

document.onkeypress = stopRKey; 
