$(document).ready(function(){
	$(document).on('click', "#contact_send", function(){
		
		var contactErrors = false;

		var $contactAnswer = $(".contact-answer");
		var $form = $("#contactf");

		$form.find("input[type=text], input[type=email]").each(function(){
	  		if ( $(this).is('.hilight') )
		  	{
				$(this).removeClass('hilight');
		  	}
	  
			if( $(this).val() == "" )
			{
				$(this).addClass('hilight');
				contactErrors = true;
			}
		});
		var email = $("#contact_email");

		// "\w" in regular expression includes A-Z a-z 0-9 and "_" (underscore)
		var emailReg = /^([0-9a-zA-Z]([\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,4})$/;

		if( !emailReg.test(email.val()) || email.val() == "" )
		{
			 $(email).addClass('hilight');
			 contactErrors = true;
		}

		if (contactErrors)
		{
			$contactAnswer.fadeOut("quick", function(){
				$form.slideDown("quick");
			});
			return false;
		}else{
			$form.slideUp("quick", function(){
				$contactAnswer.slideDown("quick");
			});
			var data = $("#contactf").serialize();

		  	$.ajax({
			  	type: 'POST',
			  	data: data+'&source=mobile',
			  	url:'/ajax/sendMessage.php',
		  		success: function(response)
		  		{

				  	if( response.indexOf('error') >= 0 )
				  	{
						$contactAnswer.html('שגיאה בשליחת הודעה!');
						
						setTimeout(function(){
							$contactAnswer.slideUp('quick', function(){
								$form.slideDown('quick');
							});
							
						},1500);
				  	}
				  	else
				  	{
				  		var hideInterval = 6;
				  	
	  					$contactAnswer.slideUp('quick');

				  		var answer = $('<div/>', { 'class':'contact-thanks' })
				  		.append('<div class="line1">הודעתך התקבלה בהצלחה!</div>')
				  		.append('<div class="line2">נציגינו יחזרו אליך בהקדם האפשרי</div>')
				  		.append('<div class="line3">הודעה זו תעלם בעוד <span class="contact-close-counter">'+hideInterval+'</span> שניות</div>');

				  		$(".contact").append(answer);
				  		$("#contact-thanks").fadeIn('quick');

				  		window.contactInterval = setInterval(function(){
				  			$('.contact-close-counter').html( parseInt($('.contact-close-counter').html())-1 );
				  		},1000);
				  		
				  		setTimeout(function(){
				  			window.clearInterval(contactInterval);
				  			answer.fadeOut("quick");

				  			$form.find(":input").each(function(){

				  				$(this).val("");

				  			});
				  			$form.fadeIn("quick");

				  		}, hideInterval*1000);
				  	}


		  		}

			  });
		}


		return false;
	});
});