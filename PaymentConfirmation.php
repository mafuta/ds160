<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$exit = false;
$order = ( isset($_GET['Order']) && !empty($_GET['Order']) ? $_GET['Order'] : null );
if ( !empty($order) )
{
	list($paymentHash, $customerID) = explode(";;", $order);
	
	if ( !ds160::checkPaymentHash($customerID, $paymentHash) )
	{
		$exit = true;
	}
}else{
	$exit = true;
}
if ( $exit == true )
{
	header('location:index.php');
	exit;
}

require_once($_SERVER['DOCUMENT_ROOT'].'/inc/header.php');
?>

<div class="progressBar step6"></div>  
<div id="rgs" class="dsbox" style="width:950px; margin-top:1em;">
    <div class="dsbox_left" style="width:220px; word-wrap:break;">
        <div style="font-weight:bold; font-size:15px; margin-bottom:1em;">מה השלב הבא?</div>
        <p style="width:220px; text-align:justify; display:inline-block; margin-bottom:1em;">לקוח יקר, השלמת את השלב הראשוני בתהליך בקשת הויזה שלך, השלבים הבאים בטיפול בבקשתך הינם:</p>

        <div class="finish_left_item">
            <span class="finish_icon_black" style="background-position:1px 0px; width:21px; height:18px;"></span>
            <span class="finish_left_item_text" style="display:inline-block;">
                המתנה לשיחה מנציגי השירות שלנו, שידאגו להשלים פרטים חסרים ואת אמצעי תשלום האגרה ויתאמו יחד אתכם את מועד הריאיון שלכם בשגרירות לפי לוח הזמנים שלכם.
            </span>
        </div>
        
        <div class="finish_left_item">
            <span class="finish_icon_black" style="background-position:0px -20px; width:21px; height:22px;"></span>
            <span class="finish_left_item_text" style="display:inline-block;">
                קבלת כל המסמכים ליום הריאיון ודף הנחיות לתיבת הדוא"ל שלכם.
            </span>
        </div>
        
        <div class="finish_left_item">
            <span class="finish_icon_black" style="background-position:1px -43px; width:22px; height:23px;"></span>
            <span class="finish_left_item_text" style="display:inline-block;">
                הדפסת המסמכים המצורפים למייל וקבלת תדרוך והסבר טלפוני מלא מאחד מנציגנו.
            </span>
        </div>
        
        <div class="finish_left_item">
            <span class="finish_icon_black" style="background-position:0px -71px; width:22px; height:23px;"></span>
            <span class="finish_left_item_text" style="display:inline-block;">
               ניגשים לריאיון בשגרירות האמריקאית.
            </span>
        </div>

        <p>
            <u>לכל שאלה או בקשה</u>:<br />
            מוקד שירות: 052-8518665<br />
            דוא"ל: <a href="mailto:info@ds160.co.il" class="mail-address">info@ds160.co.il</a>
            <br />
            <span class="mail-address" onclick="Tooltip('contact');">טופס יצירת קשר</span>    
        </p>
    </div>
    
    <div class="dsbox_right" style="width:670px;">
        <div class="finish_header"></div>
    
        <div class="finish_info_item">
            <span class="finish_icon_gray" style="background-position:1px 0px; width:30px; height:28px;"></span>
            <span class="finish_item_text" style="display:inline-block;">
                לתיבת הדוא"ל שסיפקתם נשלחה הודעת אישור על קליטת בקשתכם במערכת.
                <br />
                * במידה ואינכם מצליחים לראות את ההודעה נא בדקו בתיבת ה"ספאם"
            </span>
        </div>
        
        <div class="finish_info_item">
            <span class="finish_icon_gray" style="background-position:1px -35px; width:30px; height:31px;"></span>
            <span class="finish_item_text" style="display:inline-block;">
                <b>אגרת השגרירות:</b> על מנת שנוכל לתאם בעבורכם את הריאיון בשגרירות עליכם לשלם את אגרת הויזה, אגרת הויזה הינה תשלום חובה לשגרירות ארה"ב בעלות של <b>$160</b>, בשקלים: כ- ₪ <?=ds160::getVisaFee()?>.
                לכל מגיש בקשה, ללא קשר לגילו.
            </span>
        </div>
        
        <span style="display:block; margin-right:6em; margin-top:1em; margin-bottom:1em;">אפשרויות תשלום האגרה:</span>
        
        <div class="finish_info_item">
            <span class="finish_icon_gray" style="background-position:1px -74px; width:30px; height:29px;"></span>
            <span class="finish_item_text" style="display:inline-block;">
                <u>בכרטיס אשראי</u>: נציג שירות יצור עמכם קרש להשלמת פרטי אמצעי התשלום.
            </span>
        </div>
        
        <div class="finish_info_item">
            <span class="finish_icon_gray" style="background-position:1px -104px; width:30px; height:28px;"></span>
            <span class="finish_item_text" style="display:inline-block;">
               <u>במזומן בשקלים</u>: אנו נדאג לשלוח לכם למייל שובר תשלום אישי אותו תוכלו לשלם בסניפי בנק הדואר.
            </span>
        </div>
        
        <div class="finish_info_item">
            <span class="finish_icon_gray" style="background-position:1px -135px; width:30px; height:29px;"></span>
            <span class="finish_item_text" style="display:inline-block;">
                נציג שירות יצור עמכם קשר בהקדם על מנת לברר את אפשרות התשלום המועדפת עליכם.
            </span>
        </div>
        
        <div style="width:90%; margin:0 auto; height:1px; border-bottom: 1px solid lightgray;"></div>
        
        
        <button class="finishButton" onclick="window.location.href='ds160.php';"></button>
    </div>

</div>

<!-- Google Code for Sale Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 984070199;
    var google_conversion_language = "en";
    var google_conversion_format = "2";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "QY87CLGP-wUQt_Ce1QM";
    var google_conversion_value = 0;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/984070199/?value=0&amp;label=QY87CLGP-wUQt_Ce1QM&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php');
?>
