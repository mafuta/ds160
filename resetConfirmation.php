<?php
$secured = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/base.php');

$mysqli = db::get_instance();
$ds160 = new ds160;

$required = array('UserHash','CustomerID');

foreach($required as $field)
{
	if (!isset($_GET[$field]) || empty($_GET[$field]))
	{
		header('location:'.SiteURL);
		exit;
	}
}

if ( !get_magic_quotes_gpc() )
{
	$_GET = array_map('mysql_real_escape_string',$_GET);
}

if ( !$ds160->getCustomerByHash($_GET['CustomerID'], $_GET['UserHash']) )
{
	$message = 'כניסה לעמוד איננה מאושרת, במידה והנך חושב שזוהי טעות אנא פנה לשירות הלקוחות.';
}
elseif( $ds160->HashUsed($_GET['CustomerID']) )
{
	header('location:'.SiteURL);
}
else
{
	if ( !ds160::resetCustomerPassword($_GET['CustomerID'], true) )
	{
		$message = 'קרתה תקלה בניסיון לשלוח סיסמה חדשה, אנא צור קשר עם צוות האתר';
	}
	else
	{
		
		$message = 'סיסמה חדשה נשלחה בהצלחה';

	}
}

include_once($_SERVER['DOCUMENT_ROOT'].'/inc/cleanHeader.php');
?>

	<div style="margin:0 auto; margin-top:10em; width:515px; background:#fff; border:8px solid #343434; padding:20px; border-radius:12px; font-size:14px;">
		<table>
			<tr>
				<td style="vertical-align:top;"><img src="/img/smallLoading.gif" /></td>
				<td style="vertical-align:top;">
					<?php
						echo '<b>'.$message.'!</b>';
						echo '<br />';
						echo 'הינך מועבר\ת לעמוד הראשי...';
					?>
				</td>
			</tr>
		</table>

	</div>
	
	<script type="text/javascript">
		setTimeout(function(){
			window.location.href='<?php echo SiteURL;?>';
		},8000);
	</script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/inc/cleanFooter.php');
?>