<?php
class customers
{
	public function __construct()
	{

	}

	public function getAll($limit, $where)
	{
		$conn = db::get_instance();
		
		$sql = "SELECT id, full_name, username, phone, reg_date, email, last_login_date FROM customers;";
		
		$result = $conn->query($sql);
		
		if ( $result->num_rows )
		{
			$customers = [];
			
			while ( $row = $result->fetch_object() )
			{
				$customers[] = new customer($row);
			}
		
			return $customers;
		
		}
		
		return false;
	}
	
	public static function exist($CustomerID)
	{
		$conn = db::get_instance();
		 
		$sql = "SELECT count(1) FROM customers WHERE id='{$CustomerID}';";

		$result = $conn->query($sql);
		
		if ( $result->num_rows )
		{
			return true;
		}
		
		return false;
	}
}