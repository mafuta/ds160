<?php

/*
 * Singletone mysqli wrapper
 * (C) Matan Hafuta
 * 2012
 * 
 */
 //Extending the default PHP's mysqli class


class db extends mysqli {
  
	//Static variable that will store the connection to the MySQL server
	//Will be returned for every connection request instead of creating new instance
	private static $connection = null;
	protected $result   = null;
	protected $numRows  = null;
    protected $where    = "";
        
	private function __construct($server,$username,$password,$db){
		//The constructor is declared as private to prevent the user from creating few instances
		//Can also pass the $charset='utf-8' as fourth parameter instead of setting the encoding later
		parent::__construct($server,$username,$password,$db);

		if ($this->connect_error){
		  die('Couldnt establish connection to the database');
		}

		//Setting the charset to utf8 (for PHP Ver < 5.4)
		$this->set_charset('utf8');
	}


	public static function get_instance(){
		//Public method, the user call this method in order to create connection to the database
		if (is_null(self::$connection)){
		  //If the collector is empty the function will call the constructor

			if ($_SERVER['SERVER_ADDR']=='127.0.0.1' || $_SERVER["SERVER_ADDR"] == "192.168.1.100"){
                $db_host = "212.179.77.63";
                $db_name = "hmywnldu_ds160";
                $db_user = "hmywnldu_hafuta";
                $db_pass = "I9Se0%X9?vx&";
			}elseif ($_SERVER['SERVER_ADDR']=='212.179.77.63'){
                $db_host = "127.0.0.1";
				$db_name = "hmywnldu_ds160";
				$db_user = "hmywnldu_hafuta";
				$db_pass = "I9Se0%X9?vx&";
			}

		  self::$connection = new self($db_host, $db_user, $db_pass, $db_name);
		}
		return self::$connection;
	}

	//Taking care of the error printing instead of doing this multiple times in the code
	public function query($query){
            $this->result = parent::query($query);

            if (!$this->result)
            {
                    //die($this->error);
                    /**
                     * @Todo Send error to hafuta@gmail.com
                     */
            }

            return $this->result;
	}

	public function escape($value){
		return parent::real_escape_string($value);
	}

	public function numRows($result){
		return $result->num_rows;
	}

	//Closes the connection
	public function __destruct(){
		if (!is_null(self::$connection)){
		  $this->close();
		}
	}
        
        /**
         * @method insert
         * 
         * @param string $tableName
         * @param array $insertData
         * @return insert_id on success, false on failure
         */
        public function insert($tableName, $insertData)
        {
            if ( empty($tableName) || empty($insertData)  )
            {
                return false;
            }
            
            $sql = "INSERT INTO `{$tableName}` SET";
            foreach($insertData as $field=>$val)
            {
                $sql .= " `{$field}`='".$this->escape($val)."',";
            }
           
            if ( substr($sql, -1) == "," )
            {
                $sql = substr($sql, 0, -1);
            }
            
            $sql .= ";";

            $result = $this->query($sql);
            if ( $result == true )
            {
                return $this->insert_id;
            }else{
                return false;
            }
        }
        
        /**
         * @method update
         * 
         * @param string $tableName
         * @param array $insertData
         * @return insert_id on success, false on failure
         */
        public function update($tableName, $updateData)
        {
            if ( empty($tableName) || empty($updateData)  )
            {
                return false;
            }
            
            $sql = "UPDATE `{$tableName}` SET";
            foreach($updateData as $field=>$val)
            {
                $sql .= " `{$field}`='".$this->escape($val)."',";
            }
           
            if ( substr($sql, -1) == "," )
            {
                $sql = substr($sql, 0, -1);
            }
            
            if ( !empty($this->where) )
            {
                $sql .= " WHERE ".$this->where;
            }
            
            $sql .= ";";
			
			
            $result = $this->query($sql);
            if ( $result == true )
            {
                return $this->affected_rows;
            }else{
                return false;
            }
        }
        
        public function where($field, $condition, $operator = null)
        {
            if ( empty($operator) || !in_array($operator, ["=", ">", "<", "!=", "<>", ">=", "<="])  )
            {
                $operator = "=";
            }
            
            if ( !empty($this->where) )
            {
                $this->where .= " AND";
            }
            
            $this->where .= " `{$field}`{$operator}'".$this->escape($condition)."'"; 
			
			return $this;
        }
        
        public function cleanWhere()
        {
            $this->where = "";
        }
        
        public function fetch_object($result)
        {
            if ( !empty($result) )
                return parent::fetch_object($result);
            else
                return false;
            
        }
}