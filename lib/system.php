<?php
/**
 * Description of system
 *
 * @author: Matan Hafuta
 * hafuta@gmail.com
 */
class system {

    public static function getUserIP($long = false) {
    	$IPVar = [
            "HTTP_X_REAL_IP",
            "REMOTE_ADDR",
            "HTTP_CLIENT_IP",
            "HTTP_X_CLUSTER_CLIENT_IP"
    	];
    	
    	foreach ( $IPVar as $key ) {
			if (array_key_exists ( $key, $_SERVER ) === true) {
				foreach ( explode ( ',', $_SERVER [$key] ) as $ip ) {
					if (filter_var ( $ip, FILTER_VALIDATE_IP ) !== false) {
							return ($long ? ip2long ( $ip ) : $ip);
					}
				}
			}
    	}
    }
    
    public static function getProtocol()
    {
        return ( isset($_SERVER["HTTPS"]) && !empty($_SERVER["HTTPS"]) ? "https://" : "http://" );
    }
    
    public function checkSecured($secured) {

        if ( empty ( $secured )) {
            $secured = false;
        }

        if ( empty( $_SERVER ["HTTPS"] ) && $secured ) {
			// If we are not on secured page and we should be then redirect
			header ( "location:https://" . $_SERVER ["SERVER_NAME"] . $_SERVER ["REQUEST_URI"] );
			exit;
        } elseif (! empty ( $_SERVER ["HTTPS"] ) && ! $secured) {
			// If we are on secured and we shouldn't be redirect to regular http
			header ( "location:http://" . $_SERVER ["SERVER_NAME"] . $_SERVER ["REQUEST_URI"] );
			exit;
        }
    }

    public function checkBlocked()
    {
        $mysqli = db::get_instance();
        $clientIP = system::getUserIP(true);
        $sql    = "SELECT blockID FROM blocked_ips WHERE ip='{$clientIP}' AND active=1;";
        $result = $mysqli->query($sql);
        if ( $result->num_rows > 0 ) { 
            header("location: http://www.ds160.co.il/404.php");
            exit;
        } 
    }

    public function checkConnected() {
        if (isset ( $_SESSION ["USER_ID"] ) && ! empty ( $_SESSION ["USER_ID"] )) {
                return true;
        }
        return false;
    }

    public function validateUser() {
        if (isset ( $_SESSION ["USER_HASH"] ) && isset ( $_SESSION ["USER_ID"] )) {
            if ( $this->getUserHash ( $_SESSION ["USER_ID"] ) == $_SESSION ["USER_HASH"]) {
                return true;
            }
        }
        return false;
    }

    public function getUserHash($userid) {
        $mysqli = db::get_instance ();
        $sql = "SELECT id,email,reg_date,reg_ip FROM `customers` WHERE id='{$userid}';";

        if ($result = $mysqli->query ( $sql )) {
            if ($mysqli->numRows($result) == 1) {
                $row = $result->fetch_object();
                $hash = md5 ( $row->id . $row->email . $row->reg_date . $row->reg_ip );
                return $hash;
            }
            return false;
        }
        return false;
    }
}