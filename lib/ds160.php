<?php
class ds160{

    protected static $visaFee = 576;

	public static function sendPaymentConfirmationMail($formID){
		global $base;
		
		$formID = (int) $formID;
		if ( empty($formID) ){ return false; } 

		$mysqli = db::get_instance();

		$sql 	= 	"SELECT 
						dsforms.CustomerID,
						customers.email, customers.full_name
					FROM 
						dsforms
					INNER JOIN
						customers ON(dsforms.CustomerID = customers.id)
					WHERE dsforms.id='{$formID}';";

		$result = $mysqli->query($sql);
		
		if ( empty($result->num_rows) )
		{
			return false;
		}
		
		$customer = $result->fetch_object();

        $transport = Swift_SendmailTransport::newInstance();
		//$transport = Swift_SmtpTransport::newInstance("smtp.gmail.com",587,"tls")->setUsername($base["SERVICE_MAIL"])->setPassword($base["SERVICE_PASS"]);
			
		//יצירת אובייקט לשליחת המייל
		$mailer = Swift_Mailer::newInstance($transport);
			
		//יצירת אובייקט הודעה
		$message = Swift_Message::newInstance()
			->setSubject('בקשתך לוויזה לארה״ב בטיפול!')
			->setFrom($base["SERVICE_MAIL"] ,'ds160 - שירות לקוחות')
			->setSender($base["SERVICE_MAIL"])
			->setReturnPath($base["SERVICE_MAIL"])
			->setTo($customer->email)
			->setCharset("utf-8")
			->setBody(
			'<!doctype html>
			<html dir="rtl">
			<head>
				<meta charset="utf-8" />
			</head>
			<body>
				<table cellpadding="0" style="direction: rtl; margin: 0 auto; height: 100% !important; width: 100%; background: #ffffff;" align="center"> 
					<tr align="center">
						<td align="right">
							<span style="font-weight:bold; font-size:14px;">לקוח יקר,</span>
							<br /><br /><br />
							אנו מודים לך על שבחרת ב <a href="https://www.ds160.co.il" style="color:#000; text-decoration:underline;">www.ds160.co.il</a> לטפל בבקשתך לויזה לארה"ב!
							<br /><br />
							<u>מהו השלב הבא?</u>
							<br /><br />
							אחד מנציגנו יצור עמך קשר בהקדם על מנת לתאם את מועד הראיון שלך בשגרירות ארה"ב ולהשלים במידת הצורך פרטים חסרים בבקשתך.
							<br /><br />
							בנוסף נציג השרות ישאל את העדפתך באופן תשלום אגרת הוויזה של שגרירות ארה"ב.
							<br /><br />
							בכל שאלה או בקשה תוכל לפנות לתמיכה בכתובת: <a href="mailto:service@ds160.co.il">service@ds160.co.il</a> או לנציג שרות טלפוני בשעות פעילות המוקד (0528-518665)
							<br /><br /><br /><br />
							בברכה,<br />
							צוות השרות והתמיכה.<br />
							<a href="http://www.ds160.co.il" style="color:#000; text-decoration:underline;">www.DS160.co.il</a>
						
							<div style="margin:1em auto; height:1px; width:100%; border-top:1px solid gray;"></div>
							
							<img src="http://www.ds160.co.il/img/mail_logo.png" />
						</td>
					</tr>

				</table>

			</body>
			</html>',
				"text/html" // Mark the content-type as HTML
			);
		
		if ($mailer->send($message, $failures))
		{
			$updateData = ["confirmationMailSent" => 1];

			$mysqli->where("id", $formID)->update("dsforms", $updateData);
			return true;
		}

		return false;
	}
	
	public static function checkPaymentHash($customerID, $paymentHash){
		
		if ( empty($customerID) || empty($paymentHash) ){ return false; } 
	
		$conn = db::get_instance();
		
		$stmt =  $conn->stmt_init();
		if ( $stmt->prepare("SELECT id, confirmationMailSent FROM dsforms WHERE CustomerID=? AND PaymentHash=?") )
		{
			$stmt->bind_param("is", $customerID, $paymentHash);
			$stmt->execute();
			$stmt->store_result();
			if ( $stmt->num_rows > 0 ){
				$formID = null;
				$confirmationMailSent = 0;
				
				$stmt->bind_result($formID, $confirmationMailSent);
				$stmt->fetch();
				
				$stmt->prepare("UPDATE dsforms SET usedPaymentHash=1 WHERE CustomerID=? AND PaymentHash=?");
				$stmt->bind_param("is", $customerID, $paymentHash);
				$stmt->execute();
				
				if ( !empty($formID) && empty($confirmationMailSent) )
				{
					self::sendPaymentConfirmationMail($formID);
				}
				
				//use store_result() in order to use affected_rows on the stmt object
				return true;
			}
			$stmt->close();
		}
		
		return false;
	
	}

	public function resetCustomerPassword($CustID, $SendMail = false){
		global $salt;
		
		$mysqli = db::get_instance();
	
		//creating unique string
		$newPassword = md5($CustID.time().$salt.rand(1,1000));
		//getting first 8 characters from the string
		$newPassword = substr($newPassword,0,8);

		$sql = "UPDATE customers SET password='".md5($newPassword.$salt)."' WHERE id='{$CustID}';";
		if (!$mysqli->query($sql)){
			return false;
		}
		
		if ($SendMail)
		{
			if ( self::SendNewPassMail($CustID, $newPassword) )
			{
				$sql = "UPDATE customers SET UserHash=NULL, UserHashUsed=1 WHERE id='{$CustID}';";
				$mysqli->query($sql);
				
				return true;
			}
		}

		return false;
	}

	public function HashUsed($CustID){
	
		$CustID = (int) $CustID;
		if ( empty($CustID) ){ return false; }
		
		$conn = db::get_instance();
		
		$sql = "SELECT
					UserHashUsed
				FROM
					customers
				WHERE
					id='{$CustID}';";
		
		$result = $conn->query($sql);
		
		if ( !$result->num_rows ){ return false; }
		
		$row = $result->fetch_object();
		
		if ( (int) $row->UserHashUsed==1 )
		{
			return true;
		}
		
		return false;
	}

	public function getCustomerByHash($CustID,$Hash)
	{
		$CustID = (int) $CustID;
		if ( empty($CustID) ){ return false; }
		
		$mysqli = db::get_instance();
	
		$Hash = $mysqli->escape($Hash);
		
		$sql = "SELECT
					count(1) AS Found
				FROM
					customers
				WHERE
					id='{$CustID}' AND UserHash='{$Hash}';";

		$result = $mysqli->query($sql);
		
		if ( !$result->num_rows ){
			return false;
		}
		
		$row = $result->fetch_object();
		
		if ( $row->Found )
		{
			return true;
		}

		return false;
	}
	
 	protected function SendNewPassMail($CustID, $newPassword)
	{
		global $base;
		
		$mysqli = db::get_instance();

		$sql 	= "SELECT full_name, email, username FROM customers WHERE id='{$CustID}';";
		$result = $mysqli->query($sql);
		$User 	= $result->fetch_object();

        $transport = Swift_SendmailTransport::newInstance();
		//$transport = Swift_SmtpTransport::newInstance("smtp.gmail.com",587,"tls")->setUsername($base["SERVICE_MAIL"])->setPassword($base["SERVICE_PASS"]);
			
		//יצירת אובייקט לשליחת המייל
		$mailer = Swift_Mailer::newInstance($transport);
			
		//יצירת אובייקט הודעה
		$message = Swift_Message::newInstance()
			->setSubject('סיסמת התחברות חדשה')
			->setFrom($base["SERVICE_MAIL"] ,'ds160 - שירות לקוחות')
			->setSender($base["SERVICE_MAIL"])
			->setReturnPath($base["SERVICE_MAIL"])
			->setTo($User->email)
			->setCharset("utf-8")
			->setBody(
			'<!doctype html>
			<html dir="rtl">
			<head>
				<meta charset="utf-8" />
			</head>
			<body>
				<table cellpadding="0" style="direction: rtl; margin: 0 auto; height: 100% !important; width: 100%; background: #ffffff;" align="center"> 
					<tr align="center">
						<td align="right">
							<b>'.$User->full_name.'</b>,
							<br /><br />
							לבקשתך, נוצרה עבורך סיסמת התחברות חדשה לאתר. <br />
							פרטי ההתחברות שלך הם:<br /><br />
							<b>שם משתמש:</b> '.$User->username.'<br />
							<b>סיסמה:</b> '.$newPassword.'<br />

							<br />
							
							אם הינך זקוק לסיוע נוסף תוכל לפנות לתמיכה בכתובת: <a href="mailto:service@ds160.co.il" style="color:#000; text-decoration:underline;">service@ds160.co.il</a> או לנציג שרות טלפוני בשעות פעילות המוקד.
							<br /><br />
							בברכה,<br />
							צוות השרות והתמיכה.<br />
							<a href="http://www.ds160.co.il" style="color:#000; text-decoration:underline;">www.DS160.co.il</a>
						
							<div style="margin:1em auto; height:1px; width:100%; border-top:1px solid gray;"></div>
							
							<img src="http://www.ds160.co.il/img/mail_logo.png" />
						</td>
					</tr>

				</table>

			</body>
			</html>',
				"text/html" // Mark the content-type as HTML
			);
		
		//אפשר להדפיס את ה-$failures
		//לא מדפיס כדי לא להחזיר פלט מיותר ל-Ajax
		if ($mailer->send($message, $failures))
		{
			return true;
		}

		return false;
	}
	
	/**
	 * Sending password reset mail to customer who requested password reset
	 */
	public static function SendPassResetMail($CustomerID)
	{
		global $base;

		$mysqli = db::get_instance();

		$sql = "SELECT email, UserHash FROM customers WHERE id='{$CustomerID}';";
		$result = $mysqli->query($sql);
		$row	= $result->fetch_object();

		if (empty($row->email))
		{
			return false;
		}

        $transport = Swift_SendmailTransport::newInstance();
		//$transport = Swift_SmtpTransport::newInstance("smtp.gmail.com",587,"tls")->setUsername($base["SERVICE_MAIL"])->setPassword($base["SERVICE_PASS"]);
			
		//יצירת אובייקט לשליחת המייל
		$mailer = Swift_Mailer::newInstance($transport);
			
		//יצירת אובייקט הודעה
		$message = Swift_Message::newInstance()
			->setSubject('מייל איפוס סיסמה')
			->setFrom($base["SERVICE_MAIL"] ,'ds160 - שירות לקוחות')
			->setSender($base["SERVICE_MAIL"])
			->setReturnPath($base["SERVICE_MAIL"])
			->setTo($row->email)
			->setCharset("utf-8")
			->setBody(
			'<!doctype html>
			<html dir="rtl">
			<head>
				<meta charset="utf-8" />
			</head>
			<body>
				<table cellpadding="0" style="direction: rtl; margin: 0 auto; height: 100% !important; width: 100%; background: #ffffff;" align="center"> 
					<tr align="center">
						<td align="right">
							<b>לקוח יקר,</b><br /><br />
							הגשת בקשת איזכור סיסמה. <br /><br />
							אם ברצונך להחליף את הסיסמה לחץ על הקישור הבא:
							<br />
							<a href="https://www.ds160.co.il/resetConfirmation.php?UserHash='.$row->UserHash.'&CustomerID='.$CustomerID.'" style="text-decoration:underline; font-weight:bold;">לחץ כאן לאיפוס הסיסמה לאתר</a>
							<br /><br />
							אם אינך מעוניין להחליף את הסיסמה אנא התעלם ממייל זה.
							<br /><br />
							אם הינך זקוק לסיוע נוסף תוכל לפנות לתמיכה בכתובת: <a href="mailto:service@ds160.co.il" style="color:#000; text-decoration:underline;">service@ds160.co.il</a> או לנציג שרות טלפוני בשעות פעילות המוקד.
							<br /><br />
							בברכה,<br />
							צוות השרות והתמיכה.<br />
							<a href="http://www.ds160.co.il" style="color:#000; text-decoration:underline;">www.DS160.co.il</a>
							
							<div style="margin:1em auto; height:1px; width:100%; border-top:1px solid gray;"></div>
							
							<img src="http://www.ds160.co.il/img/mail_logo.png" />
					</tr>
				</table>

			</body>
			</html>',
				"text/html" // Mark the content-type as HTML
			);
		
		//אפשר להדפיס את ה-$failures
		//לא מדפיס כדי לא להחזיר פלט מיותר ל-Ajax
		if ($mailer->send($message, $failures))
		{
			return true;
		}

		return false;
	}

      
	public static function email_exist($email){
		$mysqli = db::get_instance();
		
		$email = $mysqli->escape($email);
		
		$sql    = "SELECT id FROM customers WHERE email='{$email}';";
		
		if ($result = $mysqli->query($sql))
		{
			if ($result->num_rows > 0){
				return true;
			}
			return false;
		}
	}

	public static function username_exist($username){
		$mysqli = db::get_instance();
		$sql   = "SELECT id FROM customers WHERE username='{$username}';";

		if ($result = $mysqli->query($sql)){
				if ($result->num_rows > 0){
						return true;
				}
				return false;
		}
		//If something isn't working return true
		return true;
	}
       
	public function sendRegistrationMail($CustID)
	{
		global $base;


		$CustID = (int) $CustID;
		if ( empty($CustID) ){ return false; }
		
		$mysqli = db::get_instance();

		$sql 	= "SELECT full_name, email, username FROM customers WHERE id='{$CustID}';";

		$result = $mysqli->query($sql);
		
		if ( empty($result->num_rows) )
		{
			return false;
		}
		
		$User 	= $result->fetch_object();


		if ( empty($User->full_name) )
		{
			$User->full_name = "לקוח";
		}

        $transport = Swift_SendmailTransport::newInstance();
		//$transport = Swift_SmtpTransport::newInstance("smtp.gmail.com",587,"tls")->setUsername($base["SERVICE_MAIL"])->setPassword($base["SERVICE_PASS"]);

		//יצירת אובייקט לשליחת המייל
		$mailer = Swift_Mailer::newInstance($transport);

		//יצירת אובייקט הודעה
		$message = Swift_Message::newInstance()
			->setSubject('אישור הרשמה - DS160')
			->setFrom($base["SERVICE_MAIL"] ,'ds160 - שירות לקוחות')
			->setSender($base["SERVICE_MAIL"])
			->setReturnPath($base["SERVICE_MAIL"])
			->setTo($User->email)
			->setCharset("utf-8")
			->setBody(
			'<!doctype html>
			<html dir="rtl">
			<head>
				<meta charset="utf-8" />
			</head>
			<body>
				<table cellpadding="0" style="direction: rtl; margin: 0 auto; height: 100% !important; width: 100%; background: #ffffff;" align="center"> 
					<tr align="center">
						<td align="right">
							<span style="font-weight:bold; font-size:14px;">ברוך הבא, '.$User->full_name.'!</span>
							<br /><br /><br />
							תודה שנרשמת ל <a href="https://www.ds160.co.il" style="color:#000; text-decoration:underline;">www.ds160.co.il</a>
							<br /><br />
							להזכירך שם המשתמש שבחרת הינו: <span style="text-decoration:underline; font-weight:bold;">'.$User->username.'</span>
							<br /><br />
							נא השלם את השאלון באתר על מנת שנוכל לתאם בעבורך ראיון בשגרירות ארה"ב לקבלת הוויזה שלך.
							<br /><br />
							
							ניתן לפנות אלינו בכל שאלה או בקשה באמצעות האימייל: <a href="mailto:service@ds160.co.il" style="color:#000; text-decoration:underline;">service@ds160.co.il</a> או באמצעות הטלפון שמספרו 052-8518665 בשעות פעילות המוקד.
							
							<br /><br /><br /><br />
							בברכה,<br />
							צוות השרות והתמיכה.<br />
							<a href="http://www.ds160.co.il" style="color:#000; text-decoration:underline;">www.DS160.co.il</a>
						
							<div style="margin:1em auto; height:1px; width:100%; border-top:1px solid gray;"></div>
							
							<img src="http://www.ds160.co.il/img/mail_logo.png" />
						</td>
					</tr>

				</table>

			</body>
			</html>',
				"text/html" // Mark the content-type as HTML
			);

		//אפשר להדפיס את ה-$failures
		//לא מדפיס כדי לא להחזיר פלט מיותר ל-Ajax
		if ($mailer->send($message, $failures))
		{
			$updateData = ["registrationMailSent" => 1];
			
			$mysqli->where("id", $CustID)->update("customers", $updateData);
			
			return true;
		} else {
            print_r($failures);exit;
        }

		return false;
	}
	   

    public static function getVisaFee() {
        return self::$visaFee;
    }

}