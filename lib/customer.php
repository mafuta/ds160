<?php
class customer
{
	private $id;
	private $full_name;
	private $username;
	private $phone;
	private $reg_date;
	private $last_login_date;
	private $email;

	public function __construct($row)
	{
        if ( !empty($row) && is_object($row)  ) {
            $this->id 				= $row->id;
            $this->full_name 		= $row->full_name;
            $this->username 		= $row->username;
            $this->phone 			= $row->phone;
            $this->reg_date 		= $row->reg_date;
            $this->last_login_date 	= $row->last_login_date;
            $this->email 			= $row->email;
        }
	}
	
	public static function Get($CustomerID)
	{
		if ( !empty($CustomerID) )
		{
	
			$conn = db::get_instance();
			
			$sql = "SELECT id, full_name, username, phone, reg_date, last_login_date, email FROM customers WHERE id='{$CustomerID}';";
			
			$result = $conn->query($sql);
			
			if ( $result->num_rows ) {
				$obj = new self($result->fetch_object());
                return $obj;
			}
		}

        return null;
	}
	
	public function getInfo()
	{
		$info = new stdClass;
		
		$info->id 				= $this->id;
		$info->full_name 		= $this->full_name;
		$info->username 		= $this->username;
		$info->phone 			= $this->phone;
		$info->reg_date 		= $this->reg_date;
		$info->last_login_date 	= $this->last_login_date;
		$info->email 			= $this->email;

		return $info;
	}
	
	public function getComments()
	{
		
		$conn = db::get_instance();
		
		$sql = "SELECT id, `time` AS `timestamp`, comment, adminid FROM customers_notes WHERE customerid='{$this->id}' ORDER BY id DESC;";
	
		$result = $conn->query($sql);
		
		if ( $result->num_rows ) {
			$comments = [];
			
			while ( $row = $result->fetch_object() ) {
				$comments[] = $row;
			}
			
			return $comments;
		}
		
		return false;
	}
}